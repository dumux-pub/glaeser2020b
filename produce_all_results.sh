#!/bin/bash

echo "This script produces all results presented in the paper, for all cases,"
echo "sequentially. This may take quite some time..."
echo ""
echo "This script is expected to be called after installation/configuration"
echo "and from the top level of the repository. The results can then be found"
echo "in subfolders of the example folders in build-cmake/examples"

pushd build-cmake/examples/case1_convergence
./produce_case1_results.sh
popd

pushd build-cmake/examples/case2_anisotropic
./produce_case2_results.sh
./produce_case3_results.sh
popd

pushd build-cmake/examples/case4_benchmark
./produce_case4_results.sh
popd

pushd build-cmake/examples/case5_benchmark_3d
./produce_case5_results.sh
popd
