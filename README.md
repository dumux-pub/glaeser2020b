Welcome to the dumux pub table Glaeser2020b
========================================

This module contains the source code for the examples shown in the paper

Dennis Gläser, Martin Schneider, Bernd Flemisch, Rainer Helmig,
Comparison of cell- and vertex-centered finite-volume schemes for flow in fractured porous media,
Journal of Computational Physics, 2021, https://doi.org/10.1016/j.jcp.2021.110715

Installation instruction (__for linux__)
----------------------------------------

For building the code from source, clone this repository and type

```sh
chmod u+x glaeser2020b/installGlaeser2020b.sh
./glaeser2020b/installGlaeser2020b.sh
```

to download and configure all dune dependencies and dumux. This might take a while.
Please note that the following requirements need to be installed:

- CMake (>2.8.12)
- C, C++ compiler (C++14 required)
- Fortran compiler (gfortran)
- UMFPack from SuiteSparse

There are convenience scripts to generate the results of all test cases presented
in the paper. These are named `produce_case<i>_results.sh` where `<i>` is the case
number between 1 and 5. You can find these scripts in the respective subfolders
within the `examples` folder. Besides this, you can use the convenience script
`produce_all_results.sh` to generate all the results of the paper in one go.
Note that these scripts also produce all plots and figures shown in the paper.
For this to work, some more requirements need to be installed:

- python3 (+ matplotlib & scipy)
- pvpython (comes with a ParaView installation)
- latex (text rendering option with latex is set for matplotlib)

Run examples from docker
------------------------

If you have docker installed, you can use the docker image provided in this
to conveniently run the code without going through the installation process.
Moreover, you can use the convenience script at `docker/docker_glaeser2020b.sh`
to spin up the container sharing the current folder with it. All files places in
`/dumux/shared` in the container are then also visible from outside the container.
This is useful e.g. to visualize the results. To use the script, simply type

```sh
./PATH_TO_GLAESER2020B/docker/docker_glaeser2020b.sh open
```

into your terminal, where `PATH_TO_GLAESER2020B` should be substituted by the actual path.
