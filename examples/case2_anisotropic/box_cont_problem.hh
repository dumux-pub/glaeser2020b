// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem class for the single-phase test considering
 *        anisotropic permeabilities, solved with the box-cont model.
 */
#ifndef DUMUX_GLAESER2020B_ANISOTROPIC_BOX_CONT_PROBLEM_HH
#define DUMUX_GLAESER2020B_ANISOTROPIC_BOX_CONT_PROBLEM_HH

#include <utility>

#include <dumux/porousmediumflow/problem.hh>
#include "boundaryconditions.hh"

namespace Dumux {

/*!
 * \brief The problem implementation.
 */
template<class TypeTag>
class OnePBoxContProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

public:

    //! The constructor
    OnePBoxContProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , fractureAperture_(getParamFromGroup<Scalar>("Facet", "SpatialParams.Aperture"))
    {}

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     * \note In the box-scheme, we compute fluxes etc element-wise, thus, per
     *       element we compute only half a fracture (and extrude with aperture/2)
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return scv.isOnFracture() ? fractureAperture_/2.0 : 1.0; }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return AnisotropicTestBCs::bulkDirichletAtPos<PrimaryVariables>(globalPos); }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const { return 293.15; /* 10°C */ }

    // functions we need for compatibilty with general solver
    template<class... Args> void writeCsvData(Args&&... x) const {}
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}

private:
    Scalar fractureAperture_;
};

} // end namespace Dumux

#endif
