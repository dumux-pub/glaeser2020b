import os
import sys
import math
import subprocess

# fracture aperture and dx in reference grid in the fracture
def getGridSpecs(aperture, networkIndex):
    result = {}

    result['dxRefFracture'] = aperture/6.0
    result['dxRatioRef'] = result['dxRefFracture']/(aperture*5.0)

    result['dxInitialFacet'] = result['dxRefFracture']*75
    result['dxRatioFacet'] = result['dxRatioRef']
    return result

def prepareGrids(geoFile, aperture, networkIndex, numRefinements, discLengths):
    mortarRatio = 1.2
    dxRefFracture = discLengths['dxRefFracture']
    dxRatioRef = discLengths['dxRatioRef']
    dxInitialFacet = discLengths['dxInitialFacet']
    dxRatioFacet = discLengths['dxRatioFacet']
    print("Removing old .geo and .msh files")
    for item in os.listdir():
        if os.path.isfile(item) and geoFile in item and os.path.splitext(item)[1] in ['.geo', '.msh']:
            subprocess.run(['rm', item], check=True)

    print("Making the geo files")
    try: subprocess.run(['python3', 'scripts/makegeofiles.py', geoFile, networkIndex, aperture], check=True)
    except: printErrorAndExit("Could not prepare geo files")

    # obtain the indices of the prepared fracture grids
    fracIndices = []
    for item in os.listdir():
        if os.path.isfile(item) and geoFile + "_frac_" in item and os.path.splitext(item)[1] == '.geo':
            index = item.replace(geoFile + "_frac_", "")
            index = index.replace(".geo", "")
            fracIndices.append(int(index))
    print("Found fractures with indices: ", fracIndices)

    print("Making the reference fracture grids")
    for fracIdx in fracIndices:
        try: subprocess.run(['gmsh', '-format', 'msh2', '-2', 'temporary_frac_' + str(fracIdx) + '.geo'])
        except: printErrorAndExit("Could not prepare fracture mesh files")

    # print("Preparing reference grid\n")
    # try:
    #     subprocess.run(['gmsh', '-setnumber', 'aperture', str(aperture),
    #                             '-setnumber', 'dxRatio', str(dxRatioRef),
    #                             '-setnumber', 'dxFracture', str(dxRefFracture),
    #                             '-format', 'msh2', '-2',
    #                             geoFile + '_reference.geo'], check=False)
    # except: printErrorAndExit("Could not make reference grid")

    print("Preparing mixed-dimensional sequence of grids\n")

    try:
        subprocess.run(['gmsh', '-setnumber', 'aperture', str(aperture),
                                '-setnumber', 'dxFracture', str(dxInitialFacet),
                                '-setnumber', 'dxRatio', str(dxRatioFacet),
                                '-format', 'msh2', '-2',
                                geoFile + '.geo'], check=False)
    except: printErrorAndExit("Could not make bulk grid")

    try:
        subprocess.run(['cp', geoFile + '.geo', geoFile + '_mortar.geo'])
        subprocess.run(['gmsh', '-setnumber', 'aperture', str(aperture),
                                '-setnumber', 'dxFracture', str(dxInitialFacet*mortarRatio),
                                '-setnumber', 'dxRatio', str(dxRatioFacet),
                                '-format', 'msh2', '-2',
                                geoFile + '_mortar.geo'], check=False)
    except: printErrorAndExit("Could not make bulk grid")

    subprocess.call(['cp', geoFile + '.msh',
                           geoFile + '_' + str(0) + '.msh'])
    subprocess.call(['cp', geoFile + '_mortar.msh',
                           geoFile + '_mortar_' + str(0) + '.msh'])

    # make further grids by refinement
    for refIdx in range(1, numRefinements+1):
        subprocess.call(['cp', geoFile + '_' + str(refIdx-1) + '.msh',
                               geoFile + '_' + str(refIdx) + '.msh'])
        subprocess.call(['cp', geoFile + '_mortar_' + str(refIdx-1) + '.msh',
                               geoFile + '_mortar_' + str(refIdx) + '.msh'])
        subprocess.call(['gmsh', '-format', 'msh2', '-refine', geoFile + '_' + str(refIdx) + '.msh'])
        subprocess.call(['gmsh', '-format', 'msh2', '-refine', geoFile + '_mortar_' + str(refIdx) + '.msh'])
