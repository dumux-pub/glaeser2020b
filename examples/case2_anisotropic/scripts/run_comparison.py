import subprocess
import argparse
import csv
import sys
import os
import numpy as np

# define a function to exit with error message
def printErrorAndExit(errorMessage):
    sys.stderr.write(errorMessage)
    sys.exit(1)

# we require Python3
if sys.version_info[0] < 3:
    printErrorAndExit("Python 3 required\n")

# parse command-line arguments
parser = argparse.ArgumentParser(
  description = 'Run case 1 with all schemes, produce comparative plots and create latex tables with matrix specifications.'
)
parser.add_argument('-a', '--aperture', required=True, help="Fracture aperture")
parser.add_argument('-o', '--outputDirectory', default=os.getcwd(), help="Directory to which the data files are written")
parser.add_argument('-mg', '--meshGeneration', choices=['refOnly', 'hybridOnly', 'all', 'skip'], default='all', help="Define which meshes to prepare, defaults to all")
parser.add_argument('-mr', '--mortarMeshRatio', choices=['bcdependent', 'finer', 'equal', 'coarser'], default='bcdependent', help="Set the mesh ratio of the mortar to the facet mesh. Defaults to bcdependent.")
parser.add_argument('-c', '--computations', choices=['all', 'refOnly', 'hybridOnly', 'skip'], default='all', help="Define which computations to do, defaults to all. If skipped, it is assumed that files are already present")
parser.add_argument('-pp', '--postProcessing', choices=['all', 'tableOnly', 'plotsOnly', 'skip'], default='all', help="Define which postprocessing steps to do, defaults to all")
parser.add_argument('-ib', '--interiorBCs', choices=['dirichlet', 'neumann'], default='neumann', help="Set usage of interior Dirichlet or Neumann boundary conditions")
args = vars(parser.parse_args())

# fracture aperture and dx to be used
aperture = 2e-3
dxFractureRef = aperture/8.0
dxFractureFacet = aperture/2.0
dxRatio = 0.025
schemes = ['tpfa', 'mpfa', 'box', 'box_mortar', 'box_cont']

# make sure the output folder exists
outDir = args['outputDirectory']
if not os.path.exists(outDir):
    os.makedirs(outDir)


####################
# prepare the grids
if not args['meshGeneration'] == 'skip':
    if not args['meshGeneration'] == 'hybridOnly':
        print("\033[1m\033[94mPreparing reference grid\033[0m\n")
        try:
            subprocess.run(['gmsh', '-2', '-setnumber', 'aperture', str(aperture),
                                          '-setnumber', 'dxFracture', str(dxFractureRef),
                                          '-setnumber', 'dxRatio', str(dxRatio),
                                          '-format', 'msh2',
                                          '../grids/unit_2d_single_reference.geo'], check=True)
        except: printErrorAndExit("Could not make reference grid\n")

    if not args['meshGeneration'] == 'refOnly':
        print("\033[1m\033[94m\n\nPreparing mixed-dimensional grid\033[0m\n")
        try:
            subprocess.run(['gmsh', '-2', '-setnumber', 'dxFracture', str(dxFractureFacet),
                                          '-setnumber', 'dxRatio', str(dxRatio),
                                          '-format', 'msh2',
                                          '../grids/unit_2d_single.geo'], check=True)
        except: printErrorAndExit("Could not make bulk grid")

        subprocess.call(['cp', '../grids/unit_2d_single.msh',
                               '../grids/unit_2d_single_mortar.msh'])
        subprocess.call(['gmsh', '-format', 'msh2', '-refine', '../grids/unit_2d_single.msh'])

        if args['mortarMeshRatio'] == 'finer' or \
          (args['mortarMeshRatio'] == 'bcdependent' and args['interiorBCs'] == 'neumann'):
                subprocess.call(['gmsh', '-format', 'msh2', '-refine', '../grids/unit_2d_single_mortar.msh'])
                subprocess.call(['gmsh', '-format', 'msh2', '-refine', '../grids/unit_2d_single_mortar.msh'])
        elif args['mortarMeshRatio'] == 'equal':
            subprocess.call(['gmsh', '-format', 'msh2', '-refine', '../grids/unit_2d_single_mortar.msh'])

solverTimes = {}
matrixSpecs = {}
if not args['computations'] == 'skip':
    ###########################
    # run all simulations

    # run reference simulation
    if not args['computations'] == 'hybridOnly':
        print("\n\n \033[1m\033[94mRunning reference simulation\033[0m\n\n")
        try:
            subprocess.run(['make', 'example_reference_1p_anisotropic'], check=True)
            subprocess.run(['./example_reference_1p_anisotropic', 'params.input',
                            '-Reference.Grid.File', '../grids/unit_2d_single_reference.msh',
                            '-Facet.SpatialParams.Aperture', str(aperture)], check=True)
        except: printErrorAndExit("Error during reference run\n")

        # copy results to folder & remove clutter
        subprocess.call(['mv', 'reference_bulk-00001.vtu', os.path.join(outDir, 'reference_bulk.vtu')])
        subprocess.call(['mv', 'reference_facet-00001.vtu', os.path.join(outDir, 'reference_facet.vtu')])
        subprocess.call('mv reference*data* ' + outDir, shell=True)
        subprocess.call(['rm', 'reference_bulk-00000.vtu', 'reference_bulk.pvd'])
        subprocess.call(['rm', 'reference_facet-00000.vtu', 'reference_facet.pvd'])

    # run facet simulations
    if not args['computations'] == 'refOnly':
        for scheme in schemes:

            # box-cont simulation is run with a different call
            if scheme == 'box_cont':
                continue

            print("\n\n \033[1m\033[94mRunning " + scheme + " simulation\033[0m\n\n")

            if os.path.exists( os.path.join(os.getcwd(), "solvertimes.csv") ):
                print("Removing old solver times .csv file")
                subprocess.call(['rm', 'solvertimes.csv'])

            try:
                intDiri = 'true' if args['interiorBCs'] == 'dirichlet' else 'false'
                subprocess.run(['make', 'example_facet_1p_anisotropic_' + scheme], check=True)
                subprocess.run(['./example_facet_1p_anisotropic_' + scheme, 'params.input',
                                '-Grid.File', '../grids/unit_2d_single.msh',
                                '-Mortar.Grid.File', '../grids/unit_2d_single_mortar.msh',
                                '-Bulk.Problem.UseInteriorDirichletBCs', intDiri,
                                '-Facet.SpatialParams.Aperture', str(aperture)], check=True)
            except: printErrorAndExit("Error during " + scheme + " run\n")

            schemeSolverTimes = open( os.path.join(os.getcwd(), 'solvertimes.csv'), 'r' ).readlines()[0]
            if schemeSolverTimes[-1] == ",":
                solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')[0:-1], dtype=float))
            else:
                solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')))

            # move results to folder
            subprocess.call(['mv', 'solvertimes.csv', os.path.join(outDir, scheme + '_solvertimes.csv')])
            subprocess.call(['mv', 'bulk-00001.vtu', os.path.join(outDir, scheme + '_bulk.vtu')])
            subprocess.call(['mv', 'facet-00001.vtp', os.path.join(outDir, scheme + '_facet.vtp')])
            subprocess.call(['mv', 'bulk_data_lo.csv', os.path.join(outDir, scheme + '_bulk_data_lo.csv')])
            subprocess.call(['mv', 'bulk_data_hi.csv', os.path.join(outDir, scheme + '_bulk_data_hi.csv')])
            subprocess.call(['mv', 'condnumbers.txt', os.path.join(outDir, scheme + '_condnumbers.csv')])

            # remove vtk clutter
            subprocess.call(['rm', 'bulk-00000.vtu', 'bulk.pvd', 'facet-00000.vtp', 'facet.pvd'])

            # get matrix specs for this scheme
            subprocess.call(['mv', 'bulk_matrix.mtx', os.path.join(outDir, scheme + '_matrix.mtx')])
            subprocess.call(['python3', os.path.join(os.getcwd(), 'scripts/getmatrixspecs.py'),
                                        os.path.join(outDir, scheme + '_matrix.mtx'),
                                        os.path.join(outDir, scheme + '_matrixspecs.csv')])
            matrixSpecs[scheme] = np.genfromtxt(os.path.join(outDir, scheme + '_matrixspecs.csv'), names=True, delimiter=',')

        # run box-cont simulation
        print("\n\n \033[1m\033[94mRunning box-cont simulation\033[0m\n\n")

        if os.path.exists( os.path.join(os.getcwd(), "solvertimes.csv") ):
            print("Removing old solver times .csv file")
            subprocess.call(['rm', 'solvertimes.csv'])

        try:
            subprocess.run(['make', 'example_box_cont_1p_anisotropic'], check=True)
            subprocess.run(['./example_box_cont_1p_anisotropic', 'params.input',
                            '-Grid.File', '../grids/unit_2d_single.msh',
                            '-Facet.SpatialParams.Aperture', str(aperture)], check=True)
        except: printErrorAndExit("Error during box-cont run\n")

        scheme ='box_cont'
        schemeSolverTimes = open( os.path.join(os.getcwd(), 'solvertimes.csv'), 'r' ).readlines()[0]
        if schemeSolverTimes[-1] == ",":
            solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')[0:-1], dtype=float))
        else:
            solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')))

        # copy results to folder
        subprocess.call(['mv', 'solvertimes.csv', os.path.join(outDir, scheme + '_solvertimes.csv')])
        subprocess.call(['mv', 'box_cont-00001.vtu', os.path.join(outDir, scheme + '_bulk.vtu')])
        subprocess.call(['mv', 'box_cont_fracture-00001.vtp', os.path.join(outDir, scheme + '_facet.vtp')])
        subprocess.call(['mv', 'condnumbers.txt', os.path.join(outDir, scheme + '_condnumbers.csv')])

        # remove vtk clutter
        subprocess.call(['rm', 'box_cont-00000.vtu', 'box_cont.pvd', 'box_cont_fracture-00000.vtp', 'box_cont_fracture.pvd'])

        # get matrix specs for this scheme
        subprocess.call(['mv', 'box_cont_matrix.mtx', os.path.join(outDir, scheme + '_matrix.mtx')])
        subprocess.call(['python3', os.path.join(os.getcwd(), 'scripts/getmatrixspecs.py'),
                                    os.path.join(outDir, scheme + '_matrix.mtx'),
                                    os.path.join(outDir, scheme + '_matrixspecs.csv')])
        matrixSpecs[scheme] = np.genfromtxt(os.path.join(outDir, scheme + '_matrixspecs.csv'), names=True, delimiter=',')

# read matrix specs and solver times from files if hybrid schemes were skipped
if args['computations'] == 'skip' or args['computations'] == 'refOnly':
    for scheme in schemes:
        matrixSpecs[scheme] = np.genfromtxt(os.path.join(outDir, scheme + '_matrixspecs.csv'), names=True, delimiter=',')
        schemeSolverTimes = open( os.path.join(outDir, scheme + '_solvertimes.csv'), 'r' ).readlines()[0]
        if schemeSolverTimes[-1] == ",":
            solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')[0:-1], dtype=float))
        else:
            solverTimes[scheme] = np.mean(np.array(schemeSolverTimes.split(',')))

if not args['postProcessing'] == 'skip':

    #############################
    # make Latex table from results
    if not args['postProcessing'] == 'plotsOnly':
        print("\n\n \033[1m\033[94mWriting latex table with matrix specifications\033[0m\n\n")

        latexTableFile = open(os.path.join(outDir, 'table.tex'), 'w')
        latexTableFile.write(r'\begin{table}[t]' + '\n')
        latexTableFile.write('\t' + r'\centering' + '\n')
        latexTableFile.write('\t' + r'\caption{\textbf{Test case 1: matrix specifications}. Number of degrees of freedom (numDof), number of nonzero entries (nnz),' + '\n' \
                             '\t' + r'                                                      nnz/numDof$^2$ and the cpu time required for the solution of the linear system,' + '\n' \
                             '\t' + r'                                                      as obtained for the different schemes. The computations were carried out on a single' + '\n' \
                             '\t' + r'                                                      cpu with 2.5 \SI{\giga \hertz}}' + '\n')
        latexTableFile.write('\t' + r'\begin{tabular}{ *{6}{l} }' + '\n')
        latexTableFile.write('\t' + r'\toprule' + '\n')
        latexTableFile.write('\tscheme\t\t&  ' \
                             'numDof\t\t& ' \
                             'nnz\t\t& ' \
                             'nnz/numDof$^2$\t\t& ' \
                             'solver cpu time ' + r'[\si{\second}]' + '\t\t' + r'\\ \midrule' + '\n')

        for key in matrixSpecs.keys():
            latexTableFile.write('\t' + key + '\t\t& '
                                      + r'\num{' + str(matrixSpecs[key]['numdofs']) + '} & '
                                      + r'\num{' + str(matrixSpecs[key]['nnz']) + '} & '
                                      + r'\num{' + str(matrixSpecs[key]['sparsity']) + '} & '
                                      + r'\num{' + str(solverTimes[key]) + r'} \\' + '\n')

        latexTableFile.write('\t' + r'\bottomrule' + '\n')
        latexTableFile.write('\t' + r'\end{tabular}' + '\n')
        latexTableFile.write('\t' + r'\label{tab:single_1e4_matrixspecs}' + '\n')
        latexTableFile.write(r'\end{table}' + '\n')



    ##########################################
    # make the pressure / transfer flux plots
    if not args['postProcessing'] == 'tableOnly':
        print("\n\n \033[1m\033[94mCalling pvpython to get plot over line data\033[0m\n\n")

        polDataScript = os.path.join(os.getcwd(), '../../../../dumux/bin/postprocessing/extractlinedata.py')

        p1RefFracture = np.array([0.25, 0.35, 0.0])
        p2RefFracture = np.array([0.75, 0.65, 0.0])
        d = p2RefFracture - p1RefFracture
        d *= 1e-4
        p1RefFracture += d
        p2RefFracture -= d

        subprocess.run(['pvpython', polDataScript,
                        '-o', outDir, '-f', os.path.join(outDir, 'reference.vtu'),
                        '-of', 'reference_bulk',
                        '-p1', '0.15', '0.0', '0.0', '-p2', '0.85', '1.0', '0.0'], check=True)
        subprocess.run(['pvpython', polDataScript,
                        '-o', outDir, '-f', os.path.join(outDir, 'reference.vtu'),
                        '-of', 'reference_fracture',
                        '-p1', str(p1RefFracture[0]), str(p1RefFracture[1]), '0.0',
                        '-p2', str(p2RefFracture[0]), str(p2RefFracture[1]), '0.0'], check=True)

        for scheme in schemes:
            subprocess.run(['pvpython', polDataScript,
                            '-o', outDir, '-f', os.path.join(outDir, scheme + '_bulk.vtu'),
                            '-p1', '0.15', '0.0', '0.0', '-p2', '0.85', '1.0', '0.0'], check=True)
            subprocess.run(['pvpython', polDataScript,
                            '-o', outDir, '-f', os.path.join(outDir, scheme + '_facet.vtp'),
                            '-p1', '0.25', '0.35', '0.0', '-p2', '0.75', '0.65', '0.0'], check=True)

        # use custom plot style
        sys.path.append("../common")
        import plotstyles
        from plotstyles import *
        import matplotlib.pyplot as plt

        ######################################
        # plot of head through the bulk medium
        plt.figure(1)

        data = np.genfromtxt(os.path.join(outDir, 'reference_bulk.csv'), delimiter=',', names=True)
        plt.plot(data["arc_length"], data["p"], label=referenceLabel, color=referenceColor)
        for scheme in schemes:
            schemeId = getSchemeId(scheme)
            data = np.genfromtxt(os.path.join(outDir, scheme + '_bulk.csv'), delimiter=',', names=True)
            plt.plot(data["arc_length"], data["p"],
                     label=labels[schemeId],
                     color=colors[schemeId],
                     linestyle=linestyles[schemeId],
                     marker=markers[schemeId])

        plt.ylabel("$h_2 \, [\mathrm{m}]$")
        plt.xlabel("$\mathrm{arc} \, \mathrm{length} \, [\mathrm{m}]$")
        plt.legend()
        plt.savefig(os.path.join(outDir, "plot_bulk_head.pdf"), bbox_inches='tight')

        ######################################
        # plot of head through the fracture
        plt.figure(2)

        data = np.genfromtxt(os.path.join(outDir, 'reference_fracture.csv'), delimiter=',', names=True)
        plt.plot(data["arc_length"], data["p"], label=referenceLabel, color=referenceColor)
        for scheme in schemes:
            schemeId = getSchemeId(scheme)
            data = np.genfromtxt(os.path.join(outDir, scheme + '_facet.csv'), delimiter=',', names=True)
            plt.plot(data["arc_length"], data["p"],
                     label=labels[schemeId],
                     color=colors[schemeId],
                     linestyle=linestyles[schemeId],
                     marker=markers[schemeId])
        plt.ylabel("$h_1 \, [\mathrm{m}]$")
        plt.xlabel("$\mathrm{arc} \, \mathrm{length} \, [\mathrm{m}]$")
        plt.legend()
        plt.savefig(os.path.join(outDir, "plot_fracture_head.pdf"), bbox_inches='tight')


        ##########################################
        # plot transfer flux & interface pressure
        plt.figure(3)

        data = np.genfromtxt(os.path.join(outDir, "reference_data_lo.csv"), delimiter=',', names=True)
        order = np.argsort(data['x'])
        plt.plot(data["x"][order], data["flux"][order], label=referenceLabel, color=referenceColor)
        for scheme in schemes:

            # do not consider tpfa and box-cont in these plots (for readability)
            if scheme == 'tpfa' or scheme == 'box_cont':
                continue

            schemeId = getSchemeId(scheme)
            data = np.genfromtxt(os.path.join(outDir, scheme + '_bulk_data_lo.csv'), delimiter=',', names=True)
            order = np.argsort(data['x'])

            plt.figure(3)
            plt.plot(data["x"][order], data["flux"][order],
                     label=labels[schemeId],
                     color=colors[schemeId],
                     linestyle=linestyles[schemeId],
                     marker=markers[schemeId])

            # the interface pressures are only available for vertex-based schemes
            if scheme == 'mpfa':
                continue

            plt.figure(4)
            plt.plot(data["x"][order], data["bulkpressure"][order],
                     label=r'$h_2 |_\gamma$ (' + labels[schemeId] + ')',
                     color=colors[schemeId],
                     linestyle='-',
                     marker=markers[schemeId],
                     alpha=0.5)
            plt.plot(data["x"][order], data["facetpressure"][order],
                     label=r'$h_1$ (' + labels[schemeId] + ')',
                     color=colors[schemeId],
                     linestyle='--',
                     marker=markers[schemeId],
                     alpha = 0.5)

        plt.figure(3)
        plt.ylabel("$F_2 |_\gamma \, [\mathrm{m}]$")
        plt.xlabel("$x \, [\mathrm{m}]$")
        plt.legend()
        plt.savefig(os.path.join(outDir, "plot_transferflux.pdf"), bbox_inches='tight')

        plt.figure(4)
        plt.ylabel("$h_i \, [\mathrm{m}]$")
        plt.xlabel("$x \, [\mathrm{m}]$")
        plt.legend()
        plt.savefig(os.path.join(outDir, "plot_ifpressure.pdf"), bbox_inches='tight')
