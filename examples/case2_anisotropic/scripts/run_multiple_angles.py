import subprocess
import math
import sys
import os

if len(sys.argv) < 3:
    sys.exit("Please provide the network index as input argument")

aperture = 2e-3
numRefinements = 5
anisotropyRatio = 5
anisotropyAngle = [math.pi/4.0]  # [0.0, math.pi/4.0, math.pi/2.0]
fracPermeability = sys.argv[2] # '1e4'

def getFolderName(angle):
    grad = angle*180.0/math.pi
    return 'angle_' + str(int(grad))

folderNames = []
for angle in anisotropyAngle:
    folderName = getFolderName(angle)
    if not os.path.exists(folderName):
        subprocess.run("mkdir " + folderName, shell=True, check=True)
    folderNames.append(folderName)

for index, [angle, folderName] in enumerate(zip(anisotropyAngle, folderNames)):
    print("\n" + 50*'*')
    print("Running convergence test for angle = " + str(angle) + " (radians)\n")
    subprocess.run(['python3', 'scripts/convergencetest.py', str(numRefinements),
                                                             str(aperture),
                                                             str(anisotropyRatio),
                                                             str(angle),
                                                             sys.argv[1],
                                                             fracPermeability,
                                                             ("true" if index != 0 else "false")], check=True)

    # copy all files in sub-folder
    print("Moving files")
    subprocess.run('mv *vtu *pvd *vtp *csv *pdf ' + folderName, shell=True)

    # make convergence plot
    print("Making plots")
    subprocess.run(['python3', 'scripts/plotrates.py', str(aperture), folderName])

    print("Moving plots")
    subprocess.run('mv *pdf ' + folderName, shell=True)
