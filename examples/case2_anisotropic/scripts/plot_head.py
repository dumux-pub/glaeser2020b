import os
import sys
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

if len(sys.argv) != 4:
    sys.exit("Expected three arguments:\n" \
             "\t - path to the folder containing the results\n" \
             "\t - the refinement level to use\n" \
             "\t - path to the extractlinedata.py script in dumux/bin/postprocessing\n")

extractScript = sys.argv[3]

# function to obtain the plot over line data for a vtu file
def getPlotData(vtuFile, p1, p2, numSamples):
    print("Reading " + vtuFile)
    subprocess.run(['pvpython', extractScript,
                    '-f', vtuFile,
                    '-of', 'temp',
                    '-r', str(numSamples),
                    '-p1', p1[0], p1[1], p1[2],
                    '-p2', p2[0], p2[1], p2[2]], check=True)
    if not os.path.exists(os.path.join(sys.argv[1], 'temp.csv')):
        sys.exit("Could not find .csv file created by " + extractScript)
    data = np.genfromtxt(os.path.join(sys.argv[1], 'temp.csv'), names=True, delimiter=',')
    os.remove(os.path.join(sys.argv[1], 'temp.csv'))
    return {'arc': data['arc_length'], 'h': data['p']}

p1 = ['-0.5', '-0.4', '0']
p2 = ['0.5', '0.4', '0']
plt.figure(1)
for schemeName in names:
    if schemeName == 'tpfa':
        continue
        
    schemeId = getSchemeId(schemeName)
    schemeName = schemeName.replace('-', '_')
    if schemeName == 'box_mortar': vtuName = os.path.join(sys.argv[1], 'mortar_bulk_' + sys.argv[2] + '-00001.vtu')
    elif schemeName == 'box_cont': vtuName = os.path.join(sys.argv[1], 'box_cont_' + sys.argv[2] + '-00001.vtu')
    else: vtuName = os.path.join(sys.argv[1], schemeName + '_bulk_' + sys.argv[2] + '-00001.vtu')

    data = getPlotData(vtuName, p1, p2, 2000)
    plt.plot(data['arc'], data['h'], label=labels[schemeId], color=colors[schemeId], linestyle=linestyles[schemeId])

# add reference and save
data = getPlotData(os.path.join(sys.argv[1], 'reference-00001.vtu'), p1, p2, 2000)
plt.plot(data['arc'], data['h'], label=referenceLabel, color=referenceColor)

plt.xlabel("arc length [m]")
plt.ylabel(r"${h_2}$ [m]")
plt.legend()
plt.savefig("headplot.pdf", bbox_inches='tight')
