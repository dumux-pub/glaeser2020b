import sys
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

aperture = float(sys.argv[1])
subFolder = sys.argv[2]

# plot (vs 1/dx)
errorNormsBulk = np.genfromtxt(subFolder + "/errornorms_bulk.csv", delimiter=',', names=True)
errorNormsFacet = np.genfromtxt(subFolder + "/errornorms_facet.csv", delimiter=',', names=True)
xAxis = [aperture/h for h in errorNormsBulk['h']]

# reference plot for first order
firstOrderErrorsBulk = []
firstOrderErrorsFacet = []
firstOrderErrorsBulk.append(errorNormsBulk['mpfa'][0]/2.0)
firstOrderErrorsFacet.append(errorNormsFacet['mpfa'][0]/2.0)
for i in range(1, len(xAxis)):
    firstOrderErrorsBulk.append( firstOrderErrorsBulk[i-1]*xAxis[i-1]/xAxis[i] )
    firstOrderErrorsFacet.append( firstOrderErrorsFacet[i-1]*xAxis[i-1]/xAxis[i] )

mpfaSchemeId = getSchemeId('mpfa')
tpfaSchemeId = getSchemeId('tpfa')
boxSchemeId = getSchemeId('box')
boxMortarSchemeId = getSchemeId('box-mortar')
boxContSchemeId = getSchemeId('box-cont')

plt.figure(1)  #, figsize = [4.0, 4.8])
plt.loglog(xAxis, errorNormsBulk['mpfa'], marker='*', label=labels[mpfaSchemeId], color=colors[mpfaSchemeId], linestyle=linestyles[mpfaSchemeId])
plt.loglog(xAxis, errorNormsBulk['tpfa'], marker='*', label=labels[tpfaSchemeId], color=colors[tpfaSchemeId], linestyle=linestyles[tpfaSchemeId])
plt.loglog(xAxis, errorNormsBulk['box'], marker='*', label=labels[boxSchemeId], color=colors[boxSchemeId], linestyle=linestyles[boxSchemeId])
plt.loglog(xAxis, errorNormsBulk['boxmortar'], marker='*', label=labels[boxMortarSchemeId], color=colors[boxMortarSchemeId], linestyle=linestyles[boxMortarSchemeId])
plt.loglog(xAxis, errorNormsBulk['boxcont'], marker='*', label=labels[boxContSchemeId], color=colors[boxContSchemeId], linestyle=linestyles[boxContSchemeId])
plt.loglog(xAxis, firstOrderErrorsBulk, label=r'$\mathcal{O} (\eta)$', color='k', linestyle='--', alpha=0.75)
plt.legend(ncol=2, loc='lower left')
plt.xlabel(r"$a/\eta_f$")
plt.ylabel(r"$\varepsilon_{h_2}$")
plt.savefig("bulk.pdf", bbox_inches='tight')

plt.figure(2)  #, figsize = [4.0, 4.8])
plt.loglog(xAxis, errorNormsFacet['mpfa'], marker='*', label=labels[mpfaSchemeId], color=colors[mpfaSchemeId], linestyle=linestyles[mpfaSchemeId])
plt.loglog(xAxis, errorNormsFacet['tpfa'], marker='*', label=labels[tpfaSchemeId], color=colors[tpfaSchemeId], linestyle=linestyles[tpfaSchemeId])
plt.loglog(xAxis, errorNormsFacet['box'], marker='*', label=labels[boxSchemeId], color=colors[boxSchemeId], linestyle=linestyles[boxSchemeId])
plt.loglog(xAxis, errorNormsFacet['boxmortar'], marker='*', label=labels[boxMortarSchemeId], color=colors[boxMortarSchemeId], linestyle=linestyles[boxMortarSchemeId])
plt.loglog(xAxis, errorNormsFacet['boxcont'], marker='*', label=labels[boxContSchemeId], color=colors[boxContSchemeId], linestyle=linestyles[boxContSchemeId])
plt.loglog(xAxis, firstOrderErrorsFacet, label=r'$\mathcal{O} (\eta)$', color='k', linestyle='--', alpha=0.75)
plt.legend(ncol=2)
plt.xlabel(r"$a/\eta_f$")
plt.ylabel(r"$\varepsilon_{h_1}$")
plt.savefig("facet.pdf", bbox_inches='tight')
