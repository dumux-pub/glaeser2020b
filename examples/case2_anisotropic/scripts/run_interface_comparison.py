import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *
from preparegrids import *

if len(sys.argv) > 2 and sys.argv[2].lower() not in ['lo', 'hi']:
    sys.exit("2nd argument (fracture side) must be either lo or hi")

if len(sys.argv) > 1: permAngle = str(float(sys.argv[1])*math.pi/180.0)
else: permAngle = '0'

if len(sys.argv) > 2: fracSideSuffix = sys.argv[2].lower()
else: fracSideSuffix = 'lo'

useDirichlet = True if len(sys.argv) > 3 and sys.argv[3].lower() in ['true', 'yes', '1'] else False
skipSimulations = True if len(sys.argv) > 4 and sys.argv[4].lower() in ['true', 'yes', '1'] else False
skipMeshGeneration = False
skipReference = True
skipOne = False

# make zoom plots on fracture tips?
doZoom = False

psi = [0.75, 1.25] if not useDirichlet else [1.25, 1.75]
psiString = [str(p) for p in psi]

# suffix for file names
suffix = 'diri' if useDirichlet else 'neum'

# fracture indices for the ref & mixed-dim solutions
fracIndicesFacet = ['1']
fracIndicesRef = ['2']

# prepare grids
aperture = 2e-3
specs = getGridSpecs(aperture, 'interfacetest')
specs['dxRatioFacet'] = 1.0
dxFacet = specs['dxInitialFacet']/5.0
dxRatio = 1.0
geoFile = 'tmp'
facetPermeability = '5e2'



if not skipSimulations:
    if not skipMeshGeneration:
        print("Making the geo files")
        try: subprocess.run(['python3', 'scripts/makegeofiles.py', geoFile, 'interfacetest', str(aperture)], check=True)
        except: sys.exit("Could not prepare geo files")

        print("Meshing the bulk medium")
        try:
            subprocess.run(['gmsh', '-2', '-setnumber', 'aperture', str(aperture),
                                          '-setnumber', 'dxFracture', str(dxFacet),
                                          '-setnumber', 'dxRatio', str(dxRatio),
                                          '-format', 'msh2',
                                          geoFile + '.geo'], check=False)
        except: sys.exit("Could not make mixed-dimensional grid")

        subprocess.run(['cp', geoFile + '.geo', geoFile + '_mortar.geo'], check=False)

    # run box & mpfa schemes (mpfa is used as reference)
    for scheme in ['box', 'mpfa']:
        bulkDataFile = scheme + '_bulk_data_' + suffix
        facetDataFile = scheme + '_facet_data_' + suffix
        exeName = 'case2_facet_1p_anisotropic_' + scheme

        subprocess.run(['make', exeName], check=True)
        subprocess.run(['./' + exeName, 'params.input',
                        '-Bulk.SpatialParams.PermeabilityAngle', permAngle,
                        '-Grid.File', geoFile + '.msh',
                        '-Mortar.Grid.File', geoFile + '_mortar.msh',
                        '-Bulk.Problem.UseInteriorDirichletBCs', ('true' if useDirichlet else 'false'),
                        '-Bulk.IO.CsvFileNameBody', bulkDataFile,
                        '-Facet.IO.CsvFileNameBody', facetDataFile,
                        '-Facet.SpatialParams.Permeability', str(facetPermeability)], check=True)

    # run box-mortar scheme for various values of psi
    for pIndex, p in enumerate(psi):

        print("\nRunning simulation for p = " + str(psiString[pIndex]) + "\n")
        if not skipMeshGeneration:
            try:
                subprocess.run(['gmsh', '-2', '-setnumber', 'aperture', str(aperture),
                                              '-setnumber', 'dxFracture', str(dxFacet*p),
                                              '-setnumber', 'dxRatio', str(dxRatio),
                                              '-format', 'msh2',
                                               geoFile + '_mortar.geo'], check=False)
            except: sys.exit("Could not make mixed-dimensional grid")

        curSuffix = suffix + '_' + str(p)
        bulkDataFile = 'box_mortar_bulk_data_' + curSuffix
        facetDataFile = 'box_mortar_facet_data_' + curSuffix
        exeName = 'case2_facet_1p_anisotropic_box_mortar'

        subprocess.run(['make', exeName], check=True)
        subprocess.run(['./' + exeName, 'params.input',
                        '-Bulk.SpatialParams.PermeabilityAngle', permAngle,
                        '-Grid.File', geoFile + '.msh',
                        '-Mortar.Grid.File', geoFile + '_mortar.msh',
                        '-Bulk.Problem.UseInteriorDirichletBCs', ('true' if useDirichlet else 'false'),
                        '-Bulk.IO.CsvFileNameBody', bulkDataFile,
                        '-Facet.IO.CsvFileNameBody', facetDataFile,
                        '-Facet.SpatialParams.Permeability', str(facetPermeability)], check=True)

# function to clip reference data in order for the fracture tips not to be plotted
def clipRefData(refData):
    newX = []
    newFlux = []

    for i in range(0, len(refData['x'])):
        if refData['x'][i] < 0.199300006:
            newX.append(refData['x'][i])
            newFlux.append(refData['flux'][i])

    return {'x': np.array(newX), 'flux': np.array(newFlux)}



zoom_xLimits = [-0.355, -0.2]
def setFluxLimits(axis):
    if doZoom:
        axis.set_xlim(zoom_xLimits)
        axis.set_ylim([-5, 15])
    else:
        axis.set_ylim([-20, 20])

def setPressureLimits(axis):
    # if useDirichlet
    if doZoom:
        axis.set_xlim(zoom_xLimits)
        axis.set_ylim([1.05, 1.25])
    else:
        pass
        # axis.set_ylim([-11, 11])

# add box scheme
for figIndex, plotFlux in enumerate([False, True]):
    fig = plt.figure(figIndex, figsize=[12.0, 3.0])
    ax_box = fig.add_subplot(131)
    dataFileName = 'box_bulk_data_' + suffix + '_' + fracIndicesFacet[0] + '_' + fracSideSuffix + '.csv'
    data = np.genfromtxt(dataFileName, names=True, delimiter=',')
    indexMap = np.argsort(data['x'])
    if not plotFlux:
        ax_box.plot(data['x'][indexMap], data['bulkpressure'][indexMap], color=colors[getSchemeId('box')], label=r'$h_2 |_{\gamma_1}$', linestyle='-', alpha=0.5)
        ax_box.plot(data['x'][indexMap], data['facetpressure'][indexMap], color=colors[getSchemeId('box')], label=r'$h_1 |_{\gamma_1}$', linestyle='-.')
        # ax_box.set_ylabel(r'$h |_{\gamma_1}$')
        setPressureLimits(ax_box)
    else:
        refDataFile = 'mpfa_bulk_data_' + suffix + '_' + fracIndicesFacet[0] + '_' + fracSideSuffix + '.csv'
        refData = np.genfromtxt(refDataFile, names=True, delimiter=',')
        # refData = clipRefData(refData)
        refDataIndexMap = np.argsort(refData['x'])
        ax_box.plot(refData['x'][refDataIndexMap], refData['flux'][refDataIndexMap], color=referenceColor, label=r'$\lambda_\mathrm{ref}$', linestyle='--', alpha=0.5)
        ax_box.plot(data['x'][indexMap], data['flux'][indexMap], color=colors[getSchemeId('box')], label=r'$\lambda$', linestyle='-', alpha=0.5)
        ax_box.plot(data['x'][indexMap], data['kgradp'][indexMap], color=colors[getSchemeId('box')], label=r'$\left( \mathbf{K} \nabla h_2 \right) \cdot \mathbf{n} |_{\gamma_1}$', linestyle='-.')
        setFluxLimits(ax_box)

    ax_box.legend(loc='upper right')
    ax_box.set_xlabel(r'$x$')
    ax_box.set_title(r'\Large ' + labels[getSchemeId('box')])

    # add plots for box-mortar
    schemeId = getSchemeId('box-mortar')
    for pIndex, p in enumerate(psi):
        if pIndex == 0: ax_mortar = fig.add_subplot(132)
        else: ax_mortar = fig.add_subplot(133)
        curSuffix = suffix + '_' + str(p)
        dataFileName = 'box_mortar_bulk_data_' + curSuffix + '_' + fracIndicesFacet[0] + '_' + fracSideSuffix + '.csv'
        data = np.genfromtxt(dataFileName, names=True, delimiter=',')
        indexMap = np.argsort(data['x'])
        if not plotFlux:
            ax_mortar.plot(data['x'][indexMap], data['bulkpressure'][indexMap], color=colors[schemeId], label=r'$h_2 |_{\gamma_1}$', linestyle='-', alpha=0.5)
            ax_mortar.plot(data['x'][indexMap], data['facetpressure'][indexMap], color=colors[schemeId], label=r'$h_1 |_{\gamma_1}$', linestyle='-.')
            setPressureLimits(ax_mortar)
        else:
            refDataFile = 'mpfa_bulk_data_' + suffix + '_' + fracIndicesFacet[0] + '_' + fracSideSuffix + '.csv'
            refData = np.genfromtxt(refDataFile, names=True, delimiter=',')
            refData = clipRefData(refData)
            refDataIndexMap = np.argsort(refData['x'])
            ax_mortar.plot(refData['x'][refDataIndexMap], refData['flux'][refDataIndexMap], color=referenceColor, label=r'$\lambda_\mathrm{ref}$', linestyle='--', alpha=0.5)
            ax_mortar.plot(data['x'][indexMap], data['flux'][indexMap], color=colors[getSchemeId('box-mortar')], label=r'$\lambda$', linestyle='-', alpha=0.5)
            ax_mortar.plot(data['x'][indexMap], data['kgradp'][indexMap], color=colors[getSchemeId('box-mortar')], label=r'$\left( \mathbf{K} \nabla h_2 \right) \cdot \mathbf{n} |_{\gamma_1}$', linestyle='-.')
            setFluxLimits(ax_mortar)

        ax_mortar.legend(loc='upper right')
        ax_mortar.set_title(r'\Large ' + labels[getSchemeId('box_mortar')] + r' ($\psi = ' + psiString[pIndex] + r'$)')
        ax_mortar.set_xlabel(r'$x$')

    # ax_mortar.yaxis.set_major_formatter(plt.NullFormatter())
    # ax_mortar.yaxis.set_tick_params(length=0)
    # ax_mortar.set_ylabel(r'$p |_{\gamma_1}$')

    nameSuffix = '_zoom' if doZoom else ''
    if plotFlux: plt.savefig(f'interface_flux{nameSuffix}.pdf', bbox_inches='tight')
    else: plt.savefig(f'interface_pressure{nameSuffix}.pdf', bbox_inches='tight')
