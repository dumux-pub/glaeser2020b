import numpy as np
import scipy
import scipy.sparse.linalg as linalg
import scipy.io as io

M = io.mmread("bulk_matrix.mtx").tocsc()

invM = linalg.inv(M)
normOneM = linalg.norm(M, 1)
normOneInvM = linalg.norm(invM, 1)
print("One-Norm M: {:5.1e}".format(normOneM) )
print("One-Norm InvM: {:5.1e}".format(normOneInvM) )
print("Condition number (one-norm): {:5.1e}\n".format(normOneM*normOneInvM))

normInfM = linalg.norm(M, np.inf)
normInfInvM = linalg.norm(invM, np.inf)
print("Inf-Norm M: {:5.1e}".format(normInfM) )
print("Inf-Norm InvM: {:5.1e}".format(normInfInvM) )
print("Condition number (inf-norm): {:5.1e}\n".format(normInfM*normInfInvM))

print("Geometry mean: {:5.1e}".format( np.sqrt(normOneM*normOneInvM * normInfM*normInfInvM)) )
