import subprocess
import math
import sys
import os

from preparegrids import *
from plot_error_vs_angle import plotErrorsVsAngle

if len(sys.argv) < 2:
    sys.exit("Please provide the network index as input argument")

aperture = 2e-3
anisotropyRatio = 5
anisotropyAngle = [math.pi/4.0] #, math.pi/2.0]
fracPermeabilities = ['1e-6', '1e-5', '1e-4', '1e-3', '1e-2', '1', '1e2', '1e3', '1e4', '1e5', '1e6']
subFolder = sys.argv[1].strip('/')

def getFolderName(angle, kf):
    grad = angle*180.0/math.pi
    return subFolder + '/kf_' + kf + '_angle_' + str(int(grad))

for angle in anisotropyAngle:
   for kf in fracPermeabilities:
       folderName = getFolderName(angle, kf)
       if not os.path.exists(folderName):
           subprocess.run("mkdir " + folderName, shell=True, check=True)

# prepare grids as in a refinement study
# refLevel = 5
# gridSpecs = getGridSpecs(aperture, sys.argv[1])
# gridSpecs['dxInitialFacet'] = gridSpecs['dxInitialFacet']/math.pow(2, refLevel)
numRefinements = 5
gridSpecs = getGridSpecs(aperture, sys.argv[1])
prepareGrids("temporary", str(aperture), sys.argv[1], numRefinements, gridSpecs)

# replace the first two with the last two grids to start "convergence test"
# at second finest level
subprocess.run(['cp', f'temporary_{numRefinements}.msh', f'temporary_1.msh'])
subprocess.run(['cp', f'temporary_{numRefinements-1}.msh', f'temporary_0.msh'])

results = []
for angle in anisotropyAngle:
    for kf in fracPermeabilities:
        folderName = getFolderName(angle, kf)
        results.append({'angle': angle, 'kf': kf, 'folder': folderName})

        print("\n" + '*'*50)
        print("Computing errors for for angle = " + str(angle) + " (rad) and kf = " + str(kf) + "\n")
        subprocess.run(['python3', 'scripts/convergencetest.py', '1',
                         str(aperture),
                         str(anisotropyRatio),
                         str(angle),
                         sys.argv[1],
                         kf,
                         "true"], check=True)

        # copy all files in sub-folder
        subprocess.run('mv *vtu *pvd *vtp *csv ' + folderName, shell=True, check=True)

# generate plots
plotErrorsVsAngle(results)
