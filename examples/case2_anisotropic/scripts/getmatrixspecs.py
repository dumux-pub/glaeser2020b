import scipy.io as io
import numpy as np
import sys
import os

if len(sys.argv) < 2:
    sys.stderr.write("Please provide the file name of the matrix to check " \
                     "and the file in which to write the gathered data (optional)")
    sys.exit(1)

M = io.mmread(sys.argv[1]).tocsc()

shape = M.shape
if len(shape) != 2:
    sys.stderr.write("Unexpected matrix shape")
    sys.exit(1)

if shape[0] != shape[1]:
    sys.stderr.write("Expected a square matrix")
    sys.exit(1)

numDofs = int(shape[0])
nnz = int(M.count_nonzero())
sp = float(nnz)/float(numDofs*numDofs)
print('dofs = {:5d}'.format(numDofs))
print('nnz = {:5d}'.format(nnz))
print('nnz/dofs^2 = {:5e}'.format(sp))

if len(sys.argv) > 2:
    outFile = open(sys.argv[2], 'w')
else:
    outFile = open(os.path.splitext(sys.argv[1])[0] + '.csv', 'w')

outFile.write("numdofs,nnz,sparsity\n")
outFile.write(str(numDofs) + ',' + str(nnz) + ',' + str(sp));
