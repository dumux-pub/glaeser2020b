close all
clear all
clc

arg_list = argv ();
A = mmread(arg_list{1});

A_dof = size(A, 1);
['dof ' num2str(A_dof)]

A_nnz = nnz(A);
['nnz ' num2str(A_nnz)]

A_sparsity = A_nnz/A_dof^2;
['nnz/dof^2 ' num2str(A_sparsity)]

A_cond = condest(A);
['cond2 ' num2str(A_cond, "%5.1e")]

fid = fopen(arg_list{2}, "w");
fdisp(fid, "ndof,nnz,sparsity,condest");
fdisp(fid, [num2str(A_dof) ',' num2str(A_nnz) ',' num2str(A_sparsity) ',' num2str(A_cond)]);
fclose(fid);
