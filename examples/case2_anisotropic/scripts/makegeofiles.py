import subprocess
import argparse
import csv
import sys
import os
import numpy as np

# we require Python3
if sys.version_info[0] < 3:
    sys.exit("Python 3 required\n")

if len(sys.argv) < 4:
    print(len(sys.argv))
    sys.exit("Please provide the target geo file name, the network index and the aperture\n")

# define fracture corners
if sys.argv[2] == '1':
    fractures = [
    [[-0.25, -0.15], [0.25,  0.15]]
    ]

elif sys.argv[2] == '2':
    fractures = [
    [[-0.25, -0.15], [0.25,  0.15]],
    [[-0.25,  0.15], [0.25, -0.15]]
    ]

elif sys.argv[2] == '3':
    fractures = [
    [[-0.25, -0.35], [-0.15,  0.35]],
    [[-0.35,  0.15], [0.35,  -0.15]],
    [[0.25,  -0.35], [0.14,   0.4]],
    [[0.0,   0.4],  [0.4,    0.2]]
    ]

elif sys.argv[2] == 'interfacetest':
    fractures = [
    [[-0.35, 0.15], [0.35, -0.15]]
    ]

else:
    sys.exit("Invalid network index (" + sys.argv[2] + ") provided. Choose from [1, 2, 3, interfacetest]\n")

a = float(sys.argv[3])
defaultDxRef = "aperture/8.0"
defaultDxFacet = "aperture/2.0"
defaultDxRatio = 0.025

geoFileName = os.path.splitext(sys.argv[1])[0]
geoFile = open(geoFileName + ".geo", 'w')
geoFileRef = open(geoFileName + '_reference.geo', 'w')

# write everything up to the domain definition
for file, dxString in zip([geoFile, geoFileRef], [defaultDxFacet, defaultDxRef]):
    file.write("SetFactory(\"OpenCASCADE\");\n")
    file.write("\n")
    file.write("DefineConstant[ aperture = " + str(a) + " ];\n")
    file.write("DefineConstant[ dxFracture = " + dxString + " ];\n")
    file.write("DefineConstant[ dxRatio = 0.025 ];\n")
    file.write("\n")
    file.write("// domain corners\n")
    file.write("dxBulk = dxFracture/dxRatio;\n")
    file.write("Point(0) = {-0.5, -0.5, 0.0, dxBulk};\n")
    file.write("Point(1) = { 0.5, -0.5, 0.0, dxBulk};\n")
    file.write("Point(2) = { 0.5,  0.5, 0.0, dxBulk};\n")
    file.write("Point(3) = {-0.5,  0.5, 0.0, dxBulk};\n")
    file.write("\n")
    file.write("// domain plane\n")
    file.write("Line(1) = {0, 1}; Line(2) = {1, 2};\n")
    file.write("Line(3) = {2, 3}; Line(4) = {3, 0};\n")
    file.write("Curve Loop(1) = {1, 2, 3, 4};\n")
    file.write("Plane Surface(1) = {1};\n")
    file.write("\n")

# write fractures to hybrid-dimensional .geo file
lineIdx = 5
for fracture in fractures:
    geoFile.write("p1 = newp; Point(newp) = {" + str(fracture[0][0])
                                               + ", " + str(fracture[0][1]) + ", 0.0, dxFracture};\n")
    geoFile.write("p2 = newp; Point(newp) = {" + str(fracture[1][0]) + ", "
                                               + str(fracture[1][1]) + ", 0.0, dxFracture};\n")
    geoFile.write("Line(" + str(lineIdx) + ") = {p1, p2};\n\n")
    lineIdx += 1

# cut the lines and fragment everything
geoFile.write("cutLines[] = BooleanIntersection{ Line{5:" + str(lineIdx-1) + "}; Delete; }{ Surface{1}; };\n")
if len(fractures) > 1:
    geoFile.write("fragmentLines[] = BooleanFragments{ Line{cutLines[]}; Delete; }{ Line{cutLines[]}; Delete; };\n")
    geoFile.write("BooleanFragments{ Surface{1}; Delete; }{ Line{fragmentLines[]}; }\n")
    geoFile.write("Physical Surface(1) = {Surface{:}};\n")
    geoFile.write("\n")
    geoFile.write("fragmentLinesUnique[] = Unique(fragmentLines[]);\n")
    geoFile.write("For i In {0:#fragmentLinesUnique[]-1}\n")
    geoFile.write("    Printf(\"i = \", i);\n")
    geoFile.write("    Printf(\"idx = \", fragmentLinesUnique[i]);\n")
    geoFile.write("    Physical Line(i+1) = { fragmentLinesUnique[i] };\n")
    geoFile.write("EndFor\n")
    geoFile.write("Characteristic Length{PointsOf{ Line{fragmentLinesUnique[]}; }} = dxFracture;\n")
else:
    geoFile.write("BooleanFragments{ Surface{1}; Delete; }{ Line{cutLines[]}; }\n")
    geoFile.write("Physical Surface(1) = {Surface{:}};\n")
    geoFile.write("\n")
    geoFile.write("cutLinesUnique[] = Unique(cutLines[]);\n")
    geoFile.write("For i In {0:#cutLinesUnique[]-1}\n")
    geoFile.write("    Printf(\"i = \", i);\n")
    geoFile.write("    Printf(\"idx = \", cutLinesUnique[i]);\n")
    geoFile.write("    Physical Line(i+1) = { cutLinesUnique[i] };\n")
    geoFile.write("EndFor\n")
    geoFile.write("Characteristic Length{PointsOf{ Line{cutLinesUnique[]}; }} = dxFracture;\n")

# create and merge surfaces for all fractures
surfIdx = 2
for fracture in fractures:
    p1Raw = np.array(fracture[0])
    p2Raw = np.array(fracture[1])

    d = p2Raw - p1Raw
    n = np.array([d[1], -d[0]])
    n *= a/2.0

    p1 = p1Raw - n; p2 = p1Raw + n;
    p3 = p2Raw - n; p4 = p2Raw + n;

    geoFileRef.write("\n// fracture plane\n")
    geoFileRef.write("p1 = newp; Point(p1) = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0, dxFracture};\n")
    geoFileRef.write("p2 = newp; Point(p2) = {" + str(p3[0]) + ", " + str(p3[1]) + ", 0.0, dxFracture};\n")
    geoFileRef.write("p3 = newp; Point(p3) = {" + str(p4[0]) + ", " + str(p4[1]) + ", 0.0, dxFracture};\n")
    geoFileRef.write("p4 = newp; Point(p4) = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0, dxFracture};\n")
    geoFileRef.write("l1 = newl; Line(l1) = {p1, p2};\n")
    geoFileRef.write("l2 = newl; Line(l2) = {p2, p3};\n")
    geoFileRef.write("l3 = newl; Line(l3) = {p3, p4};\n")
    geoFileRef.write("l4 = newl; Line(l4) = {p4, p1};\n")
    geoFileRef.write("ll = newll; Line Loop(ll) = {l1, l2, l3, l4};\n")
    geoFileRef.write("Plane Surface(" + str(surfIdx) + ") = {ll};\n")
    surfIdx += 1

    # for each fracture, make a .geo file that produces a structured mesh with a
    # single cell discretizing the width of the fracture. This can be used to compute
    # cross-section averaged solutions from the equi-dimensional reference solution.
    # We start with index 5 again in order to match the indices in the mixed-dimensional grid
    with open(geoFileName + '_frac_' + str(surfIdx-2) + '.geo', 'w') as fracFile:
        longLength = np.linalg.norm(p3-p1)
        fracFile.write("DefineConstant[ aperture = " + str(a) + " ];\n")
        fracFile.write("DefineConstant[ dxFracture = " + defaultDxRef + " ];\n")
        fracFile.write("\n// produces a structured grid for fracture with index " + str(lineIdx) + "\n")
        fracFile.write("numPointsShort = 2;\n")
        fracFile.write("numPointsLong = Floor({:5f}/dxFracture) + 1;\n".format(longLength))
        fracFile.write("p1 = newp; Point(p1) = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0};\n")
        fracFile.write("p2 = newp; Point(p2) = {" + str(p3[0]) + ", " + str(p3[1]) + ", 0.0};\n")
        fracFile.write("p3 = newp; Point(p3) = {" + str(p4[0]) + ", " + str(p4[1]) + ", 0.0};\n")
        fracFile.write("p4 = newp; Point(p4) = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0};\n")
        fracFile.write("l1 = newl; Line(l1) = {p1, p2}; Transfinite Line{l1} = numPointsLong;\n")
        fracFile.write("l2 = newl; Line(l2) = {p2, p3}; Transfinite Line{l2} = numPointsShort;\n")
        fracFile.write("l3 = newl; Line(l3) = {p3, p4}; Transfinite Line{l3} = numPointsLong;\n")
        fracFile.write("l4 = newl; Line(l4) = {p4, p1}; Transfinite Line{l4} = numPointsShort;\n")
        fracFile.write("ll = newll; Line Loop(ll) = {l1, l2, l3, l4};\n")
        fracFile.write("Plane Surface(1) = {ll};\n")
        fracFile.write("Transfinite Surface{1};\n")
        fracFile.write("Recombine Surface{1};\n")

geoFileRef.write("\n// confine the fractures to the domain\n")
geoFileRef.write("cutFracPlanes[] = BooleanIntersection{ Surface{2:" + str(surfIdx-1) + "}; Delete; }{ Surface{1}; };\n")

if len(fractures) > 1:
    geoFileRef.write("fragmentSurfs[] = BooleanFragments{ Surface{cutFracPlanes[]}; Delete; }{ Surface{cutFracPlanes[]}; Delete; };\n")
    geoFileRef.write("domain[] = BooleanDifference{ Surface{1}; Delete; }{ Surface{fragmentSurfs[]}; };\n")
    geoFileRef.write("Physical Surface(1) = {domain[]};\n")
    geoFileRef.write("\n")
    geoFileRef.write("fragmentSurfsUnique[] = Unique(fragmentSurfs[]);\n")
    geoFileRef.write("For i In {0:#fragmentSurfsUnique[]-1}\n")
    geoFileRef.write("    Printf(\"i = \", i);\n")
    geoFileRef.write("    Printf(\"idx = \", fragmentSurfsUnique[i]);\n")
    geoFileRef.write("    Physical Surface(i+2) = { fragmentSurfsUnique[i] };\n")
    geoFileRef.write("EndFor\n")
    geoFileRef.write("Characteristic Length{PointsOf{ Surface{fragmentSurfsUnique[]}; }} = dxFracture;\n")
else:
    geoFileRef.write("domain[] = BooleanDifference{ Surface{1}; Delete; }{ Surface{cutFracPlanes[]}; };\n")
    geoFileRef.write("Physical Surface(1) = {domain[]};\n")
    geoFileRef.write("\n")
    geoFileRef.write("cutFracPlanesUnique[] = Unique(cutFracPlanes[]);\n")
    geoFileRef.write("For i In {0:#cutFracPlanesUnique[]-1}\n")
    geoFileRef.write("    Printf(\"i = \", i);\n")
    geoFileRef.write("    Printf(\"idx = \", cutFracPlanesUnique[i]);\n")
    geoFileRef.write("    Physical Surface(i+2) = { cutFracPlanesUnique[i] };\n")
    geoFileRef.write("EndFor\n")
    geoFileRef.write("Characteristic Length{PointsOf{ Surface{cutFracPlanesUnique[]}; }} = dxFracture;\n")
