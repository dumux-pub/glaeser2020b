import sys
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

# this expects the angle (string), frac perm (string), the folders with the results and the id of the network on which the results were computed
if len(sys.argv) != 5: sys.exit("Expected four arguments (see comment)")
angle = sys.argv[1]
perm = sys.argv[2]
folder = sys.argv[3].strip('/')
netIndex = int(sys.argv[4])

for d in ['1', '2']:
    with open('errortable' + d + '.tex', 'w') as tableFile:

        head = r'\head_' + d

        # write header
        tableFile.write(r'\begin{table}[h]' + '\n' \
                        r'  {\footnotesize' + '\n' \
                        r'  \centering' + '\n' \
                        r'  \caption{\textbf{Case 2 - errors and rates of $' + head + '$ for $\permAngle = ' + angle + r'$ and $k = \num{' + perm + r'}$}. Listed are the errors $\errorNorm_{' + head + '}$ and the corresponding rates $r_{' + head + r'}$ over grid refinement, expressed in terms of $\discLength_\fracIdx$.}' + '\n' \
                        r'  \begin{tabular}{ *{2}{l} | *{2}{l} | *{2}{l} | *{2}{l} | *{2}{l} | *{2}{l} }' + '\n' \
                        r'  \toprule' + '\n' \
                        r'  & & \multicolumn{2}{l}{\tpfaDfm} & \multicolumn{2}{l}{\mpfaDfm} & \multicolumn{2}{l}{\eboxDfm} & \multicolumn{2}{l}{\eboxMortarDfm} & \multicolumn{2}{l}{\boxDfm} \\' + '\n' \
                        r'  &  $\discLength_\fracIdx$ ' \
                        r'& $\errorNorm_{' + head + '}$ & $r_{' + head + r'}$ ' \
                        r'& $\errorNorm_{' + head + '}$ & $r_{' + head + r'}$ ' \
                        r'& $\errorNorm_{' + head + '}$ & $r_{' + head + r'}$ ' \
                        r'& $\errorNorm_{' + head + '}$ & $r_{' + head + r'}$ ' \
                        r'& $\errorNorm_{' + head + '}$ & $r_{' + head + r'}$ \\ \midrule' + '\n'
                       )

        if d == '2': norms = np.genfromtxt(folder + "/errornorms_bulk.csv", delimiter=',', names=True)
        else: norms = np.genfromtxt(folder + "/errornorms_facet.csv", delimiter=',', names=True)

        h = norms['h']
        tableFile.write(r'\multirow{' + str(len(h)) + r'}{*}{$\fracNet_' + str(netIndex+1) + r'$}')

        for i in range(0, len(h)):
            tableFile.write(' & {:.2e}'.format(h[i]))
            for key in ['tpfa', 'mpfa', 'box', 'boxmortar', 'boxcont']:
                tableFile.write(' & {:.2e}'.format(norms[key][i]))
                if i > 0:
                    rate = (np.log(norms[key][i])-np.log(norms[key][i-1]))/(np.log(h[i]) - np.log(h[i-1]))
                    tableFile.write(' & {:.2e}'.format(rate))
                else:
                    tableFile.write(r' & ')
            tableFile.write(r' \\' + '\n')

        tableFile.write(r'  \bottomrule' + '\n')
        tableFile.write(r'  \end{tabular}' + '\n' \
                        r'  \label{tab:convDiscreteErrors}' + '\n' \
                        r'  }' + '\n' \
                        r'\end{table}' + '\n')
