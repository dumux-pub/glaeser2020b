import sys
import math
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

def toDegreesFloor(angle):
    deg = int(angle*180.0/math.pi)
    return deg + 1 if deg > 0 else 0

def toRadiansString(angle):
    if angle == 0: return "0"
    quotient = round(180.0/angle)
    return r"\pi/{:d}".format(quotient)

def plotErrorsVsAngle(results, imgFolder = './'):

    # obtain all plot data
    plotData = {}
    angles = []
    for result in results:
        folder = result['folder']
        angle = result['angle']
        kf = result['kf']

        key = str(toDegreesFloor(angle))
        if key not in angles: angles.append(key)
        if key not in plotData: plotData[key] = {}

        if 'fracPerms' not in plotData[key]: plotData[key]['fracPerms'] = np.array([])
        plotData[key]['fracPerms'] = np.append(plotData[key]['fracPerms'], float(kf))

        bulkErrors = np.genfromtxt(folder + "/errornorms_bulk.csv", delimiter=',', names=True)
        facetErrors = np.genfromtxt(folder + "/errornorms_facet.csv", delimiter=',', names=True)

        mpfaSchemeId = getSchemeId('mpfa')
        tpfaSchemeId = getSchemeId('tpfa')
        boxSchemeId = getSchemeId('box')
        boxMortarSchemeId = getSchemeId('box-mortar')
        boxContSchemeId = getSchemeId('box-cont')

        if 'bulkErrors' not in plotData[key]: plotData[key]['bulkErrors'] = {}
        if 'facetErrors' not in plotData[key]: plotData[key]['facetErrors'] = {}

        for schemeName in names:
            if getSchemeId(schemeName) not in plotData[key]['bulkErrors']:
                plotData[key]['bulkErrors'][getSchemeId(schemeName)] = np.array([])
            if getSchemeId(schemeName) not in plotData[key]['facetErrors']:
                plotData[key]['facetErrors'][getSchemeId(schemeName)] = np.array([])
            plotData[key]['bulkErrors'][getSchemeId(schemeName)] = np.append(plotData[key]['bulkErrors'][getSchemeId(schemeName)], bulkErrors[schemeName.replace('-', '')])
            plotData[key]['facetErrors'][getSchemeId(schemeName)] = np.append(plotData[key]['facetErrors'][getSchemeId(schemeName)], facetErrors[schemeName.replace('-', '')])

    # sort angles
    def getAngleFloat(angleString):
        return float(angleString)

    # make plots
    numAngles = len(angles)
    angles.sort(key=getAngleFloat)
    fig_bulk = plt.figure(1, figsize = [12.0, 3.5])
    fig_facet = plt.figure(2, figsize = [12.0, 3.5])

    axes_bulk = []
    axes_facet = []
    for angle in angles:
        figIndex = angles.index(angle) + 1
        axes_bulk.append(fig_bulk.add_subplot(1, numAngles, figIndex))
        axes_facet.append(fig_facet.add_subplot(1, numAngles, figIndex))

    for key, data in plotData.items():

        xAxis = data['fracPerms']
        indexMap = np.argsort(xAxis)

        ax_bulk = axes_bulk[angles.index(key)]
        if angles.index(key) > 0:
            ax_bulk.yaxis.set_tick_params(length=0)
        else:
            ax_bulk.set_ylabel(r'$\varepsilon_{h_2}$')
        ax_bulk.set_xlabel(r'$k$')

        ax_bulk.set_title(r'$\Theta = {}$'.format(toRadiansString(float(key))))
        for schemeName in names:
            schemeId = getSchemeId(schemeName)
            ax_bulk.loglog(xAxis[indexMap], data['bulkErrors'][schemeId][indexMap],
                           marker='*', label=labels[schemeId], color=colors[schemeId], linestyle=linestyles[schemeId])
        if angles.index(key) == len(angles) - 1:
            ax_bulk.legend(loc='upper left', bbox_to_anchor=(0.8, 1.0))
        ax_bulk.set_ylim([0.001, 0.5])

        ax_facet = axes_facet[angles.index(key)]
        if angles.index(key) > 0:
            ax_facet.yaxis.set_tick_params(length=0)
        else:
            ax_facet.set_ylabel(r'$\varepsilon_{h_1}$')
        ax_facet.set_xlabel(r'$k$')

        ax_facet.set_title(r'$\Theta = {}$'.format(toRadiansString(float(key))))
        for schemeName in names:
            schemeId = getSchemeId(schemeName)
            ax_facet.loglog(xAxis[indexMap], data['facetErrors'][schemeId][indexMap],
                            marker='*', label=labels[schemeId], color=colors[schemeId], linestyle=linestyles[schemeId])
        if angles.index(key) == len(angles) - 1:
            ax_facet.legend(loc='upper left', bbox_to_anchor=(0.8, 1.0))
        ax_facet.set_ylim([2e-5, 3.2e-1])


    plt.figure(1)
    plt.savefig(imgFolder + "/anglecomp_bulk.pdf", bbox_inches='tight')
    plt.figure(2)
    plt.savefig(imgFolder + "/anglecomp_facet.pdf", bbox_inches='tight')

if __name__ == '__main__':
    # main body of the script
    subFolder = sys.argv[1]
    anisotropyAngle = [0.0, math.pi/4.0, math.pi/2.0]
    fracPermeabilities = ['1e-6', '1e-5', '1e-4', '1e-3', '1e-2', '1', '1e2', '1e3', '1e4', '1e5', '1e6']

    results = []
    for angle in anisotropyAngle:
        for kf in fracPermeabilities:
            folderName = subFolder + '/kf_' + kf + '_angle_' + str(int(angle*180.0/math.pi))
            results.append({'angle': angle, 'kf': kf, 'folder': folderName})

    plotErrorsVsAngle(results)
