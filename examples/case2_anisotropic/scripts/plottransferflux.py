import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

if len(sys.argv) < 3:
    sys.exit("Not all mandatory runtime arguments were provided")
if sys.argv[2].lower() not in ['lo', 'hi']:
    sys.exit("2nd argument must be either 'lo' or 'hi'")

folder = sys.argv[1]
fracSideSuffix = 'lo' if sys.argv[2].lower() == 'lo' else 'hi'
refinement = sys.argv[3]
fracIndicesFacet = ['1'] # ['3', '4', '5']# net 3 vertical right['6', '7', '8'] # net 2 ['1', '2']    # net 3 horizontal['3', '4', '5']
fracIndicesRef =   ['2'] # ['5', '6', '8']# net 3 vertical right['9', '10', '12'] # net 2 ['2', '4']   # net3 horizontal ['5', '6', '8']
xAxisKey = 'x'

plt.figure(1)

# refence plot
miny =  1e20
maxy = -1e20
for index, fracIdxRef in enumerate(fracIndicesRef):
    refDataFileName = 'reference_data_' + fracSideSuffix + '_' + fracIdxRef + '.csv'
    refData = np.genfromtxt(os.path.join(folder, refDataFileName), names=True, delimiter=',')
    refIndexMap = np.argsort(refData[xAxisKey])
    if index == 0:
        plt.plot(refData[xAxisKey][refIndexMap], refData['flux'][refIndexMap], color=referenceColor, label=referenceLabel)
    else:
        plt.plot(refData[xAxisKey][refIndexMap], refData['flux'][refIndexMap], color=referenceColor)

    miny = min(miny, np.min(refData['flux']))
    maxy = max(maxy, np.max(refData['flux']))

# add plots for schemes
for index, fracIdxFacet in enumerate(fracIndicesFacet):
    for schemeName in names:
        if schemeName in ['box-cont', 'tpfa']:
            continue

        schemeId = getSchemeId(schemeName)
        schemeName = schemeName if schemeName != 'box-mortar' else 'mortar'
        dataFileName = schemeName + '_bulk_data_' + refinement + '_' + fracIdxFacet + '_' + fracSideSuffix + '.csv'
        data = np.genfromtxt(os.path.join(folder, dataFileName), names=True, delimiter=',')
        indexMap = np.argsort(data[xAxisKey])
        if index != 0:
            plt.plot(data[xAxisKey][indexMap], data['flux'][indexMap], color=colors[schemeId], linestyle=linestyles[schemeId])
        else:
            plt.plot(data[xAxisKey][indexMap], data['flux'][indexMap], color=colors[schemeId], label=labels[schemeId], linestyle=linestyles[schemeId])



plt.xlabel(r'$x$')
plt.ylabel(r'$\lambda |_{\gamma_1}$')
plt.xlim([-0.151, -0.1475])
# plt.ylim([miny, maxy])
plt.ylim([-10, 35])
plt.legend()
plt.savefig(os.path.join('.', 'flux_frac_' + fracSideSuffix + '.pdf'), bbox_inches='tight')
plt.xlim([-0.25, 0.2])
plt.ylim([0.0, 2])
plt.legend('')
plt.xlabel('')
plt.ylabel('')
plt.savefig(os.path.join('.', 'flux_frac_' + fracSideSuffix + '_closeup.pdf'), bbox_inches='tight')
