import os
import sys
import subprocess
from preparegrids import *

# define a function to exit with error message
def printErrorAndExit(errorMessage):
    sys.stderr.write(errorMessage + "\n")
    sys.exit(1)

# we require Python3
if sys.version_info[0] < 3:
    printErrorAndExit("Python 3 required\n");

# retrieve user input
numRefinements = int(sys.argv[1])
aperture = float(sys.argv[2])
anisotropyRatio = float(sys.argv[3])
anisotropyAngle = float(sys.argv[4])
networkIndex = sys.argv[5]
kf = sys.argv[6]
geoFile = "temporary"

# check if user specified to skip grid preparation
try: skipGridGeneration = sys.argv[7].lower() in ('yes', 'true', '1')
except: skipGridGeneration = False

# make sure the convergence executable is up to date
try: subprocess.run(['make', 'case2_convergence_mixeddim_1p_anisotropic'], check=True)
except: printErrorAndExit("Could not build executable\n")

gridSpecs = getGridSpecs(aperture, networkIndex)

# we use a constant grid size throughout the domain and make the grid coarser
# than the default obtained by "getGridSpecs" in order for the computational cost to be acceptable
gridSpecs['dxInitialFacet'] = gridSpecs['dxInitialFacet']*2
gridSpecs['dxRatioFacet'] = 1.0

# prepare the grids
if skipGridGeneration:
    print("Skipping grid generation!")
else:
    prepareGrids(geoFile, sys.argv[2], networkIndex, numRefinements, gridSpecs)

# obtain the indices of the prepared fracture grids
fracIndices = []
for item in os.listdir():
    if os.path.isfile(item) and geoFile + "_frac_" in item and os.path.splitext(item)[1] == '.geo':
        index = item.replace(geoFile + "_frac_", "")
        index = index.replace(".geo", "")
        fracIndices.append(int(index))
print("Found fractures with indices: ", fracIndices)

print("Removing old .vtu, .vtp, .pdv and .csv files")
subprocess.call("rm *vtu *vtp *pvd *csv", shell=True)

# prepare runtime arguments for convergence test
callArgs = ['./case2_convergence_mixeddim_1p_anisotropic']
callArgs.append("params.input")

callArgs.append('-ConvergenceTest.NumRefinements')
callArgs.append(str(numRefinements-1))

callArgs.append('-Reference.Grid.File')
callArgs.append(geoFile + '_reference.msh')

callArgs.append('-Reference.FractureGridsBaseName')
callArgs.append( 'temporary_frac')
callArgs.append('-Reference.FractureGridIndices')
callArgs.append(' '.join([str(idx) for idx in fracIndices]))

callArgs.append('-Facet.SpatialParams.Aperture')
callArgs.append(str(aperture))

callArgs.append('-Bulk.SpatialParams.PermeabilityAnisotropyRatio')
callArgs.append(str(anisotropyRatio))
callArgs.append('-Bulk.SpatialParams.PermeabilityAngle')
callArgs.append(str(anisotropyAngle))

callArgs.append('-Facet.SpatialParams.Permeability')
callArgs.append(kf)

for refIdx in range(0, numRefinements+1):
    callArgs.append('-Refinement_' + str(refIdx) + '.Grid.File')
    callArgs.append(geoFile + "_" + str(refIdx) + '.msh')

    callArgs.append('-Refinement_' + str(refIdx) + '.Mortar.Grid.File')
    callArgs.append(geoFile + '_mortar_' + str(refIdx) + '.msh')

# call dumux
try: subprocess.run(callArgs, check=True)
except: printErrorAndExit("Error during Dumux run\n")
