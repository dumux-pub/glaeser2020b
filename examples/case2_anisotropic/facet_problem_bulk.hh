// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the single-phase
 *        test considering anisotropic permeabilities.
 */
#ifndef DUMUX_GLAESER_ANISOTROPIC_FACET_BULK_PROBLEM_HH
#define DUMUX_GLAESER_ANISOTROPIC_FACET_BULK_PROBLEM_HH

#include <utility>

#include <dune/common/exceptions.hh>

#include <dumux/common/math.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "boundaryconditions.hh"

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the single-phase
 *        test considering anisotropic permeabilities.
 */
template<class TypeTag>
class OnePBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePBulkProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup)
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        static const bool useIntDirichlet = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseInteriorDirichletBCs", false);

        BoundaryTypes values;
        if (useIntDirichlet) values.setAllDirichlet();
        else values.setAllNeumann();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return AnisotropicTestBCs::bulkDirichletAtPos<PrimaryVariables>(globalPos); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    // writes the transfer fluxes to disk
    template<class BindContextFunc, class GridVariables, class SolutionVector>
    void writeCsvData(const std::string& csvFileBody,
                      BindContextFunc&& bindContext,
                      const GridVariables& gridVars,
                      const SolutionVector& x) const
    {
        std::vector<int> visitedFracs;

        // lambda to write the header to a plot file
        auto writeHeader = [&] (auto& stream)
        {
            if constexpr (GridGeometry::discMethod == DiscretizationMethod::box)
                stream << "\"x\",\"y\",\"flux\",\"kgradp\",\"bulkpressure\",\"facetpressure\"" << std::endl;
            else
                stream << "\"x\",\"y\",\"flux\"" << std::endl;
        };

        using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

            bindContext(element);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (!couplingManagerPtr_->isOnInteriorBoundary(element, scvf))
                    continue;

                const auto facetElement = couplingManagerPtr_->getLowDimElement(element, scvf);
                const auto facetElemMarker = couplingManagerPtr_->problem(Dune::index_constant<1>()).spatialParams().getDomainMarker(facetElement);

                const std::string fileNameLo = csvFileBody + "_" + std::to_string(facetElemMarker) + "_lo.csv";
                const std::string fileNameHi = csvFileBody + "_" + std::to_string(facetElemMarker) + "_hi.csv";

                if (!std::any_of(visitedFracs.begin(),
                                 visitedFracs.end(),
                                 [&] (auto marker) { return marker == facetElemMarker; }))
                {
                    std::ofstream fileLo(fileNameLo, std::ios::out); writeHeader(fileLo);
                    std::ofstream fileHi(fileNameHi, std::ios::out); writeHeader(fileHi);
                    visitedFracs.push_back(facetElemMarker);
                }

                std::ofstream fileLo(fileNameLo, std::ios::app);
                std::ofstream fileHi(fileNameHi, std::ios::app);

                using std::abs;
                const auto nx = scvf.unitOuterNormal()[0];
                const auto ny = scvf.unitOuterNormal()[1];
                const bool isLowerInterface = abs(ny) > 1e-6 ? ny >= 0.0 : nx >= 0.0;
                const bool isUpperInterface = abs(ny) > 1e-6 ? ny < 0.0 : nx < 0.0;

                FluxVars fv;
                fv.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                auto up = [] (const auto& vv) { return vv.mobility(); };
                Scalar flux = fv.advectiveFlux(/*phaseIdx*/0, up);

                // for box, plot -K/mu*gradP and pressure as well
                if constexpr (GridGeometry::discMethod == DiscretizationMethod::box)
                {
                    const auto& cache = elemFluxVarsCache[scvf];

                    Scalar p = 0.0;
                    GlobalPosition gradP(0.0);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        auto tmp = cache.gradN(scv.indexInElement());
                        tmp *= elemVolVars[scv].pressure();
                        gradP += tmp;
                        p += elemVolVars[scv].pressure()
                             *cache.shapeValues()[scv.indexInElement()];
                    }

                    const auto& K = elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].permeability();
                    auto permGradP = -1.0*vtmv(scvf.unitOuterNormal(), K, gradP);
                    permGradP /= elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].viscosity();

                    const auto& facetPressure = couplingManager().getLowDimVolVars(element, scvf).pressure();

                    if (isLowerInterface) { fileLo << scvf.center()[0] << "," << scvf.center()[1] << "," << flux/scvf.area() << ", " << permGradP << ", " << p << ", " << facetPressure << "\n"; }
                    else if (isUpperInterface) { fileHi << scvf.center()[0] << "," << scvf.center()[1] << "," << flux/scvf.area() << ", " << permGradP << ", " << p << ", " << facetPressure << "\n"; }
                    else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
                }
                else
                {
                    if (isLowerInterface) { fileLo << scvf.center()[0] << "," << scvf.center()[1] << ","<< flux/scvf.area() << "\n"; }
                    else if (isUpperInterface) { fileHi << scvf.center()[0] << "," << scvf.center()[1] << "," << flux/scvf.area() << "\n"; }
                    else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
                }
            }
        }
    }

    // functions we need for compatibility with generic solver
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
};

} // end namespace Dumux

#endif
