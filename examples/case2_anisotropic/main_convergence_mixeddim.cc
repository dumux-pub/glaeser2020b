// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase flow test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <dune/common/unused.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/io/grid/gridmanager.hh>

#include "box_cont_problem.hh"
#include "facet_problem_bulk.hh"
#include "facet_problem_facet.hh"
#include "reference_problem.hh"

#include <dumux/common/parameters.hh>
#include <dumux/common/integrate.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/discretization/projection/projector.hh>
#include <dumux/discretization/cellcentered/tpfa/fvgridgeometry.hh>

#include <examples/common/properties_facet.hh>
#include <examples/common/properties_reference.hh>

#include <examples/common/solver_reference.hh>
#include <examples/common/solver_box_cont.hh>
#include <examples/common/solver_mortar_2domain.hh>
#include <examples/common/solver_facet.hh>


// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    DUNE_UNUSED const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    const auto nr = getParam<int>("ConvergenceTest.NumRefinements") + 1;
    const auto referenceRefSuffix = std::to_string(nr);
    std::cout << "\n\nSolving Reference using refinement "
              << referenceRefSuffix
              << "\n" << std::endl;

    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Refinement_" + referenceRefSuffix;
    tree["Bulk.ParamGroup"] = "Bulk";
    tree["Facet.ParamGroup"] = "Facet";
    tree["Bulk.IO.VtkName"] = "reference_bulk" ;
    tree["Facet.IO.VtkName"] = "reference_facet";
    tree["Bulk.IO.CsvFileNameBody"] = "reference_bulk_data";
    tree["Facet.IO.CsvFileNameBody"] = "reference_facet_data";

    using namespace Dune::Functions;
    using namespace Properties::TTag;
    auto referenceData = solveFacet<OnePBulkMpfa, OnePFacetMpfa>(tree);
    const auto& refBulkGG = *referenceData.bulkGridGeometry;
    const auto& refFacetGG = *referenceData.facetGridGeometry;
    const auto refBulkBasis = getFunctionSpaceBasis(refBulkGG);
    const auto refFacetBasis = getFunctionSpaceBasis(refFacetGG);
    const auto& refBulkSolution = referenceData.bulkSolution;
    const auto& refFacetSolution = referenceData.facetSolution;

    using BlockType = Dune::FieldVector<double, 1>;
    const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, refBulkSolution);
    const auto refFacetGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, refFacetSolution);
    const auto refBulkIntegral = integrateL2Error(
            refBulkBasis.gridView(), refBulkGF,
            makeAnalyticGridViewFunction(
                [] (const auto& x) { return BlockType(0.0); }, refBulkBasis.gridView()
            ),
            0
    );
    const auto refFacetIntegral = integrateL2Error(
            refFacetBasis.gridView(), refFacetGF,
            makeAnalyticGridViewFunction(
                [] (const auto& x) { return BlockType(0.0); }, refFacetBasis.gridView()
            ),
            0
    );

    std::cout << "Bulk solution integral: " << refBulkIntegral << std::endl;
    std::cout << "Facet solution integral: " << refFacetIntegral << std::endl;

    // run the different schemes for all refinements and compute error norm
    std::vector<double> discretizationLengths;
    std::vector<double> tpfaNormsBulk, tpfaNormsFacet;
    std::vector<double> mpfaNormsBulk, mpfaNormsFacet;
    std::vector<double> boxNormsBulk, boxNormsFacet;
    std::vector<double> boxMortarNormsBulk, boxMortarNormsFacet;
    std::vector<double> boxContNormsBulk, boxContNormsFacet;

    int l2IntOrder = 2;
    for (int refIdx = 0; refIdx <= getParam<int>("ConvergenceTest.NumRefinements"); ++refIdx)
    {
        std::string refSuffix = std::to_string(refIdx);

        // run facet models
        tree["Grid.ParamGroup"] = "Refinement_" + refSuffix;
        tree["Bulk.ParamGroup"] = "Bulk";
        tree["Facet.ParamGroup"] = "Facet";
        tree["Bulk.IO.VtkName"] = "tpfa_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "tpfa_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "tpfa_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "tpfa_facet_data_" + refSuffix;

        // solve and save tpfa data outside sub-scopes to reuse things for box-cont at the end
        std::cout << "\n\nSolving TPFA\n" << std::endl;
        auto tpfaData = solveFacet<OnePBulkTpfa, OnePFacetTpfa>(tree);
        const auto& tpfaBulkGG = *tpfaData.bulkGridGeometry;
        const auto& tpfaFacetGG = *tpfaData.facetGridGeometry;
        const auto& tpfaBulkBasis = getFunctionSpaceBasis(tpfaBulkGG);
        const auto& tpfaFacetBasis = getFunctionSpaceBasis(tpfaFacetGG);

        const auto& tpfaBulkSolution = tpfaData.bulkSolution;
        const auto& tpfaFacetSolution = tpfaData.facetSolution;

        {
            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(tpfaBulkGG, refBulkGG);
                const auto bulkProjector = makeProjector(tpfaBulkBasis, refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(tpfaBulkSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, bulkProjSolution);
                tpfaNormsBulk.push_back(
                    integrateL2Error(refBulkBasis.gridView(), refBulkGF, bulkProjGF, l2IntOrder)
                    /refBulkIntegral
                );
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                const auto facetGlue = makeGlue(tpfaFacetGG, refFacetGG);
                const auto facetProjector = makeProjector(tpfaFacetBasis, refFacetBasis, facetGlue);
                auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto facetProjSolution = facetProjector.project(tpfaFacetSolution, projParams);
                const auto facetProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, facetProjSolution);
                tpfaNormsFacet.push_back(
                    integrateL2Error(refFacetBasis.gridView(), refFacetGF, facetProjGF, l2IntOrder)
                    /refFacetIntegral
                );
            }
        }

        // determine discretization length of this grid
        using std::min;
        double hMin = std::numeric_limits<double>::max();
        for (const auto& element : elements(tpfaBulkGG.gridView()))
            hMin = min(hMin, diameter(element.geometry()));
        discretizationLengths.push_back(hMin);

        tree["Bulk.IO.VtkName"] = "mpfa_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "mpfa_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "mpfa_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "mpfa_facet_data_" + refSuffix;

        { // put this in a scope to save memory
            std::cout << "\n\nSolving MPFA\n" << std::endl;
            auto mpfaData = solveFacet<OnePBulkMpfa, OnePFacetMpfa>(tree);

            const auto& mpfaBulkSolution = mpfaData.bulkSolution;
            const auto& mpfaFacetSolution = mpfaData.facetSolution;
            const auto& mpfaBulkGG = *mpfaData.bulkGridGeometry;
            const auto& mpfaFacetGG = *mpfaData.facetGridGeometry;
            const auto& mpfaBulkBasis = getFunctionSpaceBasis(mpfaBulkGG);
            const auto& mpfaFacetBasis = getFunctionSpaceBasis(mpfaFacetGG);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(mpfaBulkGG, refBulkGG);
                const auto bulkProjector = makeProjector(mpfaBulkBasis, refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(mpfaBulkSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, bulkProjSolution);
                mpfaNormsBulk.push_back(
                    integrateL2Error(refBulkBasis.gridView(), refBulkGF, bulkProjGF, l2IntOrder)
                    /refBulkIntegral
                );
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                const auto facetGlue = makeGlue(mpfaFacetGG, refFacetGG);
                const auto facetProjector = makeProjector(mpfaFacetBasis, refFacetBasis, facetGlue);
                auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto facetProjSolution = facetProjector.project(mpfaFacetSolution, projParams);
                const auto facetProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, facetProjSolution);
                mpfaNormsFacet.push_back(
                    integrateL2Error(refFacetBasis.gridView(), refFacetGF, facetProjGF, l2IntOrder)
                    /refFacetIntegral
                );
            }
        }

        // lambda to create element averaged solutions
        auto makeAveragedSolution = [&] (const auto& gridGeom, const auto& sol, auto& result)
        {
            result.resize(gridGeom.gridView().size(0));
            result = 0.0;
            for (const auto& e : elements(gridGeom.gridView()))
            {
                using GV = std::decay_t<decltype(gridGeom.gridView())>;
                const auto& eg = e.geometry();
                const auto eIdx = gridGeom.elementMapper().index(e);
                const auto elemSol = elementSolution(e, sol, gridGeom);

                auto& rule = Dune::QuadratureRules<double, GV::dimension>::rule(eg.type(), l2IntOrder);
                for (const auto& qp : rule)
                {
                    const auto& ip = eg.global(qp.position());
                    auto tmp = evalSolution(e, eg, elemSol, ip);
                    tmp *= qp.weight();
                    tmp *= eg.integrationElement(qp.position());
                    result[eIdx] += tmp;
                }

                result[eIdx] /= eg.volume();
            }
        };

        tree["Bulk.IO.VtkName"] = "box_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "box_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "box_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "box_facet_data_" + refSuffix;

        { // put this in a scope to save memory
            std::cout << "\n\nSolving BOX\n" << std::endl;
            auto boxData = solveFacet<OnePBulkBox, OnePFacetBox>(tree);
            const auto& boxBulkGG = *boxData.bulkGridGeometry;
            const auto& boxFacetGG = *boxData.facetGridGeometry;

            // make P0 bases from the grids
            using BoxBulkGridView = std::decay_t<decltype(boxBulkGG.gridView())>;
            using BoxFacetGridView = std::decay_t<decltype(boxFacetGG.gridView())>;
            auto boxAvgBulkBasis = LagrangeBasis<BoxBulkGridView, 0>(boxBulkGG.gridView());
            auto boxAvgFacetBasis = LagrangeBasis<BoxFacetGridView, 0>(boxFacetGG.gridView());

            // make element-wise averaged solutions
            auto boxBulkAvgSolution = boxData.bulkSolution;
            auto boxFacetAvgSolution = boxData.facetSolution;
            makeAveragedSolution(boxBulkGG, boxData.bulkSolution, boxBulkAvgSolution);
            makeAveragedSolution(boxFacetGG, boxData.facetSolution, boxFacetAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(boxBulkGG, refBulkGG);
                const auto bulkProjector = makeProjector(boxAvgBulkBasis, refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxBulkAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, bulkProjSolution);
                boxNormsBulk.push_back(
                    integrateL2Error(refBulkBasis.gridView(), refBulkGF, bulkProjGF, l2IntOrder)
                    /refBulkIntegral
                );
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                const auto facetGlue = makeGlue(boxFacetGG, refFacetGG);
                const auto facetProjector = makeProjector(boxAvgFacetBasis, refFacetBasis, facetGlue);
                auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto facetProjSolution = facetProjector.project(boxFacetAvgSolution, projParams);
                const auto facetProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, facetProjSolution);
                boxNormsFacet.push_back(
                    integrateL2Error(refFacetBasis.gridView(), refFacetGF, facetProjGF, l2IntOrder)
                    /refFacetIntegral
                );
            }
        }


        tree["Bulk.IO.VtkName"] = "mortar_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "mortar_facet_" + refSuffix;
        tree["Mortar.ParamGroup"] = "Mortar";
        tree["Mortar.Grid.ParamGroup"] = "Refinement_" + refSuffix + ".Mortar";
        tree["Mortar.IO.VtkName"] = "mortar_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "mortar_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "mortar_facet_data_" + refSuffix;
        tree["Mortar.IO.CsvFileNameBody"] = "mortar_data_" + refSuffix;

        {
            std::cout << "\n\nSolving BOX-MORTAR\n" << std::endl;
            auto mortarData = solveMortar(tree);
            const auto& boxBulkGG = *mortarData.bulkGridGeometry;
            const auto& boxFacetGG = *mortarData.facetGridGeometry;

            // make P0 bases from the grids
            using BoxBulkGridView = std::decay_t<decltype(boxBulkGG.gridView())>;
            using BoxFacetGridView = std::decay_t<decltype(boxFacetGG.gridView())>;
            auto boxAvgBulkBasis = LagrangeBasis<BoxBulkGridView, 0>(boxBulkGG.gridView());
            auto boxAvgFacetBasis = LagrangeBasis<BoxFacetGridView, 0>(boxFacetGG.gridView());

            // make element-wise averaged solutions
            auto boxBulkAvgSolution = mortarData.bulkSolution;
            auto boxFacetAvgSolution = mortarData.facetSolution;
            makeAveragedSolution(boxBulkGG, mortarData.bulkSolution, boxBulkAvgSolution);
            makeAveragedSolution(boxFacetGG, mortarData.facetSolution, boxFacetAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(boxBulkGG, refBulkGG);
                const auto bulkProjector = makeProjector(boxAvgBulkBasis, refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxBulkAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, bulkProjSolution);
                boxMortarNormsBulk.push_back(
                    integrateL2Error(refBulkBasis.gridView(), refBulkGF, bulkProjGF, l2IntOrder)
                    /refBulkIntegral
                );
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                const auto facetGlue = makeGlue(boxFacetGG, refFacetGG);
                const auto facetProjector = makeProjector(boxAvgFacetBasis, refFacetBasis, facetGlue);
                auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto facetProjSolution = facetProjector.project(boxFacetAvgSolution, projParams);
                const auto facetProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, facetProjSolution);
                boxMortarNormsFacet.push_back(
                    integrateL2Error(refFacetBasis.gridView(), refFacetGF, facetProjGF, l2IntOrder)
                    /refFacetIntegral
                );
            }
        }


        tree["IO.VtkName"] = "box_cont_" + refSuffix;
        tree["IO.CsvFileNameBody"] = "box_cont_data_" + refSuffix;

        {
            std::cout << "\n\nSolving BOX-CONT\n" << std::endl;
            auto boxContData = solveBoxCont(tree);
            const auto& gridGeometry = *boxContData.gridGeometry;

            // make P0 bases from the grids
            using BoxContGridView = std::decay_t<decltype(gridGeometry.gridView())>;
            auto boxContAvgBasis = LagrangeBasis<BoxContGridView, 0>(gridGeometry.gridView());
            auto boxContBasis = LagrangeBasis<BoxContGridView, 1>(gridGeometry.gridView());

            // make element-wise averaged solutions
            auto boxContAvgSolution = boxContData.solution;
            makeAveragedSolution(gridGeometry, boxContData.solution, boxContAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(gridGeometry, refBulkGG);
                const auto bulkProjector = makeProjector(boxContAvgBasis, refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxContAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refBulkBasis, bulkProjSolution);
                boxContNormsBulk.push_back(
                    integrateL2Error(refBulkBasis.gridView(), refBulkGF, bulkProjGF, l2IntOrder)
                    /refBulkIntegral
                );
            }

            {
                std::cout << "Obtaining fracture solution via projection" << std::endl;
                const auto facetGlue = makeGlue(gridGeometry, refFacetGG);
                const auto facetProjector = makeProjector(boxContBasis, refFacetBasis, facetGlue);
                auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                auto facetProjSolution = facetProjector.project(boxContData.solution, projParams);

                std::cout << "Compute facet pressure error norm" << std::endl;
                const auto facetProjGF = makeDiscreteGlobalBasisFunction<BlockType>(refFacetBasis, facetProjSolution);
                boxContNormsFacet.push_back(
                    integrateL2Error(refFacetBasis.gridView(), refFacetGF, facetProjGF, l2IntOrder)
                    /refFacetIntegral
                );
            }
        }
    }

    std::cout << "The following parameters were used" << std::endl;
    Parameters::print();

    std::cout << "\n\nWriting error norms into .csv file" << std::endl;
    std::ofstream csvFileBulk("errornorms_bulk.csv", std::ios::out);
    std::ofstream csvFileFacet("errornorms_facet.csv", std::ios::out);

    csvFileBulk << "h,tpfa,mpfa,box,boxmortar,boxcont\n";
    csvFileFacet << "h,tpfa,mpfa,box,boxmortar,boxcont\n";

    for (unsigned int i = 0; i < tpfaNormsBulk.size(); ++i)
    {
        csvFileBulk << discretizationLengths[i] << ","
                    << tpfaNormsBulk[i] << "," << mpfaNormsBulk[i] << ","
                    << boxNormsBulk[i] << ","  << boxMortarNormsBulk[i] << ","
                    << boxContNormsBulk[i] << std::endl;

        csvFileFacet << discretizationLengths[i] << ","
                     << tpfaNormsFacet[i] << "," << mpfaNormsFacet[i] << ","
                     << boxNormsFacet[i] << ","  << boxMortarNormsFacet[i] << ","
                     << boxContNormsFacet[i] << std::endl;
    }

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
