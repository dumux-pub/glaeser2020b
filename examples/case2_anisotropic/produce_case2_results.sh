#!/bin/bash

runAndPlot() {
    PERM=$1
    APERTURE="2e-3"
    RESULTS_FOLDER="results_k_${PERM}"

    # clear old results
    rm *pdf *vtu *vtp *pvd *csv

    # run convergence test for k=1e-4 and store results
    python3 scripts/convergencetest.py 5 ${APERTURE} 5 0.785398163 3 ${PERM}
    mkdir -p ${RESULTS_FOLDER}
    python3 scripts/plotrates.py ${APERTURE} "."
    mv *pdf *vtu *vtp *pvd *csv ${RESULTS_FOLDER}
}

runAndPlot 1e-4
runAndPlot 1e4
