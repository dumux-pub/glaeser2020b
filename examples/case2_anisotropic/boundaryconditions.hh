// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Defines the boundary conditions for the single-phase
 *        test considering anisotropic permeabilities.
 */
#ifndef DUMUX_GLAESER_ANISOTROPIC_BOUNDARY_CONDITIONS_HH
#define DUMUX_GLAESER_ANISOTROPIC_BOUNDARY_CONDITIONS_HH

#include <cmath>

#include <dune/common/exceptions.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/discretization/elementsolution.hh>

namespace Dumux {
namespace AnisotropicTestBCs {

    //! evaluates the Dirichlet boundary condition for a given position
    template<class PrimaryVariables, class GlobalPosition>
    PrimaryVariables bulkDirichletAtPos(const GlobalPosition& globalPos)
    {
        using std::cos;
        const auto x = globalPos[0];
        const auto y = globalPos[1];
        return (1.0 - (x+0.5)*(x+0.5))*(1.0 + cos(M_PI*(y)));
    }

} // end namespace AnisotropicTestBCs
} // end namespace Dumux

#endif
