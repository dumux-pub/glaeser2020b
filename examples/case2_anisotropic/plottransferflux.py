import sys
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
import plotstyles

# data_box_lo = np.loadtxt("box_bulk_data_5_lo.csv", delimiter=',')
# boxOrder_lo = np.argsort(data_box_lo[:, 0])
#
# data_box_hi = np.loadtxt("box_bulk_data_5_hi.csv", delimiter=',')
# boxOrder_hi = np.argsort(data_box_hi[:, 0])
#
# data_mortar_lo = np.loadtxt("mortar_bulk_data_5_lo.csv", delimiter=',')
# mortarOrder_lo = np.argsort(data_mortar_lo[:,0])
#
# data_mortar_hi = np.loadtxt("mortar_bulk_data_5_hi.csv", delimiter=',')
# mortarOrder_hi = np.argsort(data_mortar_hi[:,0])
#
# data_mpfa_lo = np.loadtxt("mpfa_bulk_data_5_lo.csv", delimiter=',')
# data_mpfa_hi = np.loadtxt("mpfa_bulk_data_5_hi.csv", delimiter=',')
data_ref = np.loadtxt("reference_data_lo.csv", delimiter=',')

data_lo = np.loadtxt("bulk_data_lo.csv", delimiter=',')
order_lo = np.argsort(data_lo[:, 0])


# plot
#
plt.plot(np.array(data_lo[:, 0])[order_lo], np.array(data_lo[:, 1])[order_lo], label="flux", color='k')
plt.plot(np.array(data_lo[:, 0])[order_lo], np.array(data_lo[:, 2])[order_lo], label="kGradP", color='r')
# plt.plot(np.array(data_box_lo[:, 0])[boxOrder_lo], np.array(data_box_lo[:, 1])[boxOrder_lo], label="box (lo)", color='k')
# plt.plot(np.array(data_box_hi[:, 0])[boxOrder_hi], np.array(data_box_hi[:, 1])[boxOrder_hi], label="box (hi)", color='k', linestyle='--')
# #
# plt.plot(np.array(data_mortar_lo[:, 0])[mortarOrder_lo], np.array(data_mortar_lo[:, 1])[mortarOrder_lo], label="box-mortar (lo)", color='r')
# plt.plot(np.array(data_mortar_hi[:, 0])[mortarOrder_hi], np.array(data_mortar_hi[:, 1])[mortarOrder_hi], label="box-mortar (hi)", color='r', linestyle='--')
#
# plt.plot(data_mpfa_lo[:, 0], data_mpfa_lo[:, 1], label="mpfa (lo)", color='b')
# plt.plot(data_mpfa_hi[:, 0], data_mpfa_hi[:, 1], label="mpfa (hi)", color='b', linestyle='--')

plt.plot(data_ref[:, 0], data_ref[:, 1], label="reference", color='b')
plt.legend()
# plt.show()
plt.savefig("transfer.pdf", bbox_inches='tight')

plt.figure(2)
plt.plot(np.array(data_lo[:, 0])[order_lo], np.array(data_lo[:, 3])[order_lo], label="ifPressure", color='k')
plt.plot(np.array(data_lo[:, 0])[order_lo], np.array(data_lo[:, 4])[order_lo], label="facetPressure", color='r')
plt.savefig("pressure.pdf", bbox_inches='tight')
