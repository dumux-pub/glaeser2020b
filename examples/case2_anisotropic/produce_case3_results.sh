#!/bin/bash

# clear old results
rm *pdf *vtu *vtp *pvd *csv

# run case of interface condition (11a)
python3 scripts/run_interface_comparison.py 0 lo
mkdir -p results_conditions_11a
mv *pdf *vtu *vtp *pvd *csv results_conditions_11a

# run case of interface condition (11b)
python3 scripts/run_interface_comparison.py 0 lo true
mkdir -p results_conditions_11b
mv *pdf *vtu *vtp *pvd *csv results_conditions_11b
