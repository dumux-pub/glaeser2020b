// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the reference model in the single-phase
 *        test considering anisotropic permeabilities.
 */
#ifndef DUMUX_GLAESER_ANISOTROPIC_REFERENCE_PROBLEM_HH
#define DUMUX_GLAESER_ANISOTROPIC_REFERENCE_PROBLEM_HH

#include <utility>

#include <dune/common/exceptions.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "boundaryconditions.hh"

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the single-phase
 *        test considering anisotropic permeabilities.
 */
template<class TypeTag>
class OnePReferenceProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePReferenceProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                         std::shared_ptr<typename ParentType::SpatialParams> spatialParams)
    : ParentType(gridGeometry, spatialParams)
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return AnisotropicTestBCs::bulkDirichletAtPos<PrimaryVariables>(globalPos); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    // writes the transfer fluxes to disk
    template<class GridVariables, class SolutionVector>
    void writeCsvData(const std::string& csvFileBody,
                      const GridVariables& gridVars,
                      const SolutionVector& x) const
    {
        // determine number of fractures
        std::vector<int> fractureMarkers;
        for (const auto& element : elements(this->gridGeometry().gridView()))
            if (this->spatialParams().isOnFracture(element))
                if (!std::count(fractureMarkers.begin(),
                                fractureMarkers.end(),
                                this->spatialParams().getDomainMarker(element)))
                    fractureMarkers.push_back(this->spatialParams().getDomainMarker(element));

        // write header to all output files
        for (auto marker : fractureMarkers)
        {
            std::ofstream fluxPlot_lo(csvFileBody + "_hi_" + std::to_string(marker) + ".csv", std::ios::out);
            std::ofstream fluxPlot_hi(csvFileBody + "_lo_" + std::to_string(marker) + ".csv", std::ios::out);

            fluxPlot_lo << "x,y,flux\n";
            fluxPlot_hi << "x,y,flux\n";
        }

        using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            const bool insideOnFracture = this->spatialParams().isOnFracture(element);
            if (insideOnFracture)
                continue;

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (scvf.boundary())
                    continue;

                const auto& outsideElement = this->gridGeometry().element(scvf.outsideScvIdx());
                const auto outsideMarker = this->spatialParams().getDomainMarker(outsideElement);
                const bool outsideOnFracture = this->spatialParams().isOnFracture(outsideElement);
                if (!outsideOnFracture)
                    continue;

                std::ofstream fluxPlot_lo(csvFileBody + "_lo_" + std::to_string(outsideMarker) + ".csv", std::ios::app);
                std::ofstream fluxPlot_hi(csvFileBody + "_hi_" + std::to_string(outsideMarker) + ".csv", std::ios::app);

                using std::abs;
                const auto nx = scvf.unitOuterNormal()[0];
                const auto ny = scvf.unitOuterNormal()[1];
                const bool isLowerInterface = abs(ny) > 1e-6 ? ny >= 0.0 : nx >= 0.0;
                const bool isUpperInterface = abs(ny) > 1e-6 ? ny < 0.0 : nx < 0.0;

                FluxVars fv;
                fv.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                auto up = [] (const auto& vv) { return vv.mobility()*vv.density(); };
                Scalar flux = fv.advectiveFlux(/*phaseIdx*/0, up);

                if (isLowerInterface) { fluxPlot_lo << scvf.center()[0] << "," << scvf.center()[1] << "," << flux/scvf.area() << "\n"; }
                else if (isUpperInterface) { fluxPlot_hi << scvf.center()[0] << "," << scvf.center()[1] << "," << flux/scvf.area() << "\n"; }
                else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
            }
        }
    }

    // functions we need for compatibility with generic solver
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}
};

} // end namespace Dumux

#endif
