// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase flow test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <dune/common/unused.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/io/grid/gridmanager.hh>

#include "box_cont_problem.hh"
#include "facet_problem_bulk.hh"
#include "facet_problem_facet.hh"
#include "reference_problem.hh"

#include <dumux/common/parameters.hh>
#include <dumux/common/integrate.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/discretization/projection/projector.hh>
#include <dumux/discretization/cellcentered/tpfa/fvgridgeometry.hh>

#include <examples/common/properties_facet.hh>
#include <examples/common/properties_reference.hh>

#include <examples/common/solver_reference.hh>
#include <examples/common/solver_box_cont.hh>
#include <examples/common/solver_mortar_2domain.hh>
#include <examples/common/solver_facet.hh>


// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    DUNE_UNUSED const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    std::cout << "\n\nSolving Reference\n" << std::endl;

    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Reference.Grid";
    tree["Reference.IO.VtkName"] = "reference";
    tree["Reference.IO.CsvFileNameBody"] = "reference_data";

    using ReferenceData = std::decay_t<decltype(solveReference(tree))>;
    using ReferenceGrid = typename ReferenceData::GridManager::Grid;
    using ReferenceSolution = typename ReferenceData::SolutionVector;
    using BlockType = typename ReferenceSolution::block_type;

    static constexpr int dim = ReferenceGrid::dimension;
    static constexpr int dimWorld = ReferenceGrid::dimensionworld;
    using RefSubGrid = Dune::SubGrid<dim, ReferenceGrid>;
    using RefSubGridView = typename RefSubGrid::LeafGridView;
    using RefSubGridGeometry = CCTpfaFVGridGeometry<RefSubGridView>;
    using RefSubGridBasis = typename Dumux::FunctionSpaceBasisTraits<RefSubGridGeometry>::GlobalBasis;

    using AveragerGrid = Dune::ALUGrid<dim, dimWorld, Dune::cube, Dune::nonconforming>;
    using AveragerGridGeometry  = Dumux::CCTpfaFVGridGeometry<typename AveragerGrid::LeafGridView>;
    using AveragerFSBasis = typename Dumux::FunctionSpaceBasisTraits<AveragerGridGeometry>::GlobalBasis;
    using namespace Dune::Functions;

    Dumux::GridManager<RefSubGrid> bulkGridManager;
    ReferenceSolution refBulkSolution;
    std::shared_ptr<RefSubGridBasis> refBulkBasis;
    std::shared_ptr<RefSubGridGeometry> refBulkGridGeometry;
    std::unordered_map<int, std::unique_ptr<AveragerGrid>> refFractureGrids;
    std::unordered_map<int, std::shared_ptr<AveragerGridGeometry>> refFractureGridGeometries;
    std::unordered_map<int, std::shared_ptr<AveragerFSBasis>> refFractureBases;
    std::unordered_map<int, ReferenceSolution> refFractureSolutions;
    std::vector<int> fractureIndices;

    // run reference and obtain bulk/facet solutions
    auto referenceData = solveReference(tree);
    bulkGridManager.init(referenceData.gridManager.grid(),
                         [&] (const auto& e)
                             { return !referenceData.problem->spatialParams().isOnFracture(e); });

    refBulkGridGeometry = std::make_shared<RefSubGridGeometry>(bulkGridManager.grid().leafGridView());
    refBulkGridGeometry->update();

    const auto& refSolution = referenceData.solution;
    const auto& refBasis = getFunctionSpaceBasis(*referenceData.gridGeometry);
    refBulkBasis = std::make_shared<RefSubGridBasis>(getFunctionSpaceBasis(*refBulkGridGeometry));

    std::cout << "Extracting bulk solution via projection" << std::endl;
    const auto bulkGlue = makeGlue(*referenceData.gridGeometry, *refBulkGridGeometry);
    const auto bulkProjector = makeProjector(refBasis, *refBulkBasis, bulkGlue);

    auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
    refBulkSolution = bulkProjector.project(refSolution, projParams);

    const auto refBulkZeroGF = makeAnalyticGridViewFunction([] (const auto& x) { return BlockType(0.0); }, refBulkBasis->gridView());
    const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
    const auto refBulkIntegral = integrateL2Error(refBulkBasis->gridView(), refBulkGF, refBulkZeroGF, 0);

    std::cout << "Writing reference bulk solution to disk" << std::endl;
    Dune::VTKWriter<RefSubGridView> refBulkWriter(bulkGridManager.grid().leafGridView());
    refBulkWriter.addCellData(refBulkSolution, "p");
    refBulkWriter.write("reference_bulk");

    std::cout << "Making fracture grids for cross-sectional averaging" << std::endl;
    const auto averagerGridBaseName = getParam<std::string>("Reference.FractureGridsBaseName");
    fractureIndices = getParam<std::vector<int>>("Reference.FractureGridIndices");
    if (fractureIndices.empty())
        DUNE_THROW(Dune::InvalidStateException, "This does not work if no fractures are present");

    for (auto fracIdx : fractureIndices)
    {
        const auto mshFile = averagerGridBaseName + "_" + std::to_string(fracIdx) + ".msh";
        refFractureGrids[fracIdx] = Dune::GmshReader<AveragerGrid>::read(mshFile);
        refFractureGridGeometries[fracIdx] = std::make_shared<AveragerGridGeometry>(refFractureGrids[fracIdx]->leafGridView());
        refFractureGridGeometries[fracIdx]->update();
        refFractureBases[fracIdx] = std::make_shared<AveragerFSBasis>(getFunctionSpaceBasis(*refFractureGridGeometries[fracIdx]));
    }

    std::cout << "Making cross-section averaged fracture solutions" << std::endl;
    for (auto fracIdx : fractureIndices)
    {
        const auto glue = makeGlue(*referenceData.gridGeometry, *refFractureGridGeometries[fracIdx]);
        const auto projector = makeProjector(refBasis, *refFractureBases[fracIdx], glue);
        auto projParams = projector.defaultParams(); projParams.residualReduction = 1e-16;
        refFractureSolutions[fracIdx] = projector.project(refSolution, projParams);

        std::cout << "Writing average solution for fracture " << fracIdx << " to disk" << std::endl;
        Dune::VTKWriter<typename AveragerGrid::LeafGridView> fracWriter(refFractureBases[fracIdx]->gridView());
        fracWriter.addCellData(refFractureSolutions[fracIdx], "p");
        fracWriter.write("reference_fracture_" + std::to_string(fracIdx));
    }

    // delete no-longer needed stuff to save memory
    referenceData.gridGeometry = nullptr;
    referenceData.problem = nullptr;
    referenceData.solution = ReferenceSolution();

    // run the different schemes for all refinements and compute error norm
    std::vector<double> discretizationLengths;
    std::vector<double> tpfaNormsBulk, tpfaNormsFacet;
    std::vector<double> mpfaNormsBulk, mpfaNormsFacet;
    std::vector<double> boxNormsBulk, boxNormsFacet;
    std::vector<double> boxMortarNormsBulk, boxMortarNormsFacet;
    std::vector<double> boxContNormsBulk, boxContNormsFacet;

    // make the facet grid/grid geometry for the finest refinement.
    // we will project both the reference as well as the hybrid-dimensional
    // solutions into this grid to compute the L2 norm of the error in the fractures
    using FinestFacetGrid = Dune::FoamGrid<1, 2>;
    using FinestFacetGridView = typename FinestFacetGrid::LeafGridView;
    GridManager<FinestFacetGrid> finestFacetGridManager;
    finestFacetGridManager.init("Refinement_" + std::to_string(getParam<int>("ConvergenceTest.NumRefinements")));

    auto& finestFacetGrid = finestFacetGridManager.grid();
    std::cout << "Finest grid number of cells: " << finestFacetGrid.leafGridView().size(0) << std::endl;
    finestFacetGrid.globalRefine(2);
    std::cout << "After refinement: " << finestFacetGrid.leafGridView().size(0) << std::endl;

    CCTpfaFVGridGeometry<FinestFacetGridView> finestFacetGridGeometry(finestFacetGrid.leafGridView());
    const auto& finestFacetGridView = finestFacetGrid.leafGridView();
    const auto& finestFacetBasis = getFunctionSpaceBasis(finestFacetGridGeometry);

    // lambda to set to zero all cell-centered values for elements not belonging to given fracture
    auto setZeroOutsideFracture = [&] (const auto& glue, auto& x)
    {
        // determine facet elements that have intersections
        std::vector<bool> hasIntersection(finestFacetGridView.size(0), false);
        for (const auto& is : intersections(glue))
            for (int k = 0; k < is.numTargetNeighbors(); ++k)
                hasIntersection[finestFacetGridGeometry.elementMapper().index(is.targetEntity(k))] = true;

        for (const auto& e : elements(finestFacetGridView))
            if (!hasIntersection[finestFacetGridGeometry.elementMapper().index(e)])
                x[finestFacetGridGeometry.elementMapper().index(e)] = 0.0;
    };

    // compute the reference fracture solution
    double refFacetIntegral = 0.0;
    {
        for (auto fracIdx : fractureIndices)
        {
            const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
            const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
            auto projParams = refProjector.defaultParams(); projParams.residualReduction = 1e-16;
            auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
            setZeroOutsideFracture(refGlue, refFracX);

            const auto refFracZeroGF = makeAnalyticGridViewFunction([] (const auto& x) { return BlockType(0.0); }, finestFacetBasis.gridView());
            const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);
            refFacetIntegral += integrateL2Error(finestFacetBasis.gridView(), refFracGF, refFracZeroGF, 0);
        }
    }
    std::cout << "Bulk solution integral: " << refBulkIntegral << std::endl;
    std::cout << "Facet solution integral: " << refFacetIntegral << std::endl;

    int l2IntOrder = 2;
    for (int refIdx = 0; refIdx <= getParam<int>("ConvergenceTest.NumRefinements"); ++refIdx)
    {
        std::string refSuffix = std::to_string(refIdx);

        // run facet models
        using namespace Properties::TTag;
        tree["Grid.ParamGroup"] = "Refinement_" + refSuffix;
        tree["Bulk.ParamGroup"] = "Bulk";
        tree["Facet.ParamGroup"] = "Facet";
        tree["Bulk.IO.VtkName"] = "tpfa_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "tpfa_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "tpfa_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "tpfa_facet_data_" + refSuffix;

        // solve and save tpfa data outside sub-scopes to reuse things for box-cont at the end
        std::cout << "\n\nSolving TPFA\n" << std::endl;
        auto tpfaData = solveFacet<OnePBulkTpfa, OnePFacetTpfa>(tree);
        const auto& tpfaBulkGG = *tpfaData.bulkGridGeometry;
        const auto& tpfaFacetGG = *tpfaData.facetGridGeometry;
        const auto& tpfaBulkBasis = getFunctionSpaceBasis(tpfaBulkGG);
        const auto& tpfaFacetBasis = getFunctionSpaceBasis(tpfaFacetGG);

        const auto& tpfaBulkSolution = tpfaData.bulkSolution;
        const auto& tpfaFacetSolution = tpfaData.facetSolution;

        {
            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(*tpfaData.bulkGridGeometry, *refBulkGridGeometry);
                const auto bulkProjector = makeProjector(tpfaBulkBasis, *refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(tpfaBulkSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, bulkProjSolution);
                const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
                tpfaNormsBulk.push_back(integrateL2Error(refBulkBasis->gridView(), refBulkGF, bulkProjGF, l2IntOrder)/refBulkIntegral);
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                double fracNorm = 0.0;
                for (auto fracIdx : fractureIndices)
                {
                    const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
                    const auto facetGlue = makeGlue(tpfaFacetGG, finestFacetGridGeometry);
                    const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
                    const auto facetProjector = makeProjector(tpfaFacetBasis, finestFacetBasis, facetGlue);
                    auto projParams = refProjector.defaultParams(); projParams.residualReduction = 1e-16;
                    auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
                    auto tpfaFracX = facetProjector.project(tpfaFacetSolution, projParams);

                    setZeroOutsideFracture(refGlue, refFracX);
                    setZeroOutsideFracture(refGlue, tpfaFracX);

                    const auto tpfaFracGf = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, tpfaFracX);
                    const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);

                    const auto norm = integrateL2Error(finestFacetBasis.gridView(), refFracGF, tpfaFracGf, l2IntOrder);
                    fracNorm += norm;
                    std::cout << " -> error norm for fracture with index " << fracIdx << ": " << norm << std::endl;
                }
                tpfaNormsFacet.push_back(fracNorm/refFacetIntegral);
            }
        }

        // determine discretization length of this grid
        using std::min;
        double hMin = std::numeric_limits<double>::max();
        for (const auto& element : elements(tpfaBulkGG.gridView()))
            hMin = min(hMin, diameter(element.geometry()));
        discretizationLengths.push_back(hMin);

        tree["Bulk.IO.VtkName"] = "mpfa_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "mpfa_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "mpfa_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "mpfa_facet_data_" + refSuffix;

        { // put this in a scope to save memory
            std::cout << "\n\nSolving MPFA\n" << std::endl;
            auto mpfaData = solveFacet<OnePBulkMpfa, OnePFacetMpfa>(tree);

            const auto& mpfaBulkSolution = mpfaData.bulkSolution;
            const auto& mpfaFacetSolution = mpfaData.facetSolution;
            const auto& mpfaBulkGG = *mpfaData.bulkGridGeometry;
            const auto& mpfaFacetGG = *mpfaData.facetGridGeometry;
            const auto& mpfaBulkBasis = getFunctionSpaceBasis(mpfaBulkGG);
            const auto& mpfaFacetBasis = getFunctionSpaceBasis(mpfaFacetGG);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(mpfaBulkGG, *refBulkGridGeometry);
                const auto bulkProjector = makeProjector(mpfaBulkBasis, *refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(mpfaBulkSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, bulkProjSolution);
                const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
                mpfaNormsBulk.push_back(integrateL2Error(refBulkBasis->gridView(), refBulkGF, bulkProjGF, l2IntOrder)/refBulkIntegral);
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                double fracNorm = 0.0;
                for (auto fracIdx : fractureIndices)
                {
                    const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
                    const auto facetGlue = makeGlue(mpfaFacetGG, finestFacetGridGeometry);
                    const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
                    const auto facetProjector = makeProjector(mpfaFacetBasis, finestFacetBasis, facetGlue);
                    auto projParams = refProjector.defaultParams(); projParams.residualReduction = 1e-16;
                    auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
                    auto mpfaFracX = facetProjector.project(mpfaFacetSolution, projParams);

                    setZeroOutsideFracture(refGlue, refFracX);
                    setZeroOutsideFracture(refGlue, mpfaFracX);

                    const auto mpfaFracGf = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, mpfaFracX);
                    const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);

                    const auto norm = integrateL2Error(finestFacetBasis.gridView(), refFracGF, mpfaFracGf, l2IntOrder);
                    fracNorm += norm;
                    std::cout << " -> error norm for fracture with index " << fracIdx << ": " << norm << std::endl;
                }
                mpfaNormsFacet.push_back(fracNorm/refFacetIntegral);
            }
        }

        // lambda to create element averaged solutions
        auto makeAveragedSolution = [&] (const auto& gridGeom, const auto& sol, auto& result)
        {
            result.resize(gridGeom.gridView().size(0));
            result = 0.0;
            for (const auto& e : elements(gridGeom.gridView()))
            {
                using GV = std::decay_t<decltype(gridGeom.gridView())>;
                const auto& eg = e.geometry();
                const auto eIdx = gridGeom.elementMapper().index(e);
                const auto elemSol = elementSolution(e, sol, gridGeom);

                auto& rule = Dune::QuadratureRules<double, GV::dimension>::rule(eg.type(), l2IntOrder);
                for (const auto& qp : rule)
                {
                    const auto& ip = eg.global(qp.position());
                    auto tmp = evalSolution(e, eg, elemSol, ip);
                    tmp *= qp.weight();
                    tmp *= eg.integrationElement(qp.position());
                    result[eIdx] += tmp;
                }

                result[eIdx] /= eg.volume();
            }
        };

        tree["Bulk.IO.VtkName"] = "box_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "box_facet_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "box_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "box_facet_data_" + refSuffix;

        { // put this in a scope to save memory
            std::cout << "\n\nSolving BOX\n" << std::endl;
            auto boxData = solveFacet<OnePBulkBox, OnePFacetBox>(tree);
            const auto& boxBulkGG = *boxData.bulkGridGeometry;
            const auto& boxFacetGG = *boxData.facetGridGeometry;

            // make P0 bases from the grids
            using BoxBulkGridView = std::decay_t<decltype(boxBulkGG.gridView())>;
            using BoxFacetGridView = std::decay_t<decltype(boxFacetGG.gridView())>;
            auto boxAvgBulkBasis = LagrangeBasis<BoxBulkGridView, 0>(boxBulkGG.gridView());
            auto boxAvgFacetBasis = LagrangeBasis<BoxFacetGridView, 0>(boxFacetGG.gridView());

            // make element-wise averaged solutions
            auto boxBulkAvgSolution = boxData.bulkSolution;
            auto boxFacetAvgSolution = boxData.facetSolution;
            makeAveragedSolution(boxBulkGG, boxData.bulkSolution, boxBulkAvgSolution);
            makeAveragedSolution(boxFacetGG, boxData.facetSolution, boxFacetAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(boxBulkGG, *refBulkGridGeometry);
                const auto bulkProjector = makeProjector(boxAvgBulkBasis, *refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxBulkAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, bulkProjSolution);
                const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
                boxNormsBulk.push_back(integrateL2Error(refBulkBasis->gridView(), refBulkGF, bulkProjGF, l2IntOrder)/refBulkIntegral);
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                double fracNorm = 0.0;
                for (auto fracIdx : fractureIndices)
                {
                    const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
                    const auto facetGlue = makeGlue(boxFacetGG, finestFacetGridGeometry);
                    const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
                    const auto facetProjector = makeProjector(boxAvgFacetBasis, finestFacetBasis, facetGlue);
                    auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                    auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
                    auto boxFracX = facetProjector.project(boxFacetAvgSolution, projParams);

                    setZeroOutsideFracture(refGlue, refFracX);
                    setZeroOutsideFracture(refGlue, boxFracX);

                    const auto boxFracGf = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, boxFracX);
                    const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);

                    const auto norm = integrateL2Error(finestFacetBasis.gridView(), refFracGF, boxFracGf, l2IntOrder);
                    fracNorm += norm;
                    std::cout << " -> error norm for fracture with index " << fracIdx << ": " << norm << std::endl;
                }
                boxNormsFacet.push_back(fracNorm/refFacetIntegral);
            }
        }


        tree["Bulk.IO.VtkName"] = "mortar_bulk_" + refSuffix;
        tree["Facet.IO.VtkName"] = "mortar_facet_" + refSuffix;
        tree["Mortar.ParamGroup"] = "Mortar";
        tree["Mortar.Grid.ParamGroup"] = "Refinement_" + refSuffix + ".Mortar";
        tree["Mortar.IO.VtkName"] = "mortar_" + refSuffix;
        tree["Bulk.IO.CsvFileNameBody"] = "mortar_bulk_data_" + refSuffix;
        tree["Facet.IO.CsvFileNameBody"] = "mortar_facet_data_" + refSuffix;
        tree["Mortar.IO.CsvFileNameBody"] = "mortar_data_" + refSuffix;

        {
            std::cout << "\n\nSolving BOX-MORTAR\n" << std::endl;
            auto mortarData = solveMortar(tree);
            const auto& boxBulkGG = *mortarData.bulkGridGeometry;
            const auto& boxFacetGG = *mortarData.facetGridGeometry;

            // make P0 bases from the grids
            using BoxBulkGridView = std::decay_t<decltype(boxBulkGG.gridView())>;
            using BoxFacetGridView = std::decay_t<decltype(boxFacetGG.gridView())>;
            auto boxAvgBulkBasis = LagrangeBasis<BoxBulkGridView, 0>(boxBulkGG.gridView());
            auto boxAvgFacetBasis = LagrangeBasis<BoxFacetGridView, 0>(boxFacetGG.gridView());

            // make element-wise averaged solutions
            auto boxBulkAvgSolution = mortarData.bulkSolution;
            auto boxFacetAvgSolution = mortarData.facetSolution;
            makeAveragedSolution(boxBulkGG, mortarData.bulkSolution, boxBulkAvgSolution);
            makeAveragedSolution(boxFacetGG, mortarData.facetSolution, boxFacetAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(boxBulkGG, *refBulkGridGeometry);
                const auto bulkProjector = makeProjector(boxAvgBulkBasis, *refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxBulkAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, bulkProjSolution);
                const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
                boxMortarNormsBulk.push_back(integrateL2Error(refBulkBasis->gridView(), refBulkGF, bulkProjGF, l2IntOrder)/refBulkIntegral);
            }

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                double fracNorm = 0.0;
                for (auto fracIdx : fractureIndices)
                {
                    const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
                    const auto facetGlue = makeGlue(boxFacetGG, finestFacetGridGeometry);
                    const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
                    const auto facetProjector = makeProjector(boxAvgFacetBasis, finestFacetBasis, facetGlue);
                    auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                    auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
                    auto boxFracX = facetProjector.project(boxFacetAvgSolution, projParams);

                    setZeroOutsideFracture(refGlue, refFracX);
                    setZeroOutsideFracture(refGlue, boxFracX);

                    const auto boxFracGf = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, boxFracX);
                    const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);

                    const auto norm = integrateL2Error(finestFacetBasis.gridView(), refFracGF, boxFracGf, l2IntOrder);
                    fracNorm += norm;
                    std::cout << " -> error norm for fracture with index " << fracIdx << ": " << norm << std::endl;
                }
                boxMortarNormsFacet.push_back(fracNorm/refFacetIntegral);
            }
        }


        tree["IO.VtkName"] = "box_cont_" + refSuffix;
        tree["IO.CsvFileNameBody"] = "box_cont_data_" + refSuffix;

        {
            std::cout << "\n\nSolving BOX-CONT\n" << std::endl;
            auto boxContData = solveBoxCont(tree);
            const auto& gridGeometry = *boxContData.gridGeometry;

            // make P0 bases from the grids
            using BoxContGridView = std::decay_t<decltype(gridGeometry.gridView())>;
            auto boxContAvgBasis = LagrangeBasis<BoxContGridView, 0>(gridGeometry.gridView());
            auto boxContBasis = LagrangeBasis<BoxContGridView, 1>(gridGeometry.gridView());

            // make element-wise averaged solutions
            auto boxContAvgSolution = boxContData.solution;
            makeAveragedSolution(gridGeometry, boxContData.solution, boxContAvgSolution);

            std::cout << "Computing bulk pressure error norm" << std::endl;
            {
                const auto glue = makeGlue(gridGeometry, *refBulkGridGeometry);
                const auto bulkProjector = makeProjector(boxContAvgBasis, *refBulkBasis, glue);
                auto projParams = bulkProjector.defaultParams(); projParams.residualReduction = 1e-16;
                const auto bulkProjSolution = bulkProjector.project(boxContAvgSolution, projParams);
                const auto bulkProjGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, bulkProjSolution);
                const auto refBulkGF = makeDiscreteGlobalBasisFunction<BlockType>(*refBulkBasis, refBulkSolution);
                boxContNormsBulk.push_back(integrateL2Error(refBulkBasis->gridView(), refBulkGF, bulkProjGF, l2IntOrder)/refBulkIntegral);
            }

            std::cout << "Obtaining fracture solution via projection" << std::endl;
            const auto facetGlue = makeGlue(gridGeometry, tpfaFacetGG);
            const auto facetProjector = makeProjector(boxContBasis, tpfaFacetBasis, facetGlue);
            auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
            auto facetProjSolution = facetProjector.project(boxContData.solution, projParams);

            std::cout << "Compute facet pressure error norm" << std::endl;
            {
                double fracNorm = 0.0;
                for (auto fracIdx : fractureIndices)
                {
                    const auto refGlue = makeGlue(*refFractureGridGeometries[fracIdx], finestFacetGridGeometry);
                    const auto facetGlue = makeGlue(tpfaFacetGG, finestFacetGridGeometry);
                    const auto refProjector = makeProjector(*refFractureBases[fracIdx], finestFacetBasis, refGlue);
                    const auto facetProjector = makeProjector(tpfaFacetBasis, finestFacetBasis, facetGlue);
                    auto projParams = facetProjector.defaultParams(); projParams.residualReduction = 1e-16;
                    auto refFracX = refProjector.project(refFractureSolutions[fracIdx], projParams);
                    auto boxContFracX = facetProjector.project(facetProjSolution, projParams);

                    setZeroOutsideFracture(refGlue, refFracX);
                    setZeroOutsideFracture(refGlue, boxContFracX);

                    const auto boxContFracGf = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, boxContFracX);
                    const auto refFracGF = makeDiscreteGlobalBasisFunction<BlockType>(finestFacetBasis, refFracX);

                    const auto norm = integrateL2Error(finestFacetBasis.gridView(), refFracGF, boxContFracGf, l2IntOrder);
                    fracNorm += norm;
                    std::cout << " -> error norm for fracture with index " << fracIdx << ": " << norm << std::endl;
                }
                boxContNormsFacet.push_back(fracNorm/refFacetIntegral);
            }
        }
    }

    std::cout << "The following parameters were used" << std::endl;
    Parameters::print();

    std::cout << "\n\nWriting error norms into .csv file" << std::endl;
    std::ofstream csvFileBulk("errornorms_bulk.csv", std::ios::out);
    std::ofstream csvFileFacet("errornorms_facet.csv", std::ios::out);

    csvFileBulk << "h,tpfa,mpfa,box,boxmortar,boxcont\n";
    csvFileFacet << "h,tpfa,mpfa,box,boxmortar,boxcont\n";

    for (unsigned int i = 0; i < tpfaNormsBulk.size(); ++i)
    {
        csvFileBulk << discretizationLengths[i] << ","
                    << tpfaNormsBulk[i] << "," << mpfaNormsBulk[i] << ","
                    << boxNormsBulk[i] << ","  << boxMortarNormsBulk[i] << ","
                    << boxContNormsBulk[i] << std::endl;

        csvFileFacet << discretizationLengths[i] << ","
                     << tpfaNormsFacet[i] << "," << mpfaNormsFacet[i] << ","
                     << boxNormsFacet[i] << ","  << boxMortarNormsFacet[i] << ","
                     << boxContNormsFacet[i] << std::endl;
    }

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
