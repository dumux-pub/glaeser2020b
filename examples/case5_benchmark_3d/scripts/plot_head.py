import os
import sys
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

if len(sys.argv) < 3:
    sys.exit("Expected two arguments:\n" \
             "\t - path to the extractlinedata.py script in dumux/bin/postprocessing\n" \
             "\t - domain prefix")

extractScript = sys.argv[1]
prefix = sys.argv[2]
plotReference = True if len(sys.argv) > 3 and sys.argv[3].lower() in ['true', 'yes', '1'] else False

if plotReference:

    print("Getting reference .csv data for mfe-mortar-dfm method")
    subprocess.run(['wget', 'https://git.iws.uni-stuttgart.de/benchmarks/fracture-flow-3d/-/raw/master/field/results/UiB/MVEM/dol_line_0.csv'], check=True)
    subprocess.run(['wget', 'https://git.iws.uni-stuttgart.de/benchmarks/fracture-flow-3d/-/raw/master/field/results/UiB/MVEM/dol_line_1.csv'], check=True)

    data1 = np.loadtxt('dol_line_0.csv', delimiter=',', skiprows=1)
    data2 = np.loadtxt('dol_line_1.csv', delimiter=',', skiprows=1)

    plt.figure(1)
    plt.plot(data1[:,0], data1[:,1], label='UiB-MVEM', color=referenceColor)
    plt.figure(2)
    plt.plot(data2[:,0], data2[:,1], label='UiB-MVEM', color=referenceColor)

    os.remove('dol_line_0.csv')
    os.remove('dol_line_1.csv')

# function to obtain the plot over line data for a vtu file
def getPlotData(vtuFile, p1, p2, numSamples):
    print("Reading " + vtuFile)
    subprocess.run(['pvpython', extractScript,
                    '-f', vtuFile,
                    '-of', 'temp',
                    '-r', str(numSamples),
                    '-p1', p1[0], p1[1], p1[2],
                    '-p2', p2[0], p2[1], p2[2]], check=True)
    if not os.path.exists('temp.csv'):
        sys.exit("Could not find .csv file created by " + extractScript)
    data = np.genfromtxt('temp.csv', names=True, delimiter=',')
    os.remove('temp.csv')
    return {'arc': data['arc_length'], 'h': data['p']}

# function to get data for first/second plot
def getFirstPlotData(vtuFile):
    return getPlotData(vtuFile, ['350', '100', '-100'], ['-500', '1500', '500'], 2000)
def getSecondPlotData(vtuFile):
    return getPlotData(vtuFile, ['-500', '100', '-100'], ['350', '1500', '500'], 2000)

for schemeName in names:
    schemeId = getSchemeId(schemeName)
    actualPrefix = '' if schemeName == 'box-cont' else prefix

    schemeName = schemeName.replace('-', '_')
    if schemeName == 'box_cont': vtuName = 'benchmark_3d_box_cont-00001.vtu'
    else: vtuName = 'benchmark_3d_bulk_' + actualPrefix + '_' + schemeName + '-00001.vtu'

    theLabel = labels[schemeId]
    lineStyle = linestyles[schemeId]

    # first plot (y = 500)
    data = getFirstPlotData(vtuName)
    plt.figure(1)
    plt.plot(data['arc'], data['h'], label=theLabel, color=colors[schemeId], linestyle=lineStyle)
    plt.xlabel("arc length [m]")
    plt.ylabel(r"${h_2}$ [m]")

    # second plot (x = 625)
    data = getSecondPlotData(vtuName)
    plt.figure(2)
    plt.plot(data['arc'], data['h'], label=theLabel, color=colors[schemeId], linestyle=lineStyle)
    plt.xlabel("arc length [m]")
    plt.ylabel(r"${h_2}$ [m]")

plt.figure(1)
plt.legend()
plt.ticklabel_format(axis='y', scilimits=(0, 0))
plt.savefig("line_1.pdf", bbox_inches='tight')
plt.figure(2)
plt.legend()
plt.ticklabel_format(axis='y', scilimits=(0, 0))
plt.savefig("line_2.pdf", bbox_inches='tight')
