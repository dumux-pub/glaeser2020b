import scipy.io as io
import numpy as np
import sys
import os

# use custom plot style
sys.path.append("../common")
from plotstyles import *

def getMatrixSpecs(matrixFile):
    M = io.mmread(matrixFile).tocsc()

    shape = M.shape
    if len(shape) != 2:
        sys.stderr.write("Unexpected matrix shape")
        sys.exit(1)

    if shape[0] != shape[1]:
        sys.stderr.write("Expected a square matrix")
        sys.exit(1)

    return {'numDofs': int(shape[0]),
            'nnz': int(M.count_nonzero())}


if len(sys.argv) < 2:
    sys.exit("Please provide if the table shold be provided for '2domain' or '3domain' case")
if sys.argv[1] not in ['2domain', '3domain']:
    sys.exit("Please provide either '2domain' or '3domain' as argument")

numDofString = r'$N_\mathrm{dof}$'
nnzString = r'$N_\mathrm{nnz}$'
cpuTimeString = r'$t_\mathrm{ls} \, [\si{\percent}]$'
domainPrefix = sys.argv[1]

tableFile = open('table.tex', 'w')
tableFile.write(r'\begin{table}[h]' + '\n' \
                r'  \centering' + '\n' \
                r'  \caption{\textbf{Case 4 - matrix characteristics}. Number of degrees of freedom (' + numDofString + ') and number of nonzero entries (' + nnzString + ') in the system matrices for the fine grid. Moreover, the cpu time (' + cpuTimeString + r') for the solution of the linear system with the direct solver UMFPack~\citep{Davis2004UmfPack}, on a single cpu with \SI{1.8}{\giga\hertz}, is shown.}' + '\n' \
                r'  \begin{tabular}{ *{6}{l} }' + '\n' \
                r'  \toprule' + '\n' \
                r'  & \tpfaDfm & \mpfaDfm & \eboxDfm & \eboxMortarDfm & \boxDfm \\ \midrule' + '\n' \
               )

specs = {}
refValue = 1.0
for baseSchemeName in names:
    if domainPrefix == "3domain" and baseSchemeName == 'box-cont':
        continue

    schemeNames = [baseSchemeName] if baseSchemeName == 'box-cont' else [domainPrefix + '_' + baseSchemeName]
    for schemeName in schemeNames:
        schemeName = schemeName.replace('-', '_')
        with open(schemeName + '_solvertimes.csv', 'r') as cpuTimeFile:
            lines = cpuTimeFile.readlines()
            if len(lines) != 1: sys.exit("Expected a single line")
            line = lines[0].split(',')
            solverTimes = [float(x) for x in line if x != '']

        if schemeName == 'box_cont':
            matrixFile = 'benchmark_3d_' + schemeName + '_matrix.mtx'
        else:
            matrixFile = 'benchmark_3d_bulk_' + schemeName + '_matrix.mtx'

        key = baseSchemeName.replace('-', '')
        key = key.replace('_', '')
        specs[key] = getMatrixSpecs(matrixFile)
        specs[key]['cpuTime'] = '{:.2f}'.format(np.mean(solverTimes))

        # relate all CPU times to mpfa
        if baseSchemeName == 'mpfa':
            refValue = float(specs[key]['cpuTime'])

for rowCaption in [numDofString, nnzString, cpuTimeString]:
    tableFile.write(rowCaption + ' & ')
    if rowCaption == numDofString: measure = 'numDofs'
    elif rowCaption == nnzString: measure = 'nnz'
    else: measure = 'cpuTime'

    keys = ['tpfa', 'mpfa', 'box', 'boxmortar']
    if domainPrefix != '3domain': keys.append('boxcont')

    for key in keys:
        if measure == 'cpuTime': specs[key][measure] = '{:.2f}'.format(float(specs[key][measure])/refValue*100.0)
        tableFile.write( r'\num{' + str(specs[key][measure]) + r'}')
        if key == keys[-1]: tableFile.write(r' \\' + '\n')
        else: tableFile.write(' & ')

tableFile.write(r'  \bottomrule' + '\n' \
                r'  \end{tabular}' + '\n' \
                r'  \label{tab:benchmark3dMatrices}' + '\n' \
                r'\end{table}' + '\n')
