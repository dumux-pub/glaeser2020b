import os
import sys
import subprocess

# use custom plot style
sys.path.append("../common")
from plotstyles import *

grid = "grids/benchmark_3d.msh"
mortarGrid = "grids/benchmark_3d_mortar.msh"
isectionMortarGrid = "grids/benchmark_3d_mortar.msh"

useIntDiri = True
for schemeName in names:
    schemeName = schemeName.replace('-', '_')

    if schemeName == 'box_cont': exeName = 'case5_box_cont_benchmark_3d'
    else: exeName = 'case5_facet_benchmark_3d'

    # run dumux and copy solver time files
    def executeDumux(exe, suffix, extraParams=[]):
        print("\n\nExecute called for suffix: " + suffix)

        if os.path.exists('solvertimes.csv'): os.remove('solvertimes.csv')
        subprocess.run(['make', exe], check=True)
        subprocess.run(['./' + exe,
                        'params.input',
                        '-Bulk.Problem.UseInteriorDirichletBCs', ('true' if useIntDiri else 'false'),
                        '-Facet.Problem.UseInteriorDirichletBCs', ('true' if useIntDiri else 'false'),
                        '-Intersection.Problem.UseInteriorDirichletBCs', ('true' if useIntDiri else 'false'),
                        '-Bulk.Problem.Name', 'benchmark_3d_bulk_' + suffix,
                        '-Facet.Problem.Name', 'benchmark_3d_facet_' + suffix,
                        '-Intersection.Problem.Name', 'benchmark_3d_is_' + suffix,
                        '-IO.VtkName', 'benchmark_3d_' + suffix,
                        '-Bulk.IO.VtkName', 'benchmark_3d_bulk_' + suffix,
                        '-Facet.IO.VtkName', 'benchmark_3d_facet_' + suffix,
                        '-Intersection.IO.VtkName', 'benchmark_3d_is_' + suffix,
                        '-IO.CsvFileNameBody', 'benchmark_3d_' + suffix,
                        '-Bulk.IO.CsvFileNameBody', 'benchmark_3d_bulk_' + suffix,
                        '-Facet.IO.CsvFileNameBodys', 'benchmark_3d_facet_' + suffix,
                        '-Intersection.IO.CsvFileNameBodys', 'benchmark_3d_is_' + suffix,
                        '-Grid.File', grid,
                        '-Mortar.Grid.File', mortarGrid,
                        '-FacetMortar.Grid.File', mortarGrid,
                        '-IntersectionMortar.Grid.File', isectionMortarGrid,
                        ] + extraParams,
                        check=True)
        subprocess.run(['cp', 'solvertimes.csv', suffix + '_solvertimes.csv'], check=True)

    if schemeName == 'box_cont':
        executeDumux(exeName, schemeName)
    else:
        suffix = '2domain_' + schemeName
        executeDumux(exeName + '_' + suffix, suffix)
        suffix = '3domain_' + schemeName
        executeDumux(exeName + '_' + suffix, suffix, ['-Newton.MaxAbsoluteResidual', '1e-5'])
