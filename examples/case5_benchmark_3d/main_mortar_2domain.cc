// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase flow test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <dune/common/unused.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/common/parameters.hh>

#include "facet_problem_bulk.hh"
#include "facet_problem_facet.hh"
#include "facet_problem_intersection.hh"
#include <examples/common/properties_facet.hh>
#include <examples/common/solver_mortar.hh>

// main program
int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    DUNE_UNUSED const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // run simulation
    solveMortar();

    return 0;
}
