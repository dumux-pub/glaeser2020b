// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the single-phase
 *        test considering anisotropic permeabilities and three-
 *        dimensional fracture networks.
 */
#ifndef DUMUX_GLAESER_INTERSECTION_TEST_FACET_BULK_PROBLEM_HH
#define DUMUX_GLAESER_INTERSECTION_TEST_FACET_BULK_PROBLEM_HH

#include <utility>

#include <dune/common/exceptions.hh>

#include <dumux/common/math.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "boundaryconditions.hh"

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the single-phase
 *        test considering anisotropic permeabilities and three-
 *        dimensional fracture networks.
 */
template<class TypeTag>
class OnePBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePBulkProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup)
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (isOnOutlet(globalPos))
            values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        static const bool useIntDirichlet = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseInteriorDirichletBCs", false);

        BoundaryTypes values;
        if (useIntDirichlet) values.setAllDirichlet();
        else values.setAllNeumann();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! evaluates the Neumann boundary conditions for a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return isOnInlet(globalPos) ? NumEqVector(-1.0) : NumEqVector(0.0); }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return isOnInlet1(globalPos) || isOnInlet2(globalPos); }

    //! returns true if position is on inlet boundary segment 1
    bool isOnInlet1(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > -500.0 && globalPos[0] < -200.0
               && globalPos[1] > 1500.0 - 1e-6
               && globalPos[2] > 300.0 && globalPos[2] < 500.0;
    }

    //! returns true if position is on inlet boundary segment 2
    bool isOnInlet2(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < -500.0 + 1e-6
               && globalPos[1] > 1200.0 && globalPos[1] < 1500.0
               && globalPos[2] > 300.0 && globalPos[2] < 500.0;
    }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return isOnOutlet1(globalPos) || isOnOutlet2(globalPos); }

    //! returns true if position is on outlet boundary segment 1
    bool isOnOutlet1(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < -500.0 + 1e-6
               && globalPos[1] > 100.0 && globalPos[1] < 400.0
               && globalPos[2] > -100.0 && globalPos[2] < 100.0;
    }

    //! returns true if position is on outlet boundary segment 2
    bool isOnOutlet2(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > 350.0 - 1e-6
               && globalPos[1] > 100.0 && globalPos[1] < 400.0
               && globalPos[2] > -100.0 && globalPos[2] < 100.0;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    // TODO: implement
    template<class BindContextFunc, class GridVariables, class SolutionVector>
    void writeCsvData(const std::string& csvFileBody,
                      BindContextFunc&& bindContext,
                      const GridVariables& gridVars,
                      const SolutionVector& x) const
    {}

    // functions we need for compatibility with generic solver
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
};

} // end namespace Dumux

#endif
