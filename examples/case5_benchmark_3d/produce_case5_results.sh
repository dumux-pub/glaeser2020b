#!/bin/bash

clear() {
    FOLDER=$1
    rm ${FOLDER}/*vtu \
       ${FOLDER}/*vtp \
       ${FOLDER}/*pvd \
       ${FOLDER}/*csv \
       ${FOLDER}/*mtx \
       ${FOLDER}/*pdf
}

mkdir -p "results"
clear .
clear "results"

python3 scripts/run_all.py

python3 scripts/plot_head.py ../../../../dumux/bin/postprocessing/extractlinedata.py "2domain"
mv line1.pdf line1_2domain.pdf
mv line2.pdf line2_2domain.pdf

python3 scripts/plot_head.py ../../../../dumux/bin/postprocessing/extractlinedata.py "3domain"
mv line1.pdf line1_3domain.pdf
mv line2.pdf line2_3domain.pdf

python3 scripts/makematrixtable.py "2domain"
mv table.tex table_2domain.tex

python3 scripts/makematrixtable.py "3domain"
mv table.tex table_3domain.tex
