// We define default values for the discretization length dxFracture around the
// fracture, and ratio dxFrature/dxBulk, where dxBulk is the discretization length to be
// used away from the fracture. You can overwrite the defaults by calling gmsh, for
// example if dxFracture = 0.1 and dxRatio = 1.0 is desired:
// gmsh -setnumber dxFracture 0.1 -setnumber dxRatio 1.0 filename.geo

SetFactory("OpenCASCADE");

DefineConstant[ aperture = 2e-3 ];
DefineConstant[ dxFracture = aperture/8.0 ];
DefineConstant[ dxRatio = 0.025 ];

// domain corners
dxBulk = dxFracture/dxRatio;
Point(0) = {0.0, 0.0, 0.0, dxBulk};
Point(1) = {1.0, 0.0, 0.0, dxBulk};
Point(2) = {1.0, 1.0, 0.0, dxBulk};
Point(3) = {0.0, 1.0, 0.0, dxBulk};

// points for inlet & outlet
Point(4) = {0.0, 0.15, 0.0, dxBulk};
Point(5) = {1.0, 0.85, 0.0, dxBulk};

// fracture corners
x1 = 0.25; y1 = 0.35;
x2 = 0.75; y2 = 0.65;
angle = ArcTan( (y2-y1)/(x2-x1) );
deltaX = Sin(angle)*aperture/2.0;
deltaY = Cos(angle)*aperture/2.0;
Point(6) = {x1 + deltaX, y1 - deltaY, 0.0, dxFracture};
Point(7) = {x1 - deltaX, y1 + deltaY, 0.0, dxFracture};
Point(8) = {x2 + deltaX, y2 - deltaY, 0.0, dxFracture};
Point(9) = {x2 - deltaX, y2 + deltaY, 0.0, dxFracture};

// fracture plane
Line(1) = {6, 7}; Line(2) = {7, 9};
Line(3) = {9, 8}; Line(4) = {8, 6};
Curve Loop(0) = {1, 2, 3, 4};
Plane Surface(0) = {0};

// domain plane
Line(5) = {0, 1}; Line(6) = {1, 5};
Line(7) = {5, 2}; Line(8) = {2, 3};
Line(9) = {3, 4}; Line(10) = {4, 0};
Curve Loop(1) = {5, 6, 7, 8, 9, 10};
Plane Surface(1) = {1};

// fragment domain by fracture
is[] = BooleanFragments{ Surface{1}; Delete;  }{ Surface{0}; Delete; };

Physical Surface(0) = {is[0]};
Physical Surface(1) = {is[ {1:#is[]-1} ]};
Characteristic Length{PointsOf{Surface{is[  {1:#is[]-1} ]};}} = dxBulk;
Characteristic Length{PointsOf{Surface{is[0]};}} = dxFracture;

l1[] = PointsOf{Surface{is[0]};};
Printf("1 = ", l1[]);

l2[] = PointsOf{Surface{is[  {1:#is[]-1} ]};};
Printf("2 = ", l2[]);
