// We define default values for the discretization length dxFracture around the
// fracture, and ratio dxFrature/dxBulk, where dxBulk is the discretization length to be
// used away from the fracture. You can overwrite the defaults by calling gmsh, for
// example if dxFracture = 0.1 and dxRatio = 1.0 is desired:
// gmsh -setnumber dxFracture 0.1 -setnumber dxRatio 1.0 filename.geo

SetFactory("OpenCASCADE");

DefineConstant[ dxFracture = (2e-3/2.0) ];
DefineConstant[ dxRatio = 0.025 ];

dxBulk = dxFracture/dxRatio;
Point(0) = {0.0, 0.0, 0.0, dxBulk};
Point(1) = {1.0, 0.0, 0.0, dxBulk};
Point(2) = {1.0, 1.0, 0.0, dxBulk};
Point(3) = {0.0, 1.0, 0.0, dxBulk};

// points for inlet & outlet
Point(4) = {0.0, 0.15, 0.0, dxBulk};
Point(5) = {1.0, 0.85, 0.0, dxBulk};

Line(1) = {0, 1}; Line(2) = {1, 5};
Line(3) = {5, 2}; Line(4) = {2, 3};
Line(5) = {3, 4}; Line(6) = {4, 0};
Curve Loop(1) = {1, 2, 3, 4, 5, 6};
Plane Surface(1) = {1};

Point(6) = {0.25, 0.35, 0.0, dxFracture};
Point(7) = {0.75, 0.65, 0.0, dxFracture};
Line(7) = {6, 7};

BooleanFragments{ Surface{1}; Delete; }{ Line{7}; }

Physical Line(1) = {7};
Physical Surface(1) = {Surface{:}};
