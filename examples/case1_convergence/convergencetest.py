import subprocess
import os
import sys
import math
import numpy

sys.path.append("../common")
from plotstyles import *

if sys.version_info[0] != 3:
    sys.exit("Python 3 required")

if len(sys.argv) < 6:
    sys.exit("Please provide four arguments:\n" \
             "    1. the number of refinements\n" \
             "    2. True/False if interior Dirichlet BCs should be used\n" \
             "    3. the fracture aperture\n" \
             "    4. True/False if structured grid should be used\n" \
             "    5. The fracture permeability\n")

initialNumCells = 12
numRefinements = int(sys.argv[1])
useIntDirichlet = True if sys.argv[2] in ['True', 'true', '1'] else False
aperture = sys.argv[3]
useStructured = '1' if sys.argv[4] in ['True', 'true', '1'] else '0'
fracPerm = sys.argv[5]

feIntOrder = 3

# mortar grid with factor 1.2
subprocess.run(['gmsh', '-2',
                '-format', 'msh2',
                '-setnumber', 'numCellsX', str(initialNumCells*(4/3)),
                '-setnumber', 'structured', useStructured,
                'convtest.geo'], check=False)
subprocess.run(['cp', 'convtest.msh', 'convtest_mortar_nonconforming.msh'], check=True)

# mortar grid with factor 2
subprocess.run(['gmsh', '-2',
                '-format', 'msh2',
                '-setnumber', 'numCellsX', str(initialNumCells*2.0),
                '-setnumber', 'structured', useStructured,
                'convtest.geo'], check=False)
subprocess.run(['cp', 'convtest.msh', 'convtest_mortar_double.msh'], check=True)

# mortar grid with factor 0.5
subprocess.run(['gmsh', '-2',
                '-format', 'msh2',
                '-setnumber', 'numCellsX', str(initialNumCells*0.5),
                '-setnumber', 'structured', useStructured,
                'convtest.geo'], check=False)
subprocess.run(['cp', 'convtest.msh', 'convtest_mortar_half.msh'], check=True)

# bulk grid
subprocess.run(['gmsh', '-2',
                '-format', 'msh2',
                '-setnumber', 'numCellsX', str(initialNumCells),
                '-setnumber', 'structured', useStructured,
                'convtest.geo'], check=False)

# keep track of all created grids
baseGrids = ['convtest.msh', 'convtest_mortar_half.msh', 'convtest_mortar_double.msh', 'convtest_mortar_nonconforming.msh']
grids = ['convtest.msh', 'convtest_mortar_half.msh', 'convtest_mortar_double.msh', 'convtest_mortar_nonconforming.msh']

# create grid for a refinement
def makeGrids(refIdx):
    if refIdx > 0:
        for gridFile in baseGrids:
            subprocess.run(['gmsh', '-format', 'msh2', '-refine', gridFile], check=False)

    for gridFile in baseGrids:
        name = os.path.splitext(gridFile)[0]
        name = name + '_' + str(refIdx) + '.msh'
        subprocess.run(['cp', gridFile, name], check=False)
        grids.append(name)

for i in range(0, numRefinements+1): makeGrids(i)

errors = {}
rates = {}
for schemeName in names:

    # skip mpfa
    if schemeName == 'mpfa':
        continue

    for i in range(0, numRefinements+1):

        # make sure executable is up to date
        exeName = 'convergence_analytical_' + schemeName.replace('-', '_')
        subprocess.run(['make', exeName], check=True)

        mortarSuffices = [''] if schemeName != 'box-mortar' else ['_double', '_half', '_nonconforming']
        for suffix in mortarSuffices:
            # run dumux
            bulkGridName = 'convtest_' + str(i) + '.msh'
            mortarGridName = 'convtest_mortar' + suffix + '_' + str(i) + '.msh'
            errorFileName = 'errors_' + str(i) + '_' + schemeName + suffix + '.csv'
            refFileName = 'referencevalues_' + str(i) + '_' + schemeName + suffix + '.csv'
            subprocess.run(['./' + exeName, 'params.input',
                            '-Grid.File', bulkGridName,
                            '-Assembly.FEIntegrationOrder', str(feIntOrder),
                            '-L2Error.OutputFile', errorFileName,
                            '-L2Error.ReferenceFile', refFileName,
                            '-Facet.SpatialParams.Aperture', aperture,
                            '-Facet.SpatialParams.Permeability', fracPerm,
                            '-IO.VtkName', 'ref_' + str(i) + '_' + schemeName + suffix,
                            '-Bulk.IO.VtkName', 'bulk_ref_' + str(i) + '_' + schemeName + suffix,
                            '-Facet.IO.VtkName', 'facet_ref_' + str(i) + '_' + schemeName + suffix,
                            '-Mortar.IO.VtkName', 'mortar_ref_' + str(i) + '_' + schemeName + suffix,
                            '-Mortar.Grid.File', mortarGridName,
                            '-IO.CsvFileNameBody', 'ref_' + str(i) + '_' + schemeName + suffix,
                            '-Bulk.IO.CsvFileNameBody', 'bulk_ref_' + str(i) + '_' + schemeName + suffix,
                            '-Facet.IO.CsvFileNameBody', 'facet_ref_' + str(i) + '_' + schemeName + suffix,
                            '-Mortar.IO.CsvFileNameBody', 'mortar_ref_' + str(i) + '_' + schemeName + suffix], check=True)

            # read in errors
            if i == 0:
                errors[schemeName + suffix] = []
                rates[schemeName + suffix] = []

            # compute rates
            errors[schemeName + suffix].append(numpy.loadtxt(errorFileName, delimiter=',')[:])
            if i > 0:
                curRates = []
                curErrs = errors[schemeName + suffix][i]
                prevErrs = errors[schemeName + suffix][i-1]
                for j in range(1, len(curErrs)):
                    curRates.append( (math.log(curErrs[j]) - math.log(prevErrs[j]))/
                                     (math.log(curErrs[0]) - math.log(prevErrs[0])) )
                rates[schemeName + suffix].append(curRates)

print("Computed the following rates:")
for scheme, schemeRates in rates.items():
    print("\tScheme \"" + scheme + "\": ", schemeRates)

    print("\n")
    print(" -> average rate bulk pressure: {:.2f}".format(numpy.mean([rate[0] for rate in schemeRates])))
    if len(schemeRates[0]) > 1: print(" -> average rate facet pressure: {:.2f}".format(numpy.mean([rate[1] for rate in schemeRates])))
    if len(schemeRates[0]) > 2: print(" -> average rate interface flux: {:.2f}".format(numpy.mean([rate[2] for rate in schemeRates])))
    if len(schemeRates[0]) > 3: print(" -> average rate jump in fluxes: {:.2f}".format(numpy.mean([rate[3] for rate in schemeRates])))

# write file with all errors for all schemes
errBulkPressure = open('errors_bulk_p.csv', 'w')
errFacetPressure = open('errors_facet_p.csv', 'w')
errBulkFlux = open('errors_bulk_q.csv', 'w')
fileArray = [errBulkPressure, errFacetPressure, errBulkFlux]

for file in fileArray:
    file.write("h")
    for scheme in errors: file.write("," + scheme)
    file.write("\n")

for i in range(0, numRefinements+1):

    # write discretization length (take values from first scheme)
    h = errors[names[0]][i][0]
    for file in fileArray:
        file.write(str(h))

    for scheme in errors:
        curNorms = errors[scheme][i]

        errBulkPressure.write(',')
        errBulkPressure.write(str(curNorms[1]))

        if scheme not in ['box-cont', 'box_cont']:
            errFacetPressure.write(',')
            errFacetPressure.write(str(curNorms[2]))

            errBulkFlux.write(',')
            errBulkFlux.write(str(curNorms[3]))

        else: # write zeros, should not be used subsequently
            errFacetPressure.write(',')
            errFacetPressure.write(str(0))
            errBulkFlux.write(',')
            errBulkFlux.write(str(0))

    for file in fileArray:
        file.write('\n')

# remove created meshes
print("\nRemoving the created mesh files")
for file in grids: os.remove(file)
