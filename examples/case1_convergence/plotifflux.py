import matplotlib.pyplot as plt
import numpy as np
import copy
import sys

sys.path.append("../common")
from plotstyles import *

if len(sys.argv) < 3:
    sys.exit("Please provide two argument:\n" \
             " - the refinement level for which to plot the fluxes\n" \
             " - \"lo\" or \"hi\" to indicate if the upper or lower interface should be plotted")

refIdx = int(sys.argv[1])
suffix = sys.argv[2]
if sys.argv[2] != "lo" and sys.argv[2] != "hi":
    sys.exit("Invalid second runtime argument")

for schemeName in names:

    # box-cont does not allow for flux evaluation
    if schemeName not in ['box_cont', 'box-cont']:
        schemeId = getSchemeId(schemeName)
        fluxFileName = 'bulk_ref_' + str(refIdx) + '_' + schemeName + '_' + suffix + '.csv'
        print("Reading flux data for " + schemeName + " in file " + fluxFileName)
        data = np.genfromtxt(fluxFileName, delimiter=',', names=True)
        indices = np.argsort(data['x'])

        plt.figure(1)
        plt.plot(data['x'][indices], data['flux'][indices],
                 label=labels[schemeId], color=colors[schemeId],
                 linestyle=linestyles[schemeId])

        # plot reference only once (at the very end)
        if schemeName == 'box-mortar':
            plt.plot(data['x'][indices], data['exactFlux'][indices],
                     label='exact', color=referenceColor)

plt.legend()
plt.xlabel(r'$x$')
plt.ylabel(r'$\lambda$')
plt.savefig('ifflux_' + suffix + '.pdf', bbox_inches='tight')
