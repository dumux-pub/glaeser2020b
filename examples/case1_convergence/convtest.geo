Mesh.RecombinationAlgorithm = 2;

DefineConstant[ numCellsX = 10 ];
DefineConstant[ structured = 1 ];
numCellsY = Round(numCellsX/2.0);

Point(1) = {-0.5, -0.5, 0.0, 1.0/numCellsX};
Point(2) = { 0.5, -0.5, 0.0, 1.0/numCellsX};
Point(3) = { 0.5,  0.0, 0.0, 1.0/numCellsX};
Point(4) = { 0.5,  0.5, 0.0, 1.0/numCellsX};
Point(5) = {-0.5,  0.5, 0.0, 1.0/numCellsX};
Point(6) = {-0.5,  0.0, 0.0, 1.0/numCellsX};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 1};
Line(7) = {3, 6};

Line Loop(1) = {1, 2, 7, 6};
Line Loop(2) = {-7, 3, 4, 5};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Physical Surface(1) = {1, 2};
Physical Line(1) = {7};

If (structured == 1)
    Transfinite Line{1, 4, 7} = numCellsX + 1;
    Transfinite Line{2, 3, 5, 6} = numCellsY + 1;
    Transfinite Surface "*";
EndIf

Recombine Surface "*";
