// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem class for the convergence test using box-cont model.
 */
#ifndef DUMUX_GLAESER2020B_CONVERGENCE_BOX_CONT_PROBLEM_HH
#define DUMUX_GLAESER2020B_CONVERGENCE_BOX_CONT_PROBLEM_HH

#include <utility>

#include <dumux/porousmediumflow/problem.hh>
#include "exactsolution.hh"

namespace Dumux {

/*!
 * \brief The problem implementation.
 */
template<class TypeTag>
class OnePBoxContProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

public:

    //! The constructor
    OnePBoxContProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , permeability_(getParamFromGroup<Scalar>("Bulk", "SpatialParams.Permeability"))
    , facetPerm_(getParamFromGroup<Scalar>("Facet", "SpatialParams.Permeability"))
    , aperture_(getParamFromGroup<Scalar>("Facet", "SpatialParams.Aperture"))
    {}

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     * \note In the box-scheme, we compute fluxes etc element-wise, thus, per
     *       element we compute only half a fracture (and extrude with aperture/2)
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return scv.isOnFracture() ? aperture_/2.0 : 1.0; }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();

        static const bool useNeumann = getParam<bool>("BoxCont.UseLateralNeumannBCs", false);
        if (useNeumann)
        {
            if (globalPos[0] < this->gridGeometry().bBoxMin()[0] + 1e-6
                || globalPos[0] > this->gridGeometry().bBoxMax()[0] - 1e-6)
                values.setAllNeumann();
        }

        return values;
    }

    //! Evaluates the source term at a given position.
    template<class ElemVolVars>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElemVolVars& elemVolVars,
                       const SubControlVolume& scv) const
    {
        if (scv.isOnFracture())
        {
            std::vector<GlobalPosition> corners({scv.corner(0), scv.corner(1)});
            Dune::AffineGeometry<Scalar, 1, 2> geom(Dune::GeometryTypes::line, corners);
            return integralFacetSource(geom, facetPerm_)/geom.volume();
        }
        return integralBulkSource(scv.geometry(), facetPerm_, aperture_)/scv.volume();
    }

    //! Evaluates the Neumann boundary conditions of an scvf
    template<class ElemVolVars, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElemVolVars& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        auto integralFlux = integralBulkNormalFlux(scvf.geometry(), scvf.unitOuterNormal(),
                                                   permeability_, facetPerm_, aperture_);
        using std::abs;
        if ( abs(insideScv.dofPosition()[1]) < 1e-6 )
            integralFlux += exactFacetNormalFlux(insideScv.dofPosition(), scvf.unitOuterNormal(),
                                                 facetPerm_)*aperture_/2.0;
        return integralFlux/scvf.area();
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    {
        return scv.isOnFracture() ? exactFacetSolution(scv.dofPosition())
                                  : exactBulkSolution(element.geometry(), scv.dofPosition(), facetPerm_, aperture_);
    }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const { return 293.15; /* 10°C */ }

    // functions we need for compatibilty with general solver
    template<class... Args> void writeCsvData(Args&&... x) const {}
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}

private:
    Scalar permeability_;
    Scalar facetPerm_;
    Scalar aperture_;
};

} // end namespace Dumux

#endif
