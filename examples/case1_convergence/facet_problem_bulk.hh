// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the convergence test.
 */
#ifndef DUMUX_GLAESER_CONVERGENCE_FACET_BULK_PROBLEM_HH
#define DUMUX_GLAESER_CONVERGENCE_FACET_BULK_PROBLEM_HH

#include <utility>
#include <cmath>

#include <dune/common/exceptions.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "exactsolution.hh"

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the convergence test.
 */
template<class TypeTag>
class OnePBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;
    static constexpr int dim = GridGeometry::GridView::dimension;

public:
    OnePBulkProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup)
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , permeability_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability"))
    , facetPerm_(getParam<Scalar>("Facet.SpatialParams.Permeability"))
    , aperture_(getParam<Scalar>("Facet.SpatialParams.Aperture"))
    {
        exactSol_.resize(gridGeometry->numDofs(), 0.0);

        if constexpr (isBox)
        {
            for (const auto& e : elements(gridGeometry->gridView()))
                for (int i = 0; i < e.subEntities(dim); ++i)
                    exactSol_[gridGeometry->vertexMapper().vertexIndex(e.template subEntity<dim>(i))]
                        = exactBulkSolution(e.geometry(),
                                            e.geometry().corner(i),
                                            facetPerm_,
                                            aperture_);
        }
        else
        {
            for (const auto& e : elements(gridGeometry->gridView()))
                exactSol_[gridGeometry->elementMapper().index(e)]
                    = exactBulkSolution(e.geometry(),
                                        e.geometry().center(),
                                        facetPerm_,
                                        aperture_);
        }
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();

        static const bool useNeumann = getParam<bool>("Bulk.UseLateralNeumannBCs", false);
        if (useNeumann)
        {
            if (globalPos[0] < this->gridGeometry().bBoxMin()[0] + 1e-6
                || globalPos[0] > this->gridGeometry().bBoxMax()[0] - 1e-6)
                values.setAllNeumann();
        }

        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        static const bool useIntDirichlet = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseInteriorDirichletBCs", false);

        BoundaryTypes values;
        if (useIntDirichlet) values.setAllDirichlet();
        else values.setAllNeumann();

        return values;
    }

    //! evaluates the Dirichlet boundary condition (box implementation)
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    { return exactBulkSolution(element.geometry(), scv.dofPosition(), facetPerm_, aperture_); }

    //! evaluates the Dirichlet boundary condition (cc implementation)
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolumeFace& scvf) const
    { return exactBulkSolution(element.geometry(), scvf.ipGlobal(), facetPerm_, aperture_); }

    //! Evaluates the Neumann boundary conditions of an scvf
    template<class ElemVolVars, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElemVolVars& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        const auto integralFlux = integralBulkNormalFlux(scvf.geometry(), scvf.unitOuterNormal(),
                                                         permeability_, facetPerm_, aperture_);
        return integralFlux/scvf.area();
    }

    //! Evaluates the source term in an scv.
    template<class ElemVolVars>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElemVolVars& elemVolVars,
                       const SubControlVolume& scv) const
    {
        const auto integralSource = integralBulkSource(scv.geometry() , facetPerm_, aperture_);
        return integralSource/scv.volume();
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    // writes the transfer fluxes to disk
    template<class BindContextFunc, class GridVariables, class SolutionVector>
    void writeCsvData(const std::string& csvFileBody,
                      BindContextFunc&& bindContext,
                      const GridVariables& gridVars,
                      const SolutionVector& x) const
    {
        const std::string csvHi = csvFileBody + "_hi.csv";
        const std::string csvLo = csvFileBody + "_lo.csv";

        std::ofstream fluxPlot_lo(csvLo, std::ios::out);
        std::ofstream fluxPlot_hi(csvHi, std::ios::out);

        if constexpr (GridGeometry::discMethod == DiscretizationMethod::box)
        {
            fluxPlot_lo << "\"x\",\"flux\",\"exactFlux\",\"kgradp\",\"bulkpressure\",\"facetpressure\"" << std::endl;
            fluxPlot_hi << "\"x\",\"flux\",\"exactFlux\",\"kgradp\",\"bulkpressure\",\"facetpressure\"" << std::endl;
        }
        else
        {
            fluxPlot_lo << "\"x\",\"flux\",\"exactFlux\"" << std::endl;
            fluxPlot_hi << "\"x\",\"flux\",\"exactFlux\"" << std::endl;
        }

        using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

            bindContext(element);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (!couplingManagerPtr_->isOnInteriorBoundary(element, scvf))
                    continue;

                const bool isLowerInterface = scvf.unitOuterNormal()[1] > 0.0;
                const bool isUpperInterface = scvf.unitOuterNormal()[1] < 0.0;

                FluxVars fv;
                fv.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                auto up = [] (const auto& vv) { return 1.0; };
                const Scalar flux = fv.advectiveFlux(/*phaseIdx*/0, up);

                // evaluate the exact flux at the actual interface in the equi-dimensional setting
                const Scalar exactFlux = exactBulkNormalFlux(scvf.ipGlobal(), scvf.unitOuterNormal(),
                                                             permeability_, facetPerm_, aperture_);

                // for box, plot -K/mu*gradP and pressure as well
                if constexpr (isBox)
                {
                    const auto& cache = elemFluxVarsCache[scvf];

                    Scalar p = 0.0;
                    GlobalPosition gradP(0.0);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        auto tmp = cache.gradN(scv.indexInElement());
                        tmp *= elemVolVars[scv].pressure();
                        gradP += tmp;
                        p += elemVolVars[scv].pressure()*cache.shapeValues()[scv.indexInElement()];
                    }

                    const auto& K = elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].permeability();
                    auto permGradP = -1.0*vtmv(scvf.unitOuterNormal(), K, gradP);
                    permGradP /= elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].viscosity();

                    const auto& facetPressure = couplingManager().getLowDimVolVars(element, scvf).pressure();

                    if (isLowerInterface) { fluxPlot_lo << scvf.center()[0] << "," << flux/scvf.area() << ", " << exactFlux << "," << permGradP << "," << p << "," << facetPressure << "\n"; }
                    else if (isUpperInterface) { fluxPlot_hi << scvf.center()[0] << "," << flux/scvf.area() << ", " << exactFlux << "," << permGradP << "," << p << "," << facetPressure << "\n"; }
                    else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
                }
                else
                {
                    if (isLowerInterface) { fluxPlot_lo << scvf.center()[0] << "," << flux/scvf.area() << "," << exactFlux << "\n"; }
                    else if (isUpperInterface) { fluxPlot_hi << scvf.center()[0] << "," << flux/scvf.area() << "," << exactFlux << "\n"; }
                    else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
                }
            }
        }
    }

    // add exact solution to vtk output
    template<class Writer> void addOutputFields(Writer& writer) const
    {
        if (!isBox)
            writer.addField(exactSol_, "exact", Writer::FieldType::element);
    }

    // update additional output fields
    template<class... Args> void updateOutputFields(Args&&... x) const {}

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar permeability_;
    Scalar facetPerm_;
    Scalar aperture_;
    std::vector<Scalar> exactSol_;
};

} // end namespace Dumux

#endif
