// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase flow test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <dune/common/unused.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include "facet_problem_bulk.hh"
#include "facet_problem_facet.hh"

#include <examples/common/properties_facet.hh>
#include <examples/common/solver_facet.hh>

#include "exactsolution.hh"
#include "computeerrornorm.hh"

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    DUNE_UNUSED const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // type tags must be defined in CMakeLists.txt
    using BulkTypeTag = Dumux::Properties::TTag::BULKTYPETAG;
    using FacetTypeTag = Dumux::Properties::TTag::FACETTYPETAG;

    // get the csv/vtk file names from the input file
    Dune::ParameterTree tree;
    tree["Bulk.IO.VtkName"] = getParam<std::string>("Bulk.IO.VtkName");
    tree["Facet.IO.VtkName"] = getParam<std::string>("Facet.IO.VtkName");
    tree["Bulk.IO.CsvFileNameBody"] = getParam<std::string>("Bulk.IO.CsvFileNameBody");
    tree["Facet.IO.CsvFileNameBody"] = getParam<std::string>("Facet.IO.CsvFileNameBody");

    // run simulation
    const auto result = solveFacet<BulkTypeTag, FacetTypeTag>(tree);

    // compute error norms (evaluate exact flux at equi-dimensional interface)
    const auto bulkPerm = getParam<double>("Bulk.SpatialParams.Permeability");
    const auto facetPerm = getParam<double>("Facet.SpatialParams.Permeability");
    const auto aperture = getParam<double>("Facet.SpatialParams.Aperture");
    auto evalBulk = [&] (const auto& eg, const auto& x) { return exactBulkSolution(eg, x, facetPerm, aperture); };
    auto evalFacet = [&] (const auto& eg, const auto& x) { return exactFacetSolution(x); };
    auto evalExactFlux = [&] (const auto& x, const auto& n) { return exactBulkNormalFlux(x, n, bulkPerm, facetPerm, aperture); };

    auto bindBulkContext = [&] (const auto& e)
    { result.couplingManager->bindCouplingContext(Dune::index_constant<0>(), e, *result.assembler); };
    auto bindFacetContext = [&] (const auto& e)
    { result.couplingManager->bindCouplingContext(Dune::index_constant<1>(), e, *result.assembler); };

    const auto h = computeDiscretizationLength(*result.bulkGridGeometry);
    const auto bulkErrorPair = computeSolutionNorm(*result.bulkGridGeometry, result.bulkSolution, aperture, evalBulk);
    const auto facetErrorPair = computeSolutionNorm(*result.facetGridGeometry, result.facetSolution, aperture, evalFacet);
    const auto fluxJumpErrorPair = computeFluxJumpNorm(*result.facetGridGeometry,
                                                       *result.facetGridVariables,
                                                       result.facetSolution,
                                                       bulkPerm, facetPerm, aperture,
                                                       bindFacetContext);


    using BulkFluxVariables = GetPropType<BulkTypeTag, Properties::FluxVariables>;
    const auto fluxErrorPair = computeFluxNorm<BulkFluxVariables>(*result.bulkGridGeometry,
                                                                  *result.bulkGridVariables,
                                                                  result.bulkSolution,
                                                                  *result.couplingManager,
                                                                  Dune::index_constant<0>(),
                                                                  bindBulkContext,
                                                                  evalExactFlux);

    std::cout << "Computed the following error norms (p_2, p_1, F_2, q_1): "
              << bulkErrorPair.first << ", " << facetErrorPair.first << ", "
              << fluxErrorPair.first << ", " << fluxJumpErrorPair.first << std::endl;

    // write the norms into error file
    std::ofstream errorFile(getParam<std::string>("L2Error.OutputFile"), std::ios::out);
    std::ofstream refFile(getParam<std::string>("L2Error.ReferenceFile"), std::ios::out);
    errorFile << h << "," << bulkErrorPair.first << "," << facetErrorPair.first
                   << "," << fluxErrorPair.first << "," << fluxJumpErrorPair.first << std::endl;
    refFile << h << "," << bulkErrorPair.second << "," << facetErrorPair.second
                 << "," << fluxErrorPair.second << "," << fluxJumpErrorPair.second << std::endl;

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
