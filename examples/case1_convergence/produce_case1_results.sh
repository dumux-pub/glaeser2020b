#!/bin/bash

runAndPlot() {
    PERM=$1
    RESULTS_FOLDER="results_k_${PERM}"
    python3 convergencetest.py 6 False 1e-4 True ${PERM}
    python3 plotrates.py ${PERM}
    mkdir -p ${RESULTS_FOLDER}
    mv *.vtu *.pvd *.csv *.vtp *.pdf ${RESULTS_FOLDER}
}

runAndPlot 1e-4
runAndPlot 1e4
