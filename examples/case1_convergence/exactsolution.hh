// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Helper functions to compute the exact solutions.
 */
#ifndef DUMUX_GLAESER_CONVERGENCE_EXACT_SOL_HH
#define DUMUX_GLAESER_CONVERGENCE_EXACT_SOL_HH

#include <cmath>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dumux/common/parameters.hh>

namespace Dumux {
namespace Detail {

    template<class GlobalPosition, class Scalar = double>
    Scalar computeJump(const GlobalPosition& globalPos,
                       Scalar facetPermeability,
                       Scalar aperture)
    {
        const auto& a = aperture;
        const auto& x = globalPos[0];
        const auto& kf = facetPermeability;
        return (x + 0.5)*(x + 0.5)*a/kf;
    }

    template<class GlobalPosition, class Scalar = double>
    Scalar exactBulkSolutionFixedJump(const GlobalPosition& pos,
                                      Scalar jump)
    {
        const auto& x = pos[0];
        const auto& y = pos[1];
        return x*x*x + y*y*y + y*(x + 0.5)*(x + 0.5) + jump;
    }
}

template<class GlobalPosition, class Scalar = double>
Scalar exactFacetSolution(const GlobalPosition& globalPos)
{
    return Detail::exactBulkSolutionFixedJump(globalPos, 0.0);
}

template<class Geometry, class GlobalPosition, class Scalar = double>
Scalar exactBulkSolution(const Geometry& geometry,
                         const GlobalPosition& globalPos,
                         Scalar facetPermeability,
                         Scalar aperture)
{
    const auto jump = geometry.center()[1] < 0.0 ? -0.5*Detail::computeJump(globalPos, facetPermeability, aperture)
                                                 :  0.5*Detail::computeJump(globalPos, facetPermeability, aperture);
    return Detail::exactBulkSolutionFixedJump(globalPos, jump);
}

template<class GlobalPosition, class Scalar = double>
Scalar exactFacetSource(const GlobalPosition& globalPos,
                        Scalar facetPermeability)
{
    const auto& kf = facetPermeability;
    const auto& x = globalPos[0];
    return -6.0*x*kf;
}

template<class GlobalPosition, class Scalar = double>
Scalar exactBulkSource(const GlobalPosition& globalPos,
                       Scalar facetPermeability,
                       Scalar aperture)
{
    const auto& a = aperture;
    const auto& kf = facetPermeability;
    const auto& x = globalPos[0];
    const auto& y = globalPos[1];
    const Scalar sign = y > 0.0 ? 1.0 : -1.0;
    return -6.0*x - 6.0*y - 2.0*y - sign*a/kf;
}

template<class Geometry, class Scalar = double>
Scalar integralFacetSource(const Geometry& geometry,
                           Scalar facetPermeability)
{
    Scalar integral = 0.0;
    static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
    auto rule = Dune::QuadratureRules<Scalar, Geometry::mydimension>::rule(geometry.type(), intOrder);
    for (const auto& qp : rule)
        integral += exactFacetSource(geometry.global(qp.position()), facetPermeability)
                    *geometry.integrationElement(qp.position())
                    *qp.weight();
    return integral;
}

template<class Geometry, class Scalar = double>
Scalar integralBulkSource(const Geometry& geometry,
                          Scalar facetPermeability,
                          Scalar aperture)
{
    Scalar integral = 0.0;
    static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
    auto rule = Dune::QuadratureRules<Scalar, Geometry::mydimension>::rule(geometry.type(), intOrder);
    for (const auto& qp : rule)
        integral += exactBulkSource(geometry.global(qp.position()), facetPermeability, aperture)
                    *geometry.integrationElement(qp.position())
                    *qp.weight();
    return integral;
}

template<class Geometry, class Scalar = double>
Scalar integralBulkSolution(const Geometry& geometry,
                            Scalar facetPermeability,
                            Scalar aperture)
{
    Scalar integral = 0.0;
    static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
    auto rule = Dune::QuadratureRules<Scalar, Geometry::mydimension>::rule(geometry.type(), intOrder);
    for (const auto& qp : rule)
        integral += exactBulkSolution(geometry, geometry.global(qp.position()), facetPermeability, aperture)
                    *geometry.integrationElement(qp.position())
                    *qp.weight();
    return integral;
}

template<class GlobalPosition, class Scalar = double>
Scalar exactFacetNormalFlux(const GlobalPosition& globalPos,
                            const GlobalPosition& normal,
                            Scalar facetPermeability)
{
    const auto& kf = facetPermeability;
    const auto& x = globalPos[0];
    return -kf*3.0*x*x;
}

template<class GlobalPosition, class Scalar = double>
Scalar exactBulkNormalFlux(const GlobalPosition& globalPos,
                           const GlobalPosition& normal,
                           Scalar bulkPermeability,
                           Scalar facetPermeability,
                           Scalar aperture)
{
    const auto& kf = facetPermeability;
    const auto& a = aperture;
    const auto& x = globalPos[0];
    const auto& y = globalPos[1];
    const Scalar sign = y > 0.0 ? 1.0 : -1.0;

    GlobalPosition gradU(0.0);
    gradU[0] = 3.0*x*x + (2.0*y + sign*a/kf)*(x + 0.5);
    gradU[1] = 3.0*y*y + (x + 0.5)*(x + 0.5);
    return -1.0*bulkPermeability*(gradU*normal);
}

template<class Geometry, class Scalar=double>
Scalar integralBulkNormalFlux(const Geometry& geometry,
                             const typename Geometry::GlobalCoordinate& normal,
                             Scalar bulkPermeability,
                             Scalar facetPermeability,
                             Scalar aperture)
{
    Scalar integral = 0.0;
    static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
    auto rule = Dune::QuadratureRules<Scalar, Geometry::mydimension>::rule(geometry.type(), intOrder);
    for (const auto& qp : rule)
        integral += exactBulkNormalFlux(geometry.global(qp.position()), normal, bulkPermeability, facetPermeability, aperture)
                    *geometry.integrationElement(qp.position())
                    *qp.weight();
    return integral;
}

template<class GlobalPosition, class Scalar=double>
Scalar exactFluxJump(const GlobalPosition& globalPos,
                     Scalar bulkPermeability,
                     Scalar facetPermeability,
                     Scalar aperture)
{
    return exactBulkNormalFlux(globalPos, GlobalPosition({0.0, 1.0}),
                               bulkPermeability, facetPermeability, aperture)
           + exactBulkNormalFlux(globalPos, GlobalPosition({0.0, -1.0}),
                                 bulkPermeability, facetPermeability, aperture);

}

} // end namespace Dumux

#endif
