// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase flow test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <dune/common/unused.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include "box_cont_problem.hh"

#include <dumux/common/parameters.hh>
#include <examples/common/solver_box_cont.hh>
#include "computeerrornorm.hh"

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    DUNE_UNUSED const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // get the csv/vtk file names from the input file
    Dune::ParameterTree tree;
    tree["IO.VtkName"] = getParam<std::string>("IO.VtkName");
    tree["IO.CsvFileNameBody"] = getParam<std::string>("IO.CsvFileNameBody");

    // run simulation
    const auto result = solveBoxCont<>(tree);

    // compute error norms
    const auto facetPerm = getParam<double>("Facet.SpatialParams.Permeability");
    const auto aperture = getParam<double>("Facet.SpatialParams.Aperture");
    auto evalExact = [&] (const auto& eg, const auto& x) { return exactBulkSolution(eg, x, facetPerm, aperture); };

    const auto h = computeDiscretizationLength(*result.gridGeometry);
    const auto bulkErrorPair = computeSolutionNorm(*result.gridGeometry, result.solution, aperture, evalExact);
    std::cout << "Computed the following error norms: " << bulkErrorPair.first << std::endl;

    // write the norms into error file
    std::ofstream errorFile(getParam<std::string>("L2Error.OutputFile"), std::ios::out);
    std::ofstream refFile(getParam<std::string>("L2Error.ReferenceFile"), std::ios::out);
    errorFile << h << "," << bulkErrorPair.first << std::endl;
    refFile << h << "," << bulkErrorPair.second << std::endl;
    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
