import matplotlib.pyplot as plt
import numpy as np
import math
import copy
import sys

sys.path.append("../common")
from plotstyles import *

if len(sys.argv) < 2:
    sys.exit("Please provide the facet permeability\n")

kf = sys.argv[1]
errorsBulkPressure = np.genfromtxt('errors_bulk_p.csv', delimiter=',', names=True, deletechars='')
errorsFacetPressure = np.genfromtxt('errors_facet_p.csv', delimiter=',', names=True, deletechars='')
errorsBulkFlux = np.genfromtxt('errors_bulk_q.csv', delimiter=',', names=True, deletechars='')

# we use the integral of the finest run to scale errors (here we use the tpfa result)
lastRefinement = len(errorsBulkPressure['h'])-1
refValues = np.loadtxt('referencevalues_' + str(lastRefinement) + '_tpfa.csv', delimiter=',')
refValueBulkPressure = refValues[1]
refValueFacetPressure = refValues[2]
refValueBulkFlux = refValues[3]

# keep track of maximum values to plot second order reference line
maxBulkPressureError = 0.0
maxFacetPressureError = 0.0
maxBulkFluxError = 0.0

schemeNames = open('errors_bulk_p.csv').readlines()[0].strip('h').strip(',').strip('\n').split(',')
for curSchemeName in names:
    if curSchemeName == 'mpfa':
        continue

    schemeSuffixes = [''] if 'box-mortar' != curSchemeName else ['_double', '_half', '_nonconforming']
    schemeId = getSchemeId(curSchemeName)

    for suffix in schemeSuffixes:
        lineStyle = linestyles[schemeId]
        marker='*'

        schemeName = curSchemeName + suffix
        labelSuffix = ''
        if suffix == '_double': labelSuffix = r' ($\psi = 0.5$)'; lineStyle = '--'
        elif suffix == '_half': labelSuffix = r' ($\psi = 2.0$)'; lineStyle = 'dashdot'; marker='o'
        elif suffix == '_nonconforming': labelSuffix = r' ($\psi = 1.2$)'; lineStyle = '-'; marker='v'

        # bulk error
        plt.figure(1)
        plt.loglog(1.0/errorsBulkPressure['h'], errorsBulkPressure[schemeName]/refValueBulkPressure,
                   label=labels[schemeId] + labelSuffix, color=colors[schemeId],
                   linestyle=lineStyle, marker=marker)
        maxBulkPressureError = max(maxBulkPressureError, errorsBulkPressure[schemeName][0]/refValueBulkPressure)

        # facet error
        if schemeName not in ["box_cont", "box-cont"]:
            plt.figure(2)
            plt.loglog(1.0/errorsFacetPressure['h'], errorsFacetPressure[schemeName]/refValueFacetPressure,
                       label=labels[schemeId]+ labelSuffix, color=colors[schemeId],
                       linestyle=lineStyle, marker=marker)
            maxFacetPressureError = max(maxFacetPressureError, errorsFacetPressure[schemeName][0]/refValueFacetPressure)

            # interface flux error
            plt.figure(3)
            plt.loglog(1.0/errorsBulkFlux['h'], errorsBulkFlux[schemeName]/refValueBulkFlux,
                       label=labels[schemeId]+ labelSuffix, color=colors[schemeId],
                       linestyle=lineStyle, marker=marker)
            maxBulkFluxError = max(maxBulkFluxError, errorsBulkFlux[schemeName][0]/refValueBulkFlux)

# add first-order reference line
factor = 2.0
bulkPressureSecondOrder = [maxBulkPressureError*factor]
facetPressureSecondOrder = [maxFacetPressureError*factor]
bulkFluxSecondOrder = [maxBulkFluxError*factor]
for i in range(1, len(errorsBulkPressure['h'])):
    hCur = errorsBulkPressure['h'][i]
    hPrev = errorsBulkPressure['h'][i-1]
    scaleFactor = math.pow(hPrev/hCur, 1)
    bulkPressureSecondOrder.append(bulkPressureSecondOrder[i-1]/scaleFactor)
    facetPressureSecondOrder.append(facetPressureSecondOrder[i-1]/scaleFactor)
    bulkFluxSecondOrder.append(bulkFluxSecondOrder[i-1]/scaleFactor)
plt.figure(1);
plt.loglog(1.0/errorsBulkPressure['h'], bulkPressureSecondOrder,
           label=r'$\mathcal{O} (\eta)$', color='k', alpha=0.5, linestyle='dashed')
plt.legend(); plt.title(r'$k = \num{' + kf + '}$'); plt.xlabel(r'$1/ \eta$'); plt.ylabel(r'$\varepsilon_{h_2}$'); plt.savefig('errors_bulk.pdf', bbox_inches='tight')

plt.figure(2);
plt.loglog(1.0/errorsBulkPressure['h'], facetPressureSecondOrder,
           label=r'$\mathcal{O} (\eta)$', color='k', alpha=0.5, linestyle='dashed')
plt.legend(); plt.title(r'$k = \num{' + kf + '}$'); plt.xlabel(r'$1/ \eta$'); plt.ylabel(r'$\varepsilon_{h_1}$'); plt.savefig('errors_facet.pdf', bbox_inches='tight')

plt.figure(3);
plt.loglog(1.0/errorsBulkPressure['h'], bulkFluxSecondOrder,
           label=r'$\mathcal{O} (\eta)$', color='k', alpha=0.5, linestyle='dashed')
plt.legend(); plt.title(r'$k = \num{' + kf + '}$'); plt.xlabel(r'$1/ \eta$'); plt.ylabel(r'$\varepsilon_{\lambda}$'); plt.savefig('errors_ifflux.pdf', bbox_inches='tight')
