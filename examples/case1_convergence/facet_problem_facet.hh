// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the convergence test.
 */
#ifndef DUMUX_GLAESER_CONVERGENCE_FACET_FACET_PROBLEM_HH
#define DUMUX_GLAESER_CONVERGENCE_FACET_FACET_PROBLEM_HH

#include <dune/geometry/type.hh>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dumux/porousmediumflow/problem.hh>
#include "exactsolution.hh"

namespace Dumux {

/*!
 * \brief The problem for the lower-dimensional domain in the convergence test.
 */
template<class TypeTag>
class OnePFacetProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePFacetProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     std::shared_ptr<CouplingManager> couplingManager,
                     const std::string& paramGroup)
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , bulkPermeability_(getParam<Scalar>("Bulk.SpatialParams.Permeability"))
    , permeability_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability"))
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    {
        exactSol_.resize(gridGeometry->numDofs(), 0.0);

        if (isBox)
        {
            for (const auto& e : elements(gridGeometry->gridView()))
                for (int i = 0; i < e.subEntities(dim); ++i)
                    exactSol_[gridGeometry->vertexMapper().subIndex(e, i, dim)]
                        = exactFacetSolution(e.geometry().corner(i));
        }
        else
        {
            for (const auto& e : elements(gridGeometry->gridView()))
                exactSol_[gridGeometry->elementMapper().index(e)]
                    = exactFacetSolution(e.geometry().center());
        }
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        static const bool useNeumann = getParam<bool>("Facet.UseLateralNeumannBCs", false);
        if (useNeumann) values.setAllNeumann();
        else values.setAllDirichlet();
        return values;
    }

    //! Evaluate the source term at a given position
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        NumEqVector source(bulkFluxJump(element, fvGeometry, elemVolVars, scv));
        source /= elemVolVars[scv].extrusionFactor();
        source += integralFacetSource(scv.geometry(), permeability_)/scv.volume();
        return source;
    }

    //! Evaluate the bulk flux jump term for an scv
    Scalar bulkFluxJump(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto jump = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        jump /= scv.volume();
        return jump;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return exactFacetSolution(globalPos); }

    //! Evaluates the Neumann boundary conditions of an scvf
    template<class ElemVolVars, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElemVolVars& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        return exactFacetNormalFlux(scvf.ipGlobal(),
                                    scvf.unitOuterNormal(),
                                    permeability_);
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return aperture_; }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    // write the transfer source term to disk
    template<class BindContextFunc, class GridVariables, class SolutionVector>
    void writeCsvData(const std::string& csvFileBody,
                      BindContextFunc&& bindContext,
                      const GridVariables& gridVars,
                      const SolutionVector& x) const
    {
        std::ofstream sourcePlot(csvFileBody + ".csv", std::ios::out);
        sourcePlot << "\"x\",\"fluxJump\",\"exactFluxJump\"" << std::endl;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());

            bindContext(element);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto exactJump = exactFluxJump(scv.center(), bulkPermeability_, permeability_, aperture_);
                const auto discFluxJump = bulkFluxJump(element, fvGeometry, elemVolVars, scv);
                sourcePlot << scv.center()[0] << "," << discFluxJump << "," << exactJump << "\n";
            }
        }
    }

    // exact solution int vtk output
    template<class... Args> void updateOutputFields(Args&&... x) const {}
    template<class Writer> void addOutputFields(Writer& writer) const
    {
        const auto ft = isBox ? Writer::FieldType::vertex
                              : Writer::FieldType::element;
        writer.addField(exactSol_, "exact", ft);
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar bulkPermeability_;
    Scalar permeability_;
    Scalar aperture_;
    std::vector<Scalar> exactSol_;
};

} // end namespace Dumux

#endif
