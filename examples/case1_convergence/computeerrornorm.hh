// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Helper functions for computing error norms of this test.
 */
#ifndef DUMUX_GLAESER_CONVERGENCE_COMPUTENORMS_HH
#define DUMUX_GLAESER_CONVERGENCE_COMPUTENORMS_HH

#include <cmath>
#include <limits>

#include <dune/geometry/quadraturerules.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>
#include "exactsolution.hh"

namespace Dumux {

template<class GridGeometry, class Scalar=double>
Scalar computeDiscretizationLength(const GridGeometry& gridGeometry)
{
    using std::min;
    Scalar h = std::numeric_limits<Scalar>::max();
    for (const auto& element : elements(gridGeometry.gridView()))
        h = min(h, diameter(element.geometry()));
    return h;
}

template<class GridGeometry, class SolutionVector, class ExactEvaluator, class Scalar=double>
std::pair<Scalar, Scalar>
computeSolutionNorm(const GridGeometry& gridGeometry,
                    const SolutionVector& x,
                    Scalar aperture,
                    ExactEvaluator&& exactSolution)
{
    Scalar norm = 0.0;
    Scalar exactIntegral = 0.0;
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        const auto elemGeom = element.geometry();
        const auto elemSol = elementSolution(element, x, gridGeometry);

        using GridView = typename GridGeometry::GridView;
        using QuadRules = Dune::QuadratureRules<Scalar, GridView::dimension>;

        // lambda that performs the integration
        const auto order = getParam<Scalar>("L2Error.IntegrationOrder");
        auto rule = QuadRules::rule(elemGeom.type(), order);
        for (const auto& qp : rule)
        {
            const auto ip = elemGeom.global(qp.position());
            const auto xLocal = evalSolution(element, elemGeom, gridGeometry, elemSol, ip);
            const auto error = exactSolution(elemGeom, ip) - xLocal[0];
            norm += error*error*qp.weight()*elemGeom.integrationElement(qp.position());
            exactIntegral += exactSolution(elemGeom, ip)
                             *exactSolution(elemGeom, ip)
                             *qp.weight()*elemGeom.integrationElement(qp.position());
        }
    }

    using std::sqrt;
    return std::make_pair(sqrt(norm), sqrt(exactIntegral));
}

template<class GG, class GV, class Sol,
         class BindContext, class Scalar=double>
std::pair<Scalar, Scalar>
computeFluxJumpNorm(const GG& gridGeometry,
                    const GV& gridVariables,
                    const Sol& x,
                    Scalar bulkPermeability,
                    Scalar facetPermeability,
                    Scalar aperture,
                    BindContext&& bindContext)
{
    Scalar norm = 0.0;
    Scalar exactIntegral = 0.0;

    const auto& problem = gridVariables.curGridVolVars().problem();
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

        bindContext(element);
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto diskFluxJump = problem.bulkFluxJump(element, fvGeometry, elemVolVars, scv);

            using GridView = typename GG::GridView;
            using QuadRules = Dune::QuadratureRules<Scalar, GridView::dimension>;
            const auto order = getParam<Scalar>("L2Error.IntegrationOrder");
            const auto& scvGeom = scv.geometry();
            auto rule = QuadRules::rule(scvGeom.type(), order);

            for (const auto& qp : rule)
            {
                const auto ip = scvGeom.global(qp.position());
                const auto exactJump = exactFluxJump(ip, bulkPermeability, facetPermeability, aperture);
                const auto error = exactJump - diskFluxJump;
                norm += error*error*qp.weight()*scvGeom.integrationElement(qp.position());
                exactIntegral += exactJump*exactJump*qp.weight()*scvGeom.integrationElement(qp.position());
            }
        }
    }

    using std::sqrt;
    return std::make_pair(sqrt(norm), sqrt(exactIntegral));
}

template<class FluxVariables,
         class GG, class GV, class Sol, class CM,
         class Id, class BindContext, class ExactEvaluator,
         class Scalar=double>
std::pair<Scalar, Scalar>
computeFluxNorm(const GG& gridGeometry,
                const GV& gridVariables,
                const Sol& x,
                const CM& couplingManager,
                Id domainId,
                BindContext&& bindContext,
                ExactEvaluator&& exactFlux)
{
    Scalar norm = 0.0;
    Scalar exactIntegral = 0.0;
    bool visitedInterface = false;
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

        bool hasInterface = false;
        fvGeometry.bindElement(element);
        for (const auto& scvf : scvfs(fvGeometry))
            if (couplingManager.isOnInteriorBoundary(element, scvf))
                hasInterface = true;

        if (hasInterface)
        {
            visitedInterface = true;
            bindContext(element);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager.isOnInteriorBoundary(element, scvf))
                    continue;

                FluxVariables fluxVars;
                fluxVars.init(couplingManager.problem(domainId), element, fvGeometry,
                              elemVolVars, scvf, elemFluxVarsCache);
                const auto discFlux = fluxVars.advectiveFlux(0, [&] (const auto& vv) { return 1.0; })
                                      /scvf.area();

                // for low order, do point-wise integration with scvf integration point
                const auto order = getParam<Scalar>("L2Error.IntegrationOrder");
                if (order == 0 || order == 1)
                {
                    const auto exact = exactFlux(scvf.ipGlobal(), scvf.unitOuterNormal());
                    const auto error = exact - discFlux;
                    norm += error*error*scvf.area();
                    exactIntegral += exact*exact*scvf.area();
                }
                else
                {
                    using GridView = typename GG::GridView;
                    using QuadRules = Dune::QuadratureRules<Scalar, GridView::dimension-1>;
                    const auto& scvfGeom = scvf.geometry();
                    auto rule = QuadRules::rule(scvfGeom.type(), order);

                    for (const auto& qp : rule)
                    {
                        const auto ip = scvfGeom.global(qp.position());
                        const auto exact = exactFlux(ip, scvf.unitOuterNormal());
                        const auto error = exact - discFlux;
                        norm += error*error*qp.weight()*scvfGeom.integrationElement(qp.position());
                        exactIntegral += exact*exact*qp.weight()*scvfGeom.integrationElement(qp.position());
                    }
                }
            }
        }
    }

    if (!visitedInterface)
        std::cout << "WARNING: Did not visit any interfaces. Error norm will be zero" << std::endl;

    using std::sqrt;
    return std::make_pair(sqrt(norm), sqrt(exactIntegral));
}

} // end namespace Dumux

#endif
