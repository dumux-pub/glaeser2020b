// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver for three-domain facet coupling models.
 */
#ifndef DUMUX_GLAESER2020B_THREEDOMAIN_FACET_SOLVER_HH
#define DUMUX_GLAESER2020B_THREEDOMAIN_FACET_SOLVER_HH

#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>

namespace Dumux {

// define some convenience aliasess to be used below in the property definitions and in main
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag>
struct FacetSolverTestTraits
{
    using BulkGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGridGeometry = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using IntersectionGridGeometry = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingThreeDomainMapper<BulkGridGeometry, FacetGridGeometry, IntersectionGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingThreeDomainManager<MDTraits, CouplingMapper>;
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
};

// return type of solveFacet(), containing the
// solutions and the grid views on which they were computed
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag>
struct FacetSolveResult
{
private:
    using Assembler = typename FacetSolverTestTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>::Assembler;
    using CouplingManager = typename FacetSolverTestTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>::CouplingManager;

    using BulkGrid = Dumux::GetPropType<BulkTypeTag,  Dumux::Properties::Grid>;
    using BulkGG = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using BulkGV = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridVariables>;
    using BulkSolution = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::SolutionVector>;

    using FacetGrid = Dumux::GetPropType<FacetTypeTag,  Dumux::Properties::Grid>;
    using FacetGG = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGV = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridVariables>;
    using FacetSolution = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::SolutionVector>;

    using IntersectionGrid = Dumux::GetPropType<IntersectionTypeTag,  Dumux::Properties::Grid>;
    using IntersectionGG = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using IntersectionGV = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridVariables>;
    using IntersectionSolution = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::SolutionVector>;

public:
    Dumux::FacetCouplingGridManager<BulkGrid, FacetGrid, IntersectionGrid> gridManager;
    std::shared_ptr<CouplingManager> couplingManager;
    std::shared_ptr<Assembler> assembler;

    std::shared_ptr<BulkGG> bulkGridGeometry;
    std::shared_ptr<FacetGG> facetGridGeometry;
    std::shared_ptr<IntersectionGG> intersectionGridGeometry;

    std::shared_ptr<BulkGV> bulkGridVariables;
    std::shared_ptr<FacetGV> facetGridVariables;
    std::shared_ptr<IntersectionGV> intersectionGridVariables;

    BulkSolution bulkSolution;
    FacetSolution facetSolution;
    IntersectionSolution intersectionSolution;
};

// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class GridGeometry, class GridManager, class FacetGridView>
void updateGridGeometry(GridGeometry& gridGeometry,
                        const GridManager& gridManager,
                        const FacetGridView& facetGridView)
{
    if constexpr (GridGeometry::discMethod == Dumux::DiscretizationMethod::box)
    {
        static constexpr int higherGridId = int(GridGeometry::GridView::dimension) == 3 ? 0 : 1;
        using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings, higherGridId, higherGridId+1>;
        BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
        gridGeometry.update(facetGridView, facetGridAdapter);
    }
    else
    {
        gridGeometry.update();
    }
}

// run the simulation for the type tags defined in CMakeLists.txt (see above)
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag, class LinearSolver=UMFPackBackend>
FacetSolveResult<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>
solveFacet(const Dune::ParameterTree& paramTree)
{
    using namespace Dumux;
    using FacetSolverTestTraits = FacetSolverTestTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>;

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    using FacetSolveResult = FacetSolveResult<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>;
    FacetSolveResult result;
    auto& gridManager = result.gridManager;
    gridManager.init(paramTree.template get<std::string>("Grid.ParamGroup", "Grid"));
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();
    const auto& intersectionGridView = gridManager.template grid<2>().leafGridView();

    // create the finite volume grid geometries
    using BulkGridGeometry = typename FacetSolverTestTraits::BulkGridGeometry;
    using FacetGridGeometry = typename FacetSolverTestTraits::FacetGridGeometry;
    using IntersectionGridGeometry = typename FacetSolverTestTraits::IntersectionGridGeometry;

    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    auto facetGridGeometry = std::make_shared<FacetGridGeometry>(facetGridView);
    auto intersectionGridGeometry = std::make_shared<IntersectionGridGeometry>(intersectionGridView);

    updateGridGeometry(*bulkGridGeometry, gridManager, facetGridView);
    updateGridGeometry(*facetGridGeometry, gridManager, intersectionGridView);
    intersectionGridGeometry->update();

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename FacetSolverTestTraits::CouplingMapper>();
    couplingMapper->update(*bulkGridGeometry, *facetGridGeometry, *intersectionGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename FacetSolverTestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    using IntersectionProblem = GetPropType<IntersectionTypeTag, Properties::Problem>;

    const auto bulkParamGroup = paramTree.template get<std::string>("Bulk.ParamGroup", "Bulk");
    const auto facetParamGroup = paramTree.template get<std::string>("Facet.ParamGroup", "Facet");
    const auto intersectionParamGroup = paramTree.template get<std::string>("Intersection.ParamGroup", "Intersection");
    const auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<1>();
    const auto intersectionGridData = gridManager.getGridData()->template getSubDomainGridData<2>();

    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkGridGeometry, bulkParamGroup);
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetGridGeometry, facetGridData, facetParamGroup);
    auto intersectionSpatialParams = std::make_shared<typename IntersectionProblem::SpatialParams>(intersectionGridGeometry, intersectionGridData, intersectionParamGroup);

    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, bulkSpatialParams, couplingManager, bulkParamGroup);
    auto facetProblem = std::make_shared<FacetProblem>(facetGridGeometry, facetSpatialParams, couplingManager, facetParamGroup);
    auto intersectionProblem = std::make_shared<IntersectionProblem>(intersectionGridGeometry, intersectionSpatialParams, couplingManager, intersectionParamGroup);

    // the solution vector
    using MDTraits = typename FacetSolverTestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    static const auto intersectionId = typename MDTraits::template SubDomain<2>::Index();
    x[bulkId].resize(bulkGridGeometry->numDofs());   x[bulkId] = 0.0;
    x[facetId].resize(facetGridGeometry->numDofs()); x[facetId] = 0.0;
    x[intersectionId].resize(intersectionGridGeometry->numDofs()); x[intersectionId] = 0.0;

    // initialize coupling manager
    couplingManager->init(bulkProblem, facetProblem, intersectionProblem, couplingMapper, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    using IntersectionGridVariables = GetPropType<IntersectionTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetGridGeometry);
    auto intersectionGridVariables = std::make_shared<IntersectionGridVariables>(intersectionProblem, intersectionGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);
    intersectionGridVariables->init(x[intersectionId]);

    // intialize the vtk output module
    using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkTypeTag, Properties::SolutionVector>>;
    using FacetVtkWriter = VtkOutputModule<FacetGridVariables, GetPropType<FacetTypeTag, Properties::SolutionVector>>;
    using IntersectionVtkWriter = VtkOutputModule<IntersectionGridVariables, GetPropType<IntersectionTypeTag, Properties::SolutionVector>>;
    const auto bulkDM = BulkGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    const auto facetDM = FacetGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], paramTree["Bulk.IO.VtkName"], bulkParamGroup, bulkDM);
    FacetVtkWriter facetVtkWriter(*facetGridVariables, x[facetId], paramTree["Facet.IO.VtkName"], facetParamGroup, facetDM);
    IntersectionVtkWriter intersectionVtkWriter(*intersectionGridVariables, x[intersectionId], paramTree["Intersection.IO.VtkName"], intersectionParamGroup);

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkTypeTag, Properties::IOFields>;
    using FacetIOFields = GetPropType<FacetTypeTag, Properties::IOFields>;
    using IntersectionIOFields = GetPropType<IntersectionTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    FacetIOFields::initOutputModule(facetVtkWriter);
    IntersectionIOFields::initOutputModule(intersectionVtkWriter);
    bulkProblem->addOutputFields(bulkVtkWriter);
    facetProblem->addOutputFields(facetVtkWriter);
    intersectionProblem->addOutputFields(intersectionVtkWriter);

    // write initial solution
    bulkVtkWriter.write(0.0);
    facetVtkWriter.write(0.0);
    intersectionVtkWriter.write(0.0);

    // the assembler
    using Assembler = typename FacetSolverTestTraits::Assembler;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem, intersectionProblem),
                                                  std::make_tuple(bulkGridGeometry, facetGridGeometry, intersectionGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables, intersectionGridVariables),
                                                  couplingManager);

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto linearSolver = std::make_shared<LinearSolver>();
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // linearize & solve
    newtonSolver->solve(x);

    if (getParam<bool>("IO.WriteMatrix", false))
    {
        using Converter = MatrixConverter<typename Assembler::JacobianMatrix>;
        auto M = Converter::multiTypeToBCRSMatrix(assembler->jacobian());
        std::ofstream matrixFile(paramTree["Bulk.IO.VtkName"] + "_matrix.mtx", std::ios::out);
        Dune::writeMatrixMarket(M, matrixFile);
    }

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    facetGridVariables->update(x[facetId]);
    intersectionGridVariables->update(x[intersectionId]);

    // lambdas for coupling context update (for some reason compilation fails when using bulkId/facetId)
    auto bindBulkContext = [&] (const auto& element) -> void { couplingManager->bindCouplingContext(Dune::index_constant<0>(), element, *assembler); };
    auto bindFacetContext = [&] (const auto& element) -> void { couplingManager->bindCouplingContext(Dune::index_constant<1>(), element, *assembler); };
    auto bindIntersectionContext = [&] (const auto& element) -> void { couplingManager->bindCouplingContext(Dune::index_constant<2>(), element, *assembler); };

    // update problem-dependent output fields
    bulkProblem->updateOutputFields(bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->updateOutputFields(bindFacetContext, *facetGridVariables, x[facetId]);
    intersectionProblem->updateOutputFields(bindIntersectionContext, *intersectionGridVariables, x[intersectionId]);

    // write vtk output
    bulkVtkWriter.write(1.0);
    facetVtkWriter.write(1.0);
    intersectionVtkWriter.write(1.0);

    // write csv data
    bulkProblem->writeCsvData(paramTree["Bulk.IO.CsvFileNameBody"], bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->writeCsvData(paramTree["Facet.IO.CsvFileNameBody"], bindFacetContext, *facetGridVariables, x[facetId]);
    intersectionProblem->writeCsvData(paramTree["Intersection.IO.CsvFileNameBody"], bindIntersectionContext, *intersectionGridVariables, x[intersectionId]);

    result.couplingManager = couplingManager;
    result.assembler = assembler;
    result.bulkGridGeometry = bulkGridGeometry;
    result.facetGridGeometry = facetGridGeometry;
    result.intersectionGridGeometry = intersectionGridGeometry;
    result.bulkGridVariables = bulkGridVariables;
    result.facetGridVariables = facetGridVariables;
    result.intersectionGridVariables = intersectionGridVariables;
    result.bulkSolution = x[bulkId];
    result.facetSolution = x[facetId];
    result.intersectionSolution = x[intersectionId];
    return result;
}

// run the simulation using default parameters
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag, class LinearSolver=UMFPackBackend>
FacetSolveResult<BulkTypeTag, FacetTypeTag, IntersectionTypeTag> solveFacet()
{
    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Grid";
    tree["Bulk.ParamGroup"] = "Bulk";
    tree["Facet.ParamGroup"] = "Facet";
    tree["Intersection.ParamGroup"] = "Intersection";
    tree["Bulk.IO.VtkName"] = getParam<std::string>("Bulk.IO.VtkName", "bulk");
    tree["Facet.IO.VtkName"] = getParam<std::string>("Facet.IO.VtkName", "facet");
    tree["Intersection.IO.VtkName"] = getParam<std::string>("Intersection.IO.VtkName", "intersection");
    tree["Bulk.IO.CsvFileNameBody"] = getParam<std::string>("Bulk.IO.CsvFileNameBody", "bulk_data");
    tree["Facet.IO.CsvFileNameBody"] = getParam<std::string>("Facet.IO.CsvFileNameBody", "facet_data");
    tree["Intersection.IO.CsvFileNameBody"] = getParam<std::string>("Intersection.IO.CsvFileNameBody", "intersection_data");

    return solveFacet<BulkTypeTag, FacetTypeTag, IntersectionTypeTag, LinearSolver>(tree);
}

} // end namespace Dumux

#endif
