// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the mortar problem between the bulk
 *        and facet domain.
 */
#ifndef DUMUX_GLAESER2020B_BULK_FACET_MORTAR_PROPERTIES_HH
#define DUMUX_GLAESER2020B_BULK_FACET_MORTAR_PROPERTIES_HH

#include <dune/foamgrid/foamgrid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/properties/model.hh>

#include <dumux/discretization/fem.hh>
#include <dumux/discretization/fem/fegridgeometry.hh>

#include "problem_mortar.hh"

#ifndef MORTARFACETGRID
#define MORTARFACETGRID Dune::FoamGrid<DIMWORLD-1, DIMWORLD>
#endif

#ifndef MORTARISGRID
#define MORTARISGRID Dune::FoamGrid<DIMWORLD-2, DIMWORLD>
#endif

namespace Dumux {

struct MortarModelTraits
{
    static constexpr int numEq() { return 1; }
};

namespace Properties {

// Type Tags
namespace TTag {
// TypeTag node for the mortar between bulk & facet domains
struct BulkFacetMortar { using InheritsFrom = std::tuple<ModelProperties, FEMModel>; };

// TypeTag node for the mortar between facet & intersection domains
struct FacetIntersectionMortar { using InheritsFrom = std::tuple<ModelProperties, FEMModel>; };
} // end namespace TTag

// define the grid type (DIMWORLD must be defined in CMakeLists.txt)
template<class TypeTag>
struct Grid<TypeTag, TTag::BulkFacetMortar> { using type = MORTARFACETGRID; };
template<class TypeTag>
struct Grid<TypeTag, TTag::FacetIntersectionMortar> { using type = MORTARISGRID; };

namespace Detail {
    template<class GridView>
    using PQ0FEGridGeometry = FEGridGeometry< Dune::Functions::LagrangeBasis<GridView, 0> >;
} // end namespace Detail

// grid geometry type
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::BulkFacetMortar>
{ using type = Detail::PQ0FEGridGeometry< typename GetPropType<TypeTag, Grid>::LeafGridView >; };
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::FacetIntersectionMortar>
{ using type = Detail::PQ0FEGridGeometry< typename GetPropType<TypeTag, Grid>::LeafGridView >; };

// the problem class type
template<class TypeTag>
struct Problem<TypeTag, TTag::BulkFacetMortar> { using type = MortarProblem<TypeTag>; };
template<class TypeTag>
struct Problem<TypeTag, TTag::FacetIntersectionMortar> { using type = MortarProblem<TypeTag>; };

// The model traits
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::BulkFacetMortar> { using type = MortarModelTraits; };
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::FacetIntersectionMortar> { using type = MortarModelTraits; };

} // end namespace Properties
} // end namespace Dumux

#endif
