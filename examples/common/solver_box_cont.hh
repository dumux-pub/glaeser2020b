// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver for the box-cont model.
 */
#ifndef DUMUX_GLAESER2020B_BOX_CONT_SOLVER_HH
#define DUMUX_GLAESER2020B_BOX_CONT_SOLVER_HH

#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>
#include <examples/common/properties_box_cont.hh>

namespace Dumux {

// return type of solveBoxCont()
template<class TypeTag>
struct BoxContSolveResult
{
private:
    using Grid = Dumux::GetPropType<TypeTag,  Dumux::Properties::Grid>;
    using GG = Dumux::GetPropType<TypeTag, Dumux::Properties::GridGeometry>;
    using Solution = Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector>;
    using FacetGrid = Dune::FoamGrid<Grid::dimension-1, Grid::dimensionworld>;

public:
    using GridManager = Dumux::FacetCouplingGridManager<Grid, FacetGrid>;
    GridManager gridManager;
    std::shared_ptr<GG> gridGeometry;
    Solution solution;
};

// run the simulation for given combo of type tags
template<class TypeTag=Dumux::Properties::TTag::OnePBoxCont, class LinearSolver=UMFPackBackend>
BoxContSolveResult<TypeTag> solveBoxCont(const Dune::ParameterTree& paramTree)
{
    using namespace Dumux;

    using BoxContSolveResult = BoxContSolveResult<TypeTag>;
    BoxContSolveResult result;
    auto& gridManager = result.gridManager;
    gridManager.init(paramTree.template get<std::string>("Grid.ParamGroup", "Grid"));

    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Embeddings = typename BoxContSolveResult::GridManager::Embeddings;

    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    auto facetGridAdapter = CodimOneGridAdapter<Embeddings>(gridManager.getEmbeddings());
    gridGeometry->update(facetGridAdapter);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs()); x = 0.0;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using GridView = typename GridGeometry::GridView;
    using FacetGrid = Dune::FoamGrid<GridView::dimension-1, GridView::dimensionworld>;
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FacetGrid>;
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;

    VtkOutputModule vtkWriter(*gridVariables, x, paramTree["IO.VtkName"], facetGridAdapter, "", Dune::VTK::nonconforming);
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    problem->addOutputFields(vtkWriter);
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the non-linear solver
    auto linearSolver = std::make_shared<LinearSolver>();
    Dumux::NewtonSolver<Assembler, LinearSolver> nonLinearSolver(assembler, linearSolver);

    // solve the non-linear system with time step control
    nonLinearSolver.solve(x);

    // write Jacobian matrix to matrix market file
    if (getParam<bool>("IO.WriteMatrix", false))
    {
        std::ofstream matrixFile(paramTree["IO.VtkName"] + "_matrix.mtx", std::ios::out);
        Dune::writeMatrixMarket(assembler->jacobian(), matrixFile);
    }

    // update variables
    gridVariables->update(x);

    // update problem-dependent output fields
    problem->updateOutputFields(*gridVariables, x);

    // write vtk file
    vtkWriter.write(1.0);

    // write csv data
    problem->writeCsvData(paramTree["IO.CsvFileNameBody"], *gridVariables, x);

    result.gridGeometry = gridGeometry;
    result.solution = x;
    return result;
}

// run the simulation using default parameters
template<class TypeTag=Dumux::Properties::TTag::OnePBoxCont, class LinearSolver=UMFPackBackend>
BoxContSolveResult<TypeTag> solveBoxCont()
{
    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Grid";
    tree["IO.VtkName"] = getParam<std::string>("IO.VtkName", "box_cont");
    tree["IO.CsvFileNameBody"] = getParam<std::string>("IO.CsvFileNameBody", "box_cont_data");

    return solveBoxCont<TypeTag, LinearSolver>(tree);
}

} // end namespace Dumux

#endif
