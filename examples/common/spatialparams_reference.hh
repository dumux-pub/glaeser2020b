// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the examples as used for equi-dimensional
 *        reference runs.
 */
#ifndef DUMUX_GLAESER2020B_REFERENCE_SPATIALPARAMS_HH
#define DUMUX_GLAESER2020B_REFERENCE_SPATIALPARAMS_HH

#include <cmath>
#include <dune/common/fmatrix.hh>

#include <dumux/io/grid/griddata.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the examples as used for equi-dimensional
 *        reference runs.
 */
template< class GridGeometry, class Scalar, class PermType >
class ReferenceSpatialParams
: public FVSpatialParamsOneP< GridGeometry, Scalar, ReferenceSpatialParams<GridGeometry, Scalar, PermType> >
{
    using ThisType = ReferenceSpatialParams< GridGeometry, Scalar, PermType >;
    using ParentType = FVSpatialParamsOneP< GridGeometry, Scalar, ThisType >;

    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dimWorld = GridView::dimensionworld;

    using GridData = Dumux::GridData<typename GridView::Grid>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = PermType;

    //! the constructor
    ReferenceSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                           std::shared_ptr<const GridData> gridDataPtr)
    : ParentType(gridGeometry)
    , gridDataPtr_(gridDataPtr)
    {
        setPermeabilities_(permeabilityMatrix_, permeabilityFracture_);
        porosityMatrix_ = getParam<Scalar>("Bulk.SpatialParams.Porosity");
        porosityFracture_ = getParam<Scalar>("Facet.SpatialParams.Porosity");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (isOnFracture(element)) return permeabilityFracture_;
        else if (isOnIntersection(element)) return permeabilityIntersection_;
        else return permeabilityMatrix_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     * \param globalPos The global position
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if (isOnFracture(element)) return porosityFracture_;
        else if (isOnIntersection(element)) return porosityIntersection_;
        else return porosityMatrix_;
    }

    //! define which elements are on the fracture
    template<class IndicatorFunc>
    void setFractureElements(IndicatorFunc&& indicator)
    {
        static_assert(dimWorld == 2, "For 3d setups, use setFractureAndIntersectionElements()");

        isOnFracture_.assign(this->gridGeometry().gridView().size(0), false);
        isOnIntersection_.assign(this->gridGeometry().gridView().size(0), false);
        for (const auto& e : elements(this->gridGeometry().gridView()))
            if (indicator(e))
                isOnFracture_[this->gridGeometry().elementMapper().index(e)] = true;
    }

    //! define which elements are on the fracture
    template<class FracIndicator, class IsIndicator>
    void setFractureAndIntersectionElements(FracIndicator&& fracIndicator,
                                            IsIndicator&& isIndicator)
    {
        static_assert(dimWorld == 3, "For 2d setups, use setFractureElements()");

        isOnFracture_.assign(this->gridGeometry().gridView().size(0), false);
        isOnIntersection_.assign(this->gridGeometry().gridView().size(0), false);
        for (const auto& e : elements(this->gridGeometry().gridView()))
        {
            const auto isFrac = fracIndicator(e);
            const auto isIntersection = isIndicator(e);

            if (isFrac && isIntersection)
                DUNE_THROW(Dune::InvalidStateException,
                           "Indicators state that an element is on both fracture and intersection");

            if (isFrac) isOnFracture_[this->gridGeometry().elementMapper().index(e)] = true;
            if (isIntersection) isOnIntersection_[this->gridGeometry().elementMapper().index(e)] = true;
        }
    }

    bool isOnFracture(const Element& element) const
    { return isOnFracture_[this->gridGeometry().elementMapper().index(element)]; }

    bool isOnIntersection(const Element& element) const
    { return isOnIntersection_[this->gridGeometry().elementMapper().index(element)]; }

    int getDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }
private:
    //! set scalar permeabilities
    void setPermeabilities_(Scalar& km, Scalar& kf)
    {
        km = getParam<Scalar>("Bulk.patialParams.Permeability");
        kf = getParam<Scalar>("Facet.SpatialParams.Permeability");
    }

    //! set tensorial permeabilities
    void setPermeabilities_(Dune::FieldMatrix<Scalar, dimWorld, dimWorld>& Km,
                            Dune::FieldMatrix<Scalar, dimWorld, dimWorld>& Kf)
    {
        const auto kh = getParam<Scalar>("Bulk.SpatialParams.Permeability");
        const auto ratio = getParam<Scalar>("Bulk.SpatialParams.PermeabilityAnisotropyRatio");
        const auto angle = getParam<Scalar>("Bulk.SpatialParams.PermeabilityAngle");
        const auto kv = kh/ratio;

        using std::sin;
        using std::cos;
        Scalar cost = cos(angle);
        Scalar sint = sin(angle);

        Km = 0.0;
        Km[0][0] = cost*cost*kh + sint*sint*kv;
        Km[1][1] = sint*sint*kh + cost*cost*kv;
        Km[0][1] = Km[1][0] = cost*sint*(kh - kv);

        const auto kf = getParam<Scalar>("Facet.SpatialParams.Permeability");
        Kf = 0.0;
        Kf[0][0] = kf;
        Kf[1][1] = kf;
    }

    std::shared_ptr<const GridData> gridDataPtr_;

    Scalar a_;
    Scalar porosityMatrix_;
    Scalar porosityFracture_;
    Scalar porosityIntersection_;
    PermeabilityType permeabilityMatrix_;
    PermeabilityType permeabilityFracture_;
    PermeabilityType permeabilityIntersection_;

    std::vector<bool> isOnFracture_;
    std::vector<bool> isOnIntersection_;
};

} // end namespace Dumux

#endif
