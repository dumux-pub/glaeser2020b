// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters class used for the bulk domain.
 */
#ifndef DUMUX_GLAESER2020B_BULK_SPATIALPARAMS_HH
#define DUMUX_GLAESER2020B_BULK_SPATIALPARAMS_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters class used for the bulk domain.
 * \note PermType can either be of type Scalar or Dune::FieldMatrix
 */
template< class GridGeometry, class Scalar, class PermType >
class BulkSpatialParams
: public FVSpatialParamsOneP< GridGeometry, Scalar, BulkSpatialParams<GridGeometry, Scalar, PermType> >
{
    using ThisType = BulkSpatialParams< GridGeometry, Scalar, PermType >;
    using ParentType = FVSpatialParamsOneP< GridGeometry, Scalar, ThisType >;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    //! export the type used for permeabilities
    using PermeabilityType = PermType;

    //! the constructor
    BulkSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                      const std::string& paramGroup)
    : ParentType(gridGeometry)
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        setPermeability_(permeability_, paramGroup);
    }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

private:
    //! set a scalar permeability
    void setPermeability_(Scalar& k, const std::string& paramGroup)
    {
        k = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
    }

    //! set a tensorial permeability
    template<int d = dimWorld, std::enable_if_t<d == 2, int> = 0>
    void setPermeability_(Dune::FieldMatrix<Scalar, dimWorld, dimWorld>& K,
                          const std::string& paramGroup)
    {
        const auto kh = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        const auto ratio = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityAnisotropyRatio");
        const auto angle = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityAngle");
        const auto kv = kh/ratio;

        using std::sin;
        using std::cos;
        Scalar cost = cos(angle);
        Scalar sint = sin(angle);

        K = 0.0;
        K[0][0] = cost*cost*kh + sint*sint*kv;
        K[1][1] = sint*sint*kh + cost*cost*kv;
        K[0][1] = K[1][0] = cost*sint*(kh - kv);
    }

    //! set tensorial permeabilities
    template<int d = dimWorld, std::enable_if_t<d == 3, int> = 0>
    void setPermeability_(Dune::FieldMatrix<Scalar, dimWorld, dimWorld>& K,
                          const std::string& paramGroup)
    {
        const auto kh = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        const auto ratio = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityAnisotropyRatio");

        using std::abs;
        if (abs(ratio - 1.0) > 1e-6)
            DUNE_THROW(Dune::InvalidStateException, "3d spatial params not yet implemented for anisotropy");

        K = 0.0;
        K[0][0] = K[1][1] = K[2][2] = kh;
    }

    Scalar porosity_;
    PermeabilityType permeability_;
};

} // end namespace Dumux

#endif
