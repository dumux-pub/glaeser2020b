// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters class used for the facet domain.
 */
#ifndef DUMUX_GLAESER2020B_FACET_SPATIALPARAMS_HH
#define DUMUX_GLAESER2020B_FACET_SPATIALPARAMS_HH

#include <dumux/io/grid/griddata.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters class used for the facet domain.
 */
template< class GridGeometry, class Scalar >
class FacetSpatialParams
: public FVSpatialParamsOneP< GridGeometry, Scalar, FacetSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = FacetSpatialParams< GridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< GridGeometry, Scalar, ThisType >;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using GridData = Dumux::GridData<typename GridView::Grid>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    FacetSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                       std::shared_ptr<const GridData> gridDataPtr,
                       const std::string& paramGroup)
    : ParentType(gridGeometry)
    , gridDataPtr_(gridDataPtr)
    {
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    // Return the domain marker of a fracture
    int getDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

private:
    std::shared_ptr<const GridData> gridDataPtr_;
    PermeabilityType permeability_;
    Scalar porosity_;
};

} // end namespace Dumux

#endif
