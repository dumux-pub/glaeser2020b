// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the reference model.
 */
#ifndef DUMUX_GLAESER_REFERENCE_PROPERTIES_HH
#define DUMUX_GLAESER_REFERENCE_PROPERTIES_HH

#include <dune/alugrid/grid.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/ccmpfa.hh>
#include <examples/common/spatialparams_reference.hh>

namespace Dumux::Properties {

namespace TTag {

// create the type tag nodes
struct OnePReferenceMpfa { using InheritsFrom = std::tuple<OneP, CCMpfaModel>; };

} // end namespace TTag

template<class TypeTag>
struct Grid<TypeTag, TTag::OnePReferenceMpfa>
{ using type = Dune::ALUGrid<DIMWORLD, DIMWORLD, CELLTYPE, Dune::nonconforming>; };

// we expect the problem class to have been included before
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePReferenceMpfa>
{ using type = OnePReferenceProblem<TypeTag>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePReferenceMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using PermType = Dune::FieldMatrix<Scalar,
                                       GridView::dimensionworld,
                                       GridView::dimensionworld>;

public:
    using type = ReferenceSpatialParams< GridGeometry, Scalar, PermType >;
};

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePReferenceMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// use a custom grid geometry with larger maximum stencil
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::OnePReferenceMpfa>
{
private:
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using PrimaryIV = GetPropType<TypeTag, Properties::PrimaryInteractionVolume>;
    using SecondaryIV = GetPropType<TypeTag, Properties::SecondaryInteractionVolume>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;

    struct MyTraits : public CCMpfaFVGridGeometryTraits<GridView, NodalIndexSet, PrimaryIV, SecondaryIV>
    {
        static constexpr int maxElementStencilSize = 40;
    };
public:
    using type = CCMpfaFVGridGeometry<GridView, MyTraits, getPropValue<TypeTag, Properties::EnableGridGeometryCache>()>;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePReferenceMpfa>
{ static constexpr bool value = false; };

// use caches
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::OnePReferenceMpfa>
{ static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::OnePReferenceMpfa>
{ static constexpr bool value = true; };


} // end namespace Dumux::Properties

#endif
