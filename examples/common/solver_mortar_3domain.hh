// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver for the three-domain box facet coupling model
 *        with mortars at the bulk-facet interfaces.
 */
#ifndef DUMUX_GLAESER2020B_THREEDOMAIN_MORTAR_SOLVER_HH
#define DUMUX_GLAESER2020B_THREEDOMAIN_MORTAR_SOLVER_HH

#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>

#include <dumux/io/grid/gridmanager.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>
#include <dumux/multidomain/facet/box/mortar/mortargridcreator.hh>
#include <dumux/multidomain/facet/box/mortar/couplingmanager.hh>
#include <dumux/multidomain/facet/box/mortar/darcyslaw.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include "properties_mortar.hh"

namespace Dumux {

// type tags for the flow problem using box (must be in CMakeLists.txt)
using BulkBoxTypeTag = Dumux::Properties::TTag::BULKBOXTYPETAG;
using FacetBoxTypeTag = Dumux::Properties::TTag::FACETBOXTYPETAG;
using FacetIntersectionTypeTag = Dumux::Properties::TTag::INTERSECTIONBOXTYPETAG;

namespace Properties {

// define new type tags with modified advection types and coupling managers
namespace TTag {
struct BoxBulkMortar { using InheritsFrom = std::tuple<BulkBoxTypeTag>; };
struct BoxFacetMortar { using InheritsFrom = std::tuple<FacetBoxTypeTag>; };
struct BoxIntersectionMortar { using InheritsFrom = std::tuple<FacetIntersectionTypeTag>; };
} // end namespace TTag

// The bulk model should use a modified version of Darcy's law, using the mortar flux at interfaces
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::BoxBulkMortar>
{
    using type = BoxMortarFacetCouplingDarcysLaw< GetPropType<TypeTag, Properties::Scalar>,
                                                  GetPropType<TypeTag, Properties::GridGeometry> >;
};

// The facet model should use a modified version of Darcy's law, using the mortar flux at interfaces
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::BoxFacetMortar>
{
    using type = BoxMortarFacetCouplingDarcysLaw< GetPropType<TypeTag, Properties::Scalar>,
                                                  GetPropType<TypeTag, Properties::GridGeometry> >;
};

} // end namespace Properties

// The type tags
using MortarFacetTypeTag = Properties::TTag::BulkFacetMortar;
using MortarIntersectionTypeTag = Properties::TTag::FacetIntersectionMortar;
using BulkTypeTag = Properties::TTag::BoxBulkMortar;
using FacetTypeTag = Properties::TTag::BoxFacetMortar;
using IntersectionTypeTag = Properties::TTag::BoxIntersectionMortar;

// define some convenience aliasess to be used below in the property definitions and in main
struct MortarSolverTestTraits
{
    using BulkGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGridGeometry = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using IntersectionGridGeometry = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag, MortarFacetTypeTag, MortarIntersectionTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingThreeDomainMapper<BulkGridGeometry, FacetGridGeometry, IntersectionGridGeometry>;
    using CouplingManager = Dumux::BoxMortarFacetCouplingThreeDomainManager<MDTraits, CouplingMapper>;
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
};

// set coupling manager property
namespace Properties {
template<class TypeTag>
struct CouplingManager<TypeTag, BulkTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, IntersectionTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, MortarFacetTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, MortarIntersectionTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };

} // end namespace Properties

// return type of solveMortar(), containing the
// solutions and the grid views on which they were computed
struct MortarSolveResult
{
private:
    using Assembler = typename MortarSolverTestTraits::Assembler;
    using CouplingManager = typename MortarSolverTestTraits::CouplingManager;

    using BulkGrid = Dumux::GetPropType<BulkTypeTag,  Dumux::Properties::Grid>;
    using BulkGG = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using BulkGV = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridVariables>;
    using BulkSolution = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::SolutionVector>;

    using FacetGrid = Dumux::GetPropType<FacetTypeTag,  Dumux::Properties::Grid>;
    using FacetGG = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGV = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridVariables>;
    using FacetSolution = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::SolutionVector>;

    using IntersectionGrid = Dumux::GetPropType<IntersectionTypeTag,  Dumux::Properties::Grid>;
    using IntersectionGG = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using IntersectionGV = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridVariables>;
    using IntersectionSolution = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::SolutionVector>;

    using MortarFacetGrid = Dumux::GetPropType<MortarFacetTypeTag, Dumux::Properties::Grid>;
    using MortarFacetGG = Dumux::GetPropType<MortarFacetTypeTag, Dumux::Properties::GridGeometry>;
    using MortarFacetGV = Dumux::GetPropType<MortarFacetTypeTag, Dumux::Properties::GridVariables>;
    using MortarFacetSolution = Dumux::GetPropType<MortarFacetTypeTag, Dumux::Properties::SolutionVector>;
    using MortarFacetGridCreator = FacetCouplingMortarGridCreator<MortarFacetGrid>;

    using MortarIntersectionGrid = Dumux::GetPropType<MortarIntersectionTypeTag, Dumux::Properties::Grid>;
    using MortarIntersectionGG = Dumux::GetPropType<MortarIntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using MortarIntersectionGV = Dumux::GetPropType<MortarIntersectionTypeTag, Dumux::Properties::GridVariables>;
    using MortarIntersectionSolution = Dumux::GetPropType<MortarIntersectionTypeTag, Dumux::Properties::SolutionVector>;
    using MortarIntersectionGridCreator = FacetCouplingMortarGridCreator<MortarIntersectionGrid>;

public:
    using GridManager = Dumux::FacetCouplingGridManager<BulkGrid, FacetGrid, IntersectionGrid>;
    GridManager gridManager;
    MortarFacetGridCreator mortarFacetGridCreator;
    MortarIntersectionGridCreator mortarIntersectionGridCreator;

    std::shared_ptr<CouplingManager> couplingManager;
    std::shared_ptr<Assembler> assembler;

    std::shared_ptr<BulkGG> bulkGridGeometry;
    std::shared_ptr<FacetGG> facetGridGeometry;
    std::shared_ptr<IntersectionGG> intersectionGridGeometry;
    std::shared_ptr<MortarFacetGG> mortarFacetGridGeometry;
    std::shared_ptr<MortarIntersectionGG> mortarIntersectionGridGeometry;

    std::shared_ptr<BulkGV> bulkGridVariables;
    std::shared_ptr<FacetGV> facetGridVariables;
    std::shared_ptr<IntersectionGV> intersectionGridVariables;
    std::shared_ptr<MortarFacetGV> mortarFacetGridVariables;
    std::shared_ptr<MortarIntersectionGV> mortarIntersectionGridVariables;

    BulkSolution bulkSolution;
    FacetSolution facetSolution;
    IntersectionSolution intersectionSolution;
    MortarFacetSolution mortarFacetSolution;
    MortarIntersectionSolution mortarIntersectionSolution;
};

// run the simulation for the type tags defined in CMakeLists.txt (see above)
template<class LinearSolver=UMFPackBackend>
MortarSolveResult solveMortar(const Dune::ParameterTree& paramTree)
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////

    MortarSolveResult result;
    auto& gridManager = result.gridManager;
    gridManager.init(paramTree.template get<std::string>("Grid.ParamGroup", "Grid"));
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();
    const auto& intersectionGridView = gridManager.template grid<2>().leafGridView();

    // create the finite volume grid geometries
    using BulkGridGeometry = typename MortarSolverTestTraits::BulkGridGeometry;
    using FacetGridGeometry = typename MortarSolverTestTraits::FacetGridGeometry;
    using IntersectionGridGeometry = typename MortarSolverTestTraits::IntersectionGridGeometry;

    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    auto facetGridGeometry = std::make_shared<FacetGridGeometry>(facetGridView);
    auto intersectionGridGeometry = std::make_shared<IntersectionGridGeometry>(intersectionGridView);

    using Embeddings = typename MortarSolveResult::GridManager::Embeddings;
    bulkGridGeometry->update(facetGridView, CodimOneGridAdapter<Embeddings>(gridManager.getEmbeddings()));
    facetGridGeometry->update(intersectionGridView, CodimOneGridAdapter<Embeddings, 1, 2>(gridManager.getEmbeddings()));
    intersectionGridGeometry->update();

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename MortarSolverTestTraits::CouplingMapper>();
    couplingMapper->update(*bulkGridGeometry, *facetGridGeometry, *intersectionGridGeometry, gridManager.getEmbeddings());

    // make the facet mortar grid geometry
    using MortarFacetGrid = GetPropType<MortarFacetTypeTag, Properties::Grid>;
    using MortarIntersectionGrid = GetPropType<MortarIntersectionTypeTag, Properties::Grid>;
    Dumux::GridManager<MortarFacetGrid> rawMortarFacetGridManager;
    Dumux::GridManager<MortarIntersectionGrid> rawMortarIntersectionGridManager;
    rawMortarFacetGridManager.init(paramTree.template get<std::string>("FacetMortar.Grid.ParamGroup", "FacetMortar"));
    rawMortarIntersectionGridManager.init(paramTree.template get<std::string>("IntersectionMortar.Grid.ParamGroup", "IntersectionMortar"));
    rawMortarFacetGridManager.loadBalance();
    rawMortarIntersectionGridManager.loadBalance();
    const auto& rawMortarFacetGridView = rawMortarFacetGridManager.grid().leafGridView();
    const auto& rawMortarIntersectionGridView = rawMortarIntersectionGridManager.grid().leafGridView();

    using MortarFacetGridGeometry = GetPropType<MortarFacetTypeTag, Properties::GridGeometry>;
    using MortarIntersectionGridGeometry = GetPropType<MortarIntersectionTypeTag, Properties::GridGeometry>;
    using MortarFacetFEBasis = typename MortarFacetGridGeometry::FEBasis;
    using MortarIntersectionFEBasis = typename MortarIntersectionGridGeometry::FEBasis;
    auto rawMortarFacetFeBasis = std::make_shared<MortarFacetFEBasis>(rawMortarFacetGridView);
    auto rawMortarIntersectionFeBasis = std::make_shared<MortarIntersectionFEBasis>(rawMortarIntersectionGridView);
    auto rawMortarFacetGridGeometry = std::make_shared<MortarFacetGridGeometry>(rawMortarFacetFeBasis);
    auto rawMortarIntersectionGridGeometry = std::make_shared<MortarIntersectionGridGeometry>(rawMortarIntersectionFeBasis);
    rawMortarFacetGridGeometry->update();
    rawMortarIntersectionGridGeometry->update();

    auto& mortarFacetGridCreator = result.mortarFacetGridCreator;
    auto& mortarIntersectionGridCreator = result.mortarIntersectionGridCreator;
    mortarFacetGridCreator.create(*bulkGridGeometry, *rawMortarFacetGridGeometry);
    mortarIntersectionGridCreator.create(*facetGridGeometry, *rawMortarIntersectionGridGeometry);

    const auto& mortarFacetGridView = mortarFacetGridCreator.grid().leafGridView();
    const auto& mortarIntersectionGridView = mortarIntersectionGridCreator.grid().leafGridView();
    auto facetFeBasis = std::make_shared<MortarFacetFEBasis>(mortarFacetGridView);
    auto intersectionFeBasis = std::make_shared<MortarIntersectionFEBasis>(mortarIntersectionGridView);
    auto mortarFacetGridGeometry = std::make_shared<MortarFacetGridGeometry>(facetFeBasis);
    auto mortarIntersectionGridGeometry = std::make_shared<MortarIntersectionGridGeometry>(intersectionFeBasis);
    mortarFacetGridGeometry->update();
    mortarIntersectionGridGeometry->update();

    const auto mortarFacetOverlapIndices = mortarFacetGridCreator.getMortarOverlapIndices(*mortarFacetGridGeometry);
    const auto mortarIntersectionOverlapIndices = mortarIntersectionGridCreator.getMortarOverlapIndices(*mortarIntersectionGridGeometry);

    // the coupling manager
    using CouplingManager = typename MortarSolverTestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    using IntersectionProblem = GetPropType<IntersectionTypeTag, Properties::Problem>;
    using MortarFacetProblem = GetPropType<MortarFacetTypeTag, Properties::Problem>;
    using MortarIntersectionProblem = GetPropType<MortarIntersectionTypeTag, Properties::Problem>;

    const auto bulkParamGroup = paramTree.template get<std::string>("Bulk.ParamGroup", "Bulk");
    const auto facetParamGroup = paramTree.template get<std::string>("Facet.ParamGroup", "Facet");
    const auto intersectionParamGroup = paramTree.template get<std::string>("Intersection.ParamGroup", "Intersection");
    const auto mortarFacetParamGroup = paramTree.template get<std::string>("FacetMortar.ParamGroup", "FacetMortar");
    const auto mortarIntersectionParamGroup = paramTree.template get<std::string>("IntersectionMortar.ParamGroup", "IntersectionMortar");
    const auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<1>();
    const auto intersectionGridData = gridManager.getGridData()->template getSubDomainGridData<2>();

    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkGridGeometry, bulkParamGroup);
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetGridGeometry, facetGridData, facetParamGroup);
    auto intersectionSpatialParams = std::make_shared<typename IntersectionProblem::SpatialParams>(intersectionGridGeometry, intersectionGridData, intersectionParamGroup);

    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, bulkSpatialParams, couplingManager, bulkParamGroup);
    auto facetProblem = std::make_shared<FacetProblem>(facetGridGeometry, facetSpatialParams, couplingManager, facetParamGroup);
    auto intersectionProblem = std::make_shared<IntersectionProblem>(intersectionGridGeometry, intersectionSpatialParams, couplingManager, intersectionParamGroup);
    auto mortarFacetProblem = std::make_shared<MortarFacetProblem>(mortarFacetGridGeometry, couplingManager, mortarFacetParamGroup);
    auto mortarIntersectionProblem = std::make_shared<MortarIntersectionProblem>(mortarIntersectionGridGeometry, couplingManager, mortarIntersectionParamGroup);

    // the solution vector
    using MDTraits = typename MortarSolverTestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    static const auto intersectionId = typename MDTraits::template SubDomain<2>::Index();
    static const auto mortarFacetId = typename MDTraits::template SubDomain<3>::Index();
    static const auto mortarIntersectionId = typename MDTraits::template SubDomain<4>::Index();
    x[bulkId].resize(bulkGridGeometry->numDofs());                             x[bulkId] = 0.0;
    x[facetId].resize(facetGridGeometry->numDofs());                           x[facetId] = 0.0;
    x[intersectionId].resize(intersectionGridGeometry->numDofs());             x[intersectionId] = 0.0;
    x[mortarFacetId].resize(mortarFacetGridGeometry->numDofs());               x[mortarFacetId] = 0.0;
    x[mortarIntersectionId].resize(mortarIntersectionGridGeometry->numDofs()); x[mortarIntersectionId] = 0.0;

    // initialize coupling manager
    couplingManager->init(bulkProblem, facetProblem, intersectionProblem, mortarFacetProblem, mortarIntersectionProblem,
                          couplingMapper, mortarFacetOverlapIndices, mortarIntersectionOverlapIndices, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    using IntersectionGridVariables = GetPropType<IntersectionTypeTag, Properties::GridVariables>;
    using MortarFacetGridVariables = GetPropType<MortarFacetTypeTag, Properties::GridVariables>;
    using MortarIntersectionGridVariables = GetPropType<MortarIntersectionTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetGridGeometry);
    auto intersectionGridVariables = std::make_shared<IntersectionGridVariables>(intersectionProblem, intersectionGridGeometry);
    auto mortarFacetGridVariables = std::make_shared<MortarFacetGridVariables>(mortarFacetProblem);
    auto mortarIntersectionGridVariables = std::make_shared<MortarIntersectionGridVariables>(mortarIntersectionProblem);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);
    intersectionGridVariables->init(x[intersectionId]);
    mortarFacetGridVariables->init(x[mortarFacetId]);
    mortarIntersectionGridVariables->init(x[mortarIntersectionId]);

    // intialize the vtk output modules
    using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkTypeTag, Properties::SolutionVector>>;
    using FacetVtkWriter = VtkOutputModule<FacetGridVariables, GetPropType<FacetTypeTag, Properties::SolutionVector>>;
    using IntersectionVtkWriter = VtkOutputModule<IntersectionGridVariables, GetPropType<IntersectionTypeTag, Properties::SolutionVector>>;
    BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], paramTree["Bulk.IO.VtkName"], bulkParamGroup, Dune::VTK::nonconforming);
    FacetVtkWriter facetVtkWriter(*facetGridVariables, x[facetId], paramTree["Facet.IO.VtkName"], facetParamGroup, Dune::VTK::nonconforming);
    IntersectionVtkWriter intersectionVtkWriter(*intersectionGridVariables, x[intersectionId], paramTree["Intersection.IO.VtkName"], intersectionParamGroup);
    Dune::VTKWriter<typename MortarFacetGridGeometry::GridView> mortarFacetVtkWriter(mortarFacetGridView);
    Dune::VTKWriter<typename MortarIntersectionGridGeometry::GridView> mortarIntersectionVtkWriter(mortarIntersectionGridView);

    auto vtkMortarFacetVariable = x[mortarFacetId];
    auto vtkMortarIntersectionVariable = x[mortarIntersectionId];
    vtkMortarFacetVariable /= getParam<double>("FacetMortar.Problem.ScaleFactor", 1.0);
    vtkMortarIntersectionVariable /= getParam<double>("IntersectionMortar.Problem.ScaleFactor", 1.0);

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkTypeTag, Properties::IOFields>;
    using FacetIOFields = GetPropType<FacetTypeTag, Properties::IOFields>;
    using IntersectionIOFields = GetPropType<IntersectionTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    FacetIOFields::initOutputModule(facetVtkWriter);
    IntersectionIOFields::initOutputModule(intersectionVtkWriter);
    bulkProblem->addOutputFields(bulkVtkWriter);
    facetProblem->addOutputFields(facetVtkWriter);
    intersectionProblem->addOutputFields(intersectionVtkWriter);
    mortarFacetVtkWriter.addCellData(vtkMortarFacetVariable, "mortar");
    mortarIntersectionVtkWriter.addCellData(vtkMortarIntersectionVariable, "mortar");

    // write initial solution
    bulkVtkWriter.write(0.0);
    facetVtkWriter.write(0.0);
    intersectionVtkWriter.write(0.0);
    mortarFacetVtkWriter.write(paramTree["FacetMortar.IO.VtkName"]);
    mortarIntersectionVtkWriter.write(paramTree["IntersectionMortar.IO.VtkName"]);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem, intersectionProblem, mortarFacetProblem, mortarIntersectionProblem),
                                                  std::make_tuple(bulkGridGeometry, facetGridGeometry, intersectionGridGeometry, mortarFacetGridGeometry, mortarIntersectionGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables, intersectionGridVariables, mortarFacetGridVariables, mortarIntersectionGridVariables),
                                                  couplingManager);

    // linearize & solve
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto linearSolver = std::make_shared<LinearSolver>();
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    newtonSolver->solve(x);

    if (getParam<bool>("IO.WriteMatrix", false))
    {
        using Converter = MatrixConverter<typename Assembler::JacobianMatrix>;
        auto M = Converter::multiTypeToBCRSMatrix(assembler->jacobian());
        std::ofstream matrixFile(paramTree["Bulk.IO.VtkName"] + "_matrix.mtx", std::ios::out);
        Dune::writeMatrixMarket(M, matrixFile);
    }

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    facetGridVariables->update(x[facetId]);
    intersectionGridVariables->update(x[intersectionId]);
    mortarFacetGridVariables->update(x[mortarFacetId]);
    mortarIntersectionGridVariables->update(x[mortarIntersectionId]);

    vtkMortarFacetVariable = x[mortarFacetId];
    vtkMortarIntersectionVariable = x[mortarIntersectionId];
    vtkMortarFacetVariable /= getParam<double>("FacetMortar.Problem.ScaleFactor", 1.0);
    vtkMortarIntersectionVariable /= getParam<double>("FacetMortar.Problem.ScaleFactor", 1.0);

    // lambdas for coupling context update
    auto bindBulkContext = [&] (const auto& element) { couplingManager->bindCouplingContext(bulkId, element, *assembler); };
    auto bindFacetContext = [&] (const auto& element) { couplingManager->bindCouplingContext(facetId, element, *assembler); };
    auto bindIntersectionContext = [&] (const auto& element) { couplingManager->bindCouplingContext(intersectionId, element, *assembler); };
    auto bindMortarFacetContext = [&] (const auto& element) { couplingManager->bindCouplingContext(mortarFacetId, element, *assembler); };
    auto bindMortarIntersectionContext = [&] (const auto& element) { couplingManager->bindCouplingContext(mortarIntersectionId, element, *assembler); };

    // update problem-dependent output fields
    bulkProblem->updateOutputFields(bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->updateOutputFields(bindFacetContext, *facetGridVariables, x[facetId]);
    intersectionProblem->updateOutputFields(bindIntersectionContext, *intersectionGridVariables, x[intersectionId]);
    mortarFacetProblem->updateOutputFields(bindMortarFacetContext, *mortarFacetGridVariables, x[mortarFacetId]);
    mortarIntersectionProblem->updateOutputFields(bindMortarIntersectionContext, *mortarIntersectionGridVariables, x[mortarIntersectionId]);

    // write vtk output
    bulkVtkWriter.write(1.0);
    facetVtkWriter.write(1.0);
    intersectionVtkWriter.write(1.0);
    mortarFacetVtkWriter.write(paramTree["FacetMortar.IO.VtkName"]);
    mortarIntersectionVtkWriter.write(paramTree["IntersectionMortar.IO.VtkName"]);

    // write csv data
    bulkProblem->writeCsvData(paramTree["Bulk.IO.CsvFileNameBody"], bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->writeCsvData(paramTree["Facet.IO.CsvFileNameBody"], bindFacetContext, *facetGridVariables, x[facetId]);
    intersectionProblem->writeCsvData(paramTree["Intersection.IO.CsvFileNameBody"], bindIntersectionContext, *intersectionGridVariables, x[intersectionId]);
    mortarFacetProblem->writeCsvData(paramTree["FacetMortar.IO.CsvFileNameBody"], bindMortarFacetContext, *mortarFacetGridVariables, x[mortarFacetId]);
    mortarIntersectionProblem->writeCsvData(paramTree["IntersectionMortar.IO.CsvFileNameBody"], bindMortarIntersectionContext, *mortarIntersectionGridVariables, x[mortarIntersectionId]);

    result.couplingManager = couplingManager;
    result.assembler = assembler;
    result.bulkGridGeometry = bulkGridGeometry;
    result.facetGridGeometry = facetGridGeometry;
    result.intersectionGridGeometry = intersectionGridGeometry;
    result.mortarFacetGridGeometry = mortarFacetGridGeometry;
    result.mortarIntersectionGridGeometry = mortarIntersectionGridGeometry;
    result.bulkGridVariables = bulkGridVariables;
    result.facetGridVariables = facetGridVariables;
    result.intersectionGridVariables = intersectionGridVariables;
    result.mortarFacetGridVariables = mortarFacetGridVariables;
    result.mortarIntersectionGridVariables = mortarIntersectionGridVariables;
    result.bulkSolution = x[bulkId];
    result.facetSolution = x[facetId];
    result.intersectionSolution = x[intersectionId];
    result.mortarFacetSolution = x[mortarFacetId];
    result.mortarIntersectionSolution = x[mortarIntersectionId];
    return result;
}

// run the simulation using default parameters
template<class LinearSolver=UMFPackBackend>
MortarSolveResult solveMortar()
{
    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Grid";
    tree["Bulk.ParamGroup"] = "Bulk";
    tree["Facet.ParamGroup"] = "Facet";
    tree["Intersection.ParamGroup"] = "Intersection";
    tree["FacetMortar.ParamGroup"] = "FacetMortar";
    tree["FacetMortar.Grid.ParamGroup"] = "FacetMortar";
    tree["IntersectionMortar.ParamGroup"] = "IntersectionMortar";
    tree["IntersectionMortar.Grid.ParamGroup"] = "IntersectionMortar";
    tree["Bulk.IO.VtkName"] = getParam<std::string>("Bulk.IO.VtkName", "bulk");
    tree["Facet.IO.VtkName"] = getParam<std::string>("Facet.IO.VtkName", "facet");
    tree["Intersection.IO.VtkName"] = getParam<std::string>("Intersection.IO.VtkName", "intersection");
    tree["FacetMortar.IO.VtkName"] = "facetmortar";
    tree["IntersectionMortar.IO.VtkName"] = "intersectionmortar";
    tree["Bulk.IO.CsvFileNameBody"] = getParam<std::string>("Bulk.IO.CsvFileNameBody", "bulk_data");
    tree["Facet.IO.CsvFileNameBody"] = getParam<std::string>("Facet.IO.CsvFileNameBody", "facets_data");
    tree["Intersection.IO.CsvFileNameBody"] = getParam<std::string>("Intersection.IO.CsvFileNameBody", "intersection_data");
    tree["FacetMortar.IO.CsvFileNameBody"] = "facetmortar_data";
    tree["IntersectionMortar.IO.CsvFileNameBody"] = "intersectionmortar_data";

    return solveMortar<LinearSolver>(tree);
}

} // end namespace Dumux

#endif
