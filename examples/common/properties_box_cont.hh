// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the single-phase simulations with the box-cont model.
 */
#ifndef DUMUX_GLAESER2020B_COMMON_BOX_CONT_PROPERTIES_HH
#define DUMUX_GLAESER2020B_COMMON_BOX_CONT_PROPERTIES_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>

#include <examples/common/spatialparams_box_cont.hh>

// We expect the problem headers to be included beforehand and assume
// the following namings: OnePBoxContProblem
// Per default, we use simplices
#ifndef CELLTYPE
#define CELLTYPE Dune::simplex
#endif

namespace Dumux {
namespace Properties {
namespace TTag {
// we need to derive first from twop and then from the box-dfm Model
// because the flux variables cache type of OneP is overwritten in BoxDfmModel
struct OnePBoxCont { using InheritsFrom = std::tuple<BoxDfmModel, OneP>; };
}

template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBoxCont>
{ using type = Dune::ALUGrid<DIMWORLD, DIMWORLD, CELLTYPE, Dune::nonconforming>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBoxCont>
{ using type = OnePBoxContProblem<TypeTag>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBoxCont>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using PermType = Dune::FieldMatrix<Scalar,
                                       GridView::dimensionworld,
                                       GridView::dimensionworld>;

public:
    using type = BoxContSpatialParams< GridGeometry, Scalar, PermType >;
};

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBoxCont>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePBoxCont>
{ static constexpr bool value = false; };

} // end namespace Properties
} // end namespace Dumux

#endif
