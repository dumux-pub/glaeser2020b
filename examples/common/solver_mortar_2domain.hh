// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver for the two-domain box facet coupling model
 *        with mortars at the bulk-facet interfaces.
 */
#ifndef DUMUX_GLAESER2020B_TWODOMAIN_MORTAR_SOLVER_HH
#define DUMUX_GLAESER2020B_TWODOMAIN_MORTAR_SOLVER_HH

#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>

#include <dumux/io/grid/gridmanager.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>
#include <dumux/multidomain/facet/box/mortar/mortargridcreator.hh>
#include <dumux/multidomain/facet/box/mortar/couplingmanager.hh>
#include <dumux/multidomain/facet/box/mortar/darcyslaw.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include "properties_mortar.hh"

namespace Dumux {

// type tags for the flow problem using box (must be in CMakeLists.txt)
using BulkBoxTypeTag = Dumux::Properties::TTag::BULKBOXTYPETAG;
using FacetBoxTypeTag = Dumux::Properties::TTag::FACETBOXTYPETAG;

namespace Properties {

// define new type tags with modified advection types and coupling managers
namespace TTag {
struct BoxBulkMortar { using InheritsFrom = std::tuple<BulkBoxTypeTag>; };
struct BoxFacetMortar { using InheritsFrom = std::tuple<FacetBoxTypeTag>; };
} // end namespace TTag

// The bulk model should use a modified version of Darcy's law, using the mortar flux at interfaces
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::BoxBulkMortar>
{
    using type = BoxMortarFacetCouplingDarcysLaw< GetPropType<TypeTag, Properties::Scalar>,
                                                  GetPropType<TypeTag, Properties::GridGeometry> >;
};
} // end namespace Properties

// The type tags
using MortarTypeTag = Properties::TTag::BulkFacetMortar;
using BulkTypeTag = Properties::TTag::BoxBulkMortar;
using FacetTypeTag = Properties::TTag::BoxFacetMortar;

// define some convenience aliasess to be used below in the property definitions and in main
struct MortarSolverTestTraits
{
    using BulkGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGridGeometry = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag, MortarTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkGridGeometry, FacetGridGeometry>;
    using CouplingManager = Dumux::BoxMortarFacetCouplingManager<MDTraits, CouplingMapper>;
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
};

// set coupling manager property
namespace Properties {
template<class TypeTag>
struct CouplingManager<TypeTag, BulkTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, MortarTypeTag> { using type = typename MortarSolverTestTraits::CouplingManager; };

} // end namespace Properties

// return type of solveMortar(), containing the
// solutions and the grid views on which they were computed
struct MortarSolveResult
{
private:
    using Assembler = typename MortarSolverTestTraits::Assembler;
    using CouplingManager = typename MortarSolverTestTraits::CouplingManager;

    using BulkGrid = Dumux::GetPropType<BulkTypeTag,  Dumux::Properties::Grid>;
    using BulkGG = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using BulkGV = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridVariables>;
    using BulkSolution = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::SolutionVector>;

    using FacetGrid = Dumux::GetPropType<FacetTypeTag,  Dumux::Properties::Grid>;
    using FacetGG = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGV = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridVariables>;
    using FacetSolution = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::SolutionVector>;

    using MortarGrid = Dumux::GetPropType<MortarTypeTag,  Dumux::Properties::Grid>;
    using MortarGG = Dumux::GetPropType<MortarTypeTag, Dumux::Properties::GridGeometry>;
    using MortarGV = Dumux::GetPropType<MortarTypeTag, Dumux::Properties::GridVariables>;
    using MortarSolution = Dumux::GetPropType<MortarTypeTag, Dumux::Properties::SolutionVector>;
    using MortarGridCreator = FacetCouplingMortarGridCreator<MortarGrid>;

public:
    using GridManager = Dumux::FacetCouplingGridManager<BulkGrid, FacetGrid>;
    GridManager gridManager;
    MortarGridCreator mortarGridCreator;

    std::shared_ptr<CouplingManager> couplingManager;
    std::shared_ptr<Assembler> assembler;

    std::shared_ptr<BulkGG> bulkGridGeometry;
    std::shared_ptr<FacetGG> facetGridGeometry;
    std::shared_ptr<MortarGG> mortarGridGeometry;

    std::shared_ptr<BulkGV> bulkGridVariables;
    std::shared_ptr<FacetGV> facetGridVariables;
    std::shared_ptr<MortarGV> mortarGridVariables;

    BulkSolution bulkSolution;
    FacetSolution facetSolution;
    MortarSolution mortarSolution;
};

// run the simulation for the type tags defined in CMakeLists.txt (see above)
template<class LinearSolver=UMFPackBackend>
MortarSolveResult solveMortar(const Dune::ParameterTree& paramTree)
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////

    MortarSolveResult result;
    auto& gridManager = result.gridManager;
    gridManager.init(paramTree.template get<std::string>("Grid.ParamGroup", "Grid"));
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkGridGeometry = typename MortarSolverTestTraits::BulkGridGeometry;
    using FacetGridGeometry = typename MortarSolverTestTraits::FacetGridGeometry;

    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    auto facetGridGeometry = std::make_shared<FacetGridGeometry>(facetGridView);

    using Embeddings = typename MortarSolveResult::GridManager::Embeddings;
    bulkGridGeometry->update(facetGridView, CodimOneGridAdapter<Embeddings>(gridManager.getEmbeddings()));
    facetGridGeometry->update();

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename MortarSolverTestTraits::CouplingMapper>();
    couplingMapper->update(*bulkGridGeometry, *facetGridGeometry, gridManager.getEmbeddings());

    // make the mortar grid geometry
    using MortarGrid = GetPropType<MortarTypeTag, Properties::Grid>;
    Dumux::GridManager<MortarGrid> rawMortarGridManager;
    rawMortarGridManager.init(paramTree.template get<std::string>("Mortar.Grid.ParamGroup", "Mortar"));
    rawMortarGridManager.loadBalance();
    const auto& rawMortarGridView = rawMortarGridManager.grid().leafGridView();

    using MortarGridGeometry =  GetPropType<MortarTypeTag, Properties::GridGeometry>;
    using FEBasis = typename MortarGridGeometry::FEBasis;
    auto rawFeBasis = std::make_shared<FEBasis>(rawMortarGridView);
    auto rawMortarGridGeometry = std::make_shared<MortarGridGeometry>(rawFeBasis);
    rawMortarGridGeometry->update();

    auto& mortarGridCreator = result.mortarGridCreator;
    mortarGridCreator.create(*bulkGridGeometry, *rawMortarGridGeometry);

    const auto& mortarGridView = mortarGridCreator.grid().leafGridView();
    auto feBasis = std::make_shared<FEBasis>(mortarGridView);
    auto mortarGridGeometry = std::make_shared<MortarGridGeometry>(feBasis);
    mortarGridGeometry->update();

    // the coupling manager
    using CouplingManager = typename MortarSolverTestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    using MortarProblem = GetPropType<MortarTypeTag, Properties::Problem>;

    const auto bulkParamGroup = paramTree.template get<std::string>("Bulk.ParamGroup", "Bulk");
    const auto facetParamGroup = paramTree.template get<std::string>("Facet.ParamGroup", "Facet");
    const auto mortarParamGroup = paramTree.template get<std::string>("Mortar.ParamGroup", "Mortar");
    const auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<1>();

    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkGridGeometry, bulkParamGroup);
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetGridGeometry, facetGridData, facetParamGroup);

    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, bulkSpatialParams, couplingManager, bulkParamGroup);
    auto facetProblem = std::make_shared<FacetProblem>(facetGridGeometry, facetSpatialParams, couplingManager, facetParamGroup);
    auto mortarProblem = std::make_shared<MortarProblem>(mortarGridGeometry, couplingManager, mortarParamGroup);

    // the solution vector
    using MDTraits = typename MortarSolverTestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    static const auto mortarId = typename MDTraits::template SubDomain<2>::Index();
    x[bulkId].resize(bulkGridGeometry->numDofs());     x[bulkId] = 0.0;
    x[facetId].resize(facetGridGeometry->numDofs());   x[facetId] = 0.0;
    x[mortarId].resize(mortarGridGeometry->numDofs()); x[mortarId] = 0.0;

    // initialize coupling manager
    const auto overlapIndices = mortarGridCreator.getMortarOverlapIndices(*mortarGridGeometry);
    couplingManager->init(bulkProblem, facetProblem, mortarProblem, couplingMapper, overlapIndices, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    using MortarGridVariables = GetPropType<MortarTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetGridGeometry);
    auto mortarGridVariables = std::make_shared<MortarGridVariables>(mortarProblem);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);
    mortarGridVariables->init(x[mortarId]);

    // intialize the vtk output modules
    using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkTypeTag, Properties::SolutionVector>>;
    using FacetVtkWriter = VtkOutputModule<FacetGridVariables, GetPropType<FacetTypeTag, Properties::SolutionVector>>;
    const auto bulkDM = BulkGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], paramTree["Bulk.IO.VtkName"], bulkParamGroup, bulkDM);
    FacetVtkWriter facetVtkWriter(*facetGridVariables, x[facetId], paramTree["Facet.IO.VtkName"], facetParamGroup);
    Dune::VTKWriter<typename MortarGridGeometry::GridView> mortarVtkWriter(mortarGridView);

    auto vtkMortarVariable = x[mortarId];
    vtkMortarVariable /= getParam<double>("Mortar.Problem.ScaleFactor");

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkTypeTag, Properties::IOFields>;
    using FacetIOFields = GetPropType<FacetTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    FacetIOFields::initOutputModule(facetVtkWriter);
    bulkProblem->addOutputFields(bulkVtkWriter);
    facetProblem->addOutputFields(facetVtkWriter);
    mortarVtkWriter.addCellData(vtkMortarVariable, "mortar");

    // write initial solution
    bulkVtkWriter.write(0.0);
    facetVtkWriter.write(0.0);
    mortarVtkWriter.write(paramTree["Mortar.IO.VtkName"]);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem, mortarProblem),
                                                  std::make_tuple(bulkGridGeometry, facetGridGeometry, mortarGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables, mortarGridVariables),
                                                  couplingManager);

    // linearize & solve
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto linearSolver = std::make_shared<LinearSolver>();
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    newtonSolver->solve(x);

    if (getParam<bool>("IO.WriteMatrix", false))
    {
        using Converter = MatrixConverter<typename Assembler::JacobianMatrix>;
        auto M = Converter::multiTypeToBCRSMatrix(assembler->jacobian());
        std::ofstream matrixFile(paramTree["Bulk.IO.VtkName"] + "_matrix.mtx", std::ios::out);
        Dune::writeMatrixMarket(M, matrixFile);
    }

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    facetGridVariables->update(x[facetId]);
    mortarGridVariables->update(x[mortarId]);

    vtkMortarVariable = x[mortarId];
    vtkMortarVariable /= getParam<double>("Mortar.Problem.ScaleFactor");

    // lambdas for coupling context update
    auto bindBulkContext = [&] (const auto& element) { couplingManager->bindCouplingContext(bulkId, element, *assembler); };
    auto bindFacetContext = [&] (const auto& element) { couplingManager->bindCouplingContext(facetId, element, *assembler); };
    auto bindMortarContext = [&] (const auto& element) { couplingManager->bindCouplingContext(mortarId, element, *assembler); };

    // update problem-dependent output fields
    bulkProblem->updateOutputFields(bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->updateOutputFields(bindFacetContext, *facetGridVariables, x[facetId]);
    mortarProblem->updateOutputFields(bindMortarContext, *mortarGridVariables, x[mortarId]);

    // write vtk output
    bulkVtkWriter.write(1.0);
    facetVtkWriter.write(1.0);
    mortarVtkWriter.write(paramTree["Mortar.IO.VtkName"]);

    // write csv data
    bulkProblem->writeCsvData(paramTree["Bulk.IO.CsvFileNameBody"], bindBulkContext, *bulkGridVariables, x[bulkId]);
    facetProblem->writeCsvData(paramTree["Facet.IO.CsvFileNameBody"], bindFacetContext, *facetGridVariables, x[facetId]);
    mortarProblem->writeCsvData(paramTree["Mortar.IO.CsvFileNameBody"], bindMortarContext, *mortarGridVariables, x[mortarId]);

    result.couplingManager = couplingManager;
    result.assembler = assembler;
    result.bulkGridGeometry = bulkGridGeometry;
    result.facetGridGeometry = facetGridGeometry;
    result.mortarGridGeometry = mortarGridGeometry;
    result.bulkGridVariables = bulkGridVariables;
    result.facetGridVariables = facetGridVariables;
    result.mortarGridVariables = mortarGridVariables;
    result.bulkSolution = x[bulkId];
    result.facetSolution = x[facetId];
    result.mortarSolution = x[mortarId];
    return result;
}

// run the simulation using default parameters
template<class LinearSolver=UMFPackBackend>
MortarSolveResult solveMortar()
{
    Dune::ParameterTree tree;
    tree["Grid.ParamGroup"] = "Grid";
    tree["Bulk.ParamGroup"] = "Bulk";
    tree["Facet.ParamGroup"] = "Facet";
    tree["Mortar.ParamGroup"] = "Mortar";
    tree["Mortar.Grid.ParamGroup"] = "Mortar";
    tree["Bulk.IO.VtkName"] = getParam<std::string>("Bulk.IO.VtkName", "bulk");
    tree["Facet.IO.VtkName"] = getParam<std::string>("Facet.IO.VtkName", "facet");
    tree["Mortar.IO.VtkName"] = "mortar";
    tree["Bulk.IO.CsvFileNameBody"] = getParam<std::string>("Bulk.IO.CsvFileNameBody", "bulk_data");
    tree["Facet.IO.CsvFileNameBody"] = getParam<std::string>("Facet.IO.CsvFileNameBody", "facets_data");
    tree["Mortar.IO.CsvFileNameBody"] = "mortar_data";

    return solveMortar<LinearSolver>(tree);
}

} // end namespace Dumux

#endif
