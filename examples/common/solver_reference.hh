// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver for the reference model.
 */
#ifndef DUMUX_GLAESER2020B_REFERENCE_SOLVER_HH
#define DUMUX_GLAESER2020B_REFERENCE_SOLVER_HH

#include <iostream>

#include <dune/common/parametertree.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/discretization/method.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>

namespace Dumux {

// The type tags used here (must be defined in CMakeLists.txt)
using ReferenceTypeTag = Dumux::Properties::TTag::REFERENCETYPETAG;

// return type of solveMortar(), containing the
// solutions and the grid views on which they were computed
struct ReferenceSolveResult
{
public:
    using Grid = Dumux::GetPropType<ReferenceTypeTag,  Dumux::Properties::Grid>;
    using GridGeometry = Dumux::GetPropType<ReferenceTypeTag, Dumux::Properties::GridGeometry>;
    using SolutionVector = Dumux::GetPropType<ReferenceTypeTag, Dumux::Properties::SolutionVector>;
    using Problem = Dumux::GetPropType<ReferenceTypeTag, Dumux::Properties::Problem>;
    using GridManager = Dumux::GridManager<Grid>;
    GridManager gridManager;
    std::shared_ptr<GridGeometry> gridGeometry;
    std::shared_ptr<Problem> problem;
    SolutionVector solution;
};

// run the simulation for the type tags defined in CMakeLists.txt (see above)
template<class LinearSolver=UMFPackBackend>
ReferenceSolveResult solveReference(const Dune::ParameterTree& paramTree)
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////

    ReferenceSolveResult result;
    auto& gridManager = result.gridManager;
    gridManager.init(paramTree["Grid.ParamGroup"]);
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& gridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<ReferenceTypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(gridView);
    gridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<ReferenceTypeTag, Properties::Problem>;
    auto spatialParams = std::make_shared<typename Problem::SpatialParams>(gridGeometry, gridManager.getGridData());
    auto problem = std::make_shared<Problem>(gridGeometry, spatialParams);

    // set which elements are on fractures and/or intersections
    // we use domain marker thresholds to define everything
    // below these thresholds as fractures or intersections
    if constexpr (int(GridGeometry::GridView::dimensionworld) == 2)
    {
        std::vector<int> fracMarkers = getParam<std::vector<int>>("Reference.FractureMarkers");

        // lambda to determine is element is on fractures
        auto isOnFracture = [&] (const auto& element)
        {
            return std::count(fracMarkers.begin(),
                              fracMarkers.end(),
                              gridManager.getGridData()->getElementDomainMarker(element));
        };

        spatialParams->setFractureElements(isOnFracture);
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "3d");
    }

    // the solution vector
    using SolutionVector = GetPropType<ReferenceTypeTag, Properties::SolutionVector>;
    SolutionVector x; x.resize(gridGeometry->numDofs()); x = 0.0;

    // the grid variables
    using GridVariables = GetPropType<ReferenceTypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using VtkWriter = VtkOutputModule<GridVariables, SolutionVector>;
    VtkWriter vtkWriter(*gridVariables, x, paramTree["Reference.IO.VtkName"]);

    // Add model specific output fields
    using IOFields = GetPropType<ReferenceTypeTag, Properties::IOFields>;
    IOFields::initOutputModule(vtkWriter);
    problem->addOutputFields(vtkWriter);

    // add fracture/intersection markers
    std::vector<int> domainMarker(gridGeometry->gridView().size(0), -1);
    std::vector<bool> isOnFracture(gridGeometry->gridView().size(0), false);
    std::vector<bool> isOnIntersection(gridGeometry->gridView().size(0), false);
    for (const auto& element : elements(gridGeometry->gridView()))
    {
        if (problem->spatialParams().isOnFracture(element))
            isOnFracture[gridGeometry->elementMapper().index(element)] = true;
        if (problem->spatialParams().isOnIntersection(element))
            isOnIntersection[gridGeometry->elementMapper().index(element)] = true;
        domainMarker[gridGeometry->elementMapper().index(element)] = problem->spatialParams().getDomainMarker(element);
    }
    vtkWriter.addField(isOnFracture, "isOnFracture", VtkWriter::FieldType::element);
    vtkWriter.addField(isOnIntersection, "isOnIntersection", VtkWriter::FieldType::element);
    vtkWriter.addField(domainMarker, "domainMarker", VtkWriter::FieldType::element);

    // write initial solution
    vtkWriter.write(0.0);

    // the assembler
    using Assembler = FVAssembler<ReferenceTypeTag, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    auto linearSolver = std::make_shared<LinearSolver>();
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);

    // linearize & solve
    newtonSolver->solve(x);

    // update grid variables for output
    gridVariables->update(x);

    // update problem-dependent output fields
    problem->updateOutputFields(*gridVariables, x);

    // write vtk output
    vtkWriter.write(1.0);

    // write csv data
    problem->writeCsvData(paramTree["Reference.IO.CsvFileNameBody"], *gridVariables, x);

    result.gridGeometry = gridGeometry;
    result.problem = problem;
    result.solution = x;
    return result;
}

} // end namespace Dumux

#endif
