// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the bulk/facet domains.
 */
#ifndef DUMUX_GLAESER_COMMON_FACET_TWODOMAIN_PROPERTIES_HH
#define DUMUX_GLAESER_COMMON_FACET_TWODOMAIN_PROPERTIES_HH

#include <dune/alugrid/grid.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/cellcentered/mpfa/fvgridgeometry.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include <examples/common/spatialparams_bulk.hh>
#include <examples/common/spatialparams_facet.hh>

// We expect the problem headers to be included beforehand and assume
// the following namings: OnePBulkProblem, OnePFacetProblem, OnePIntersectionProblem

// Per default, we use simplices
#ifndef CELLTYPE
#define CELLTYPE Dune::simplex
#endif

// Allows setting if a two- or three-domain simulation is run
#ifndef NUMDOMAINS
#define NUMDOMAINS 2
#endif

namespace Dumux::Properties {

namespace TTag {

////////////////////////////////////
// Type Tags for the bulk problem //
////////////////////////////////////
struct OnePBulk { using InheritsFrom = std::tuple<OneP>; };
struct OnePBulkTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePBulk>; };

/////////////////////////////////////
// Type Tags for the facet problem //
/////////////////////////////////////
#if NUMDOMAINS==2
struct OnePFacet { using InheritsFrom = std::tuple<OneP>; };
struct OnePFacetTpfa { using InheritsFrom = std::tuple<OnePFacet, CCTpfaModel>; };
struct OnePFacetMpfa { using InheritsFrom = std::tuple<OnePFacet, CCTpfaModel>; };
struct OnePFacetBox { using InheritsFrom = std::tuple<OnePFacet, BoxModel>; };
#else
struct OnePFacet { using InheritsFrom = std::tuple<OneP>; };
struct OnePFacetTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePFacet>; };
struct OnePFacetMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePFacet>; };
struct OnePFacetBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePFacet>; };
#endif

////////////////////////////////////////////
// Type Tags for the intersection problem //
////////////////////////////////////////////
struct OnePIntersection { using InheritsFrom = std::tuple<OneP>; };
struct OnePIntersectionTpfa { using InheritsFrom = std::tuple<OnePIntersection, CCTpfaModel>; };
struct OnePIntersectionMpfa { using InheritsFrom = std::tuple<OnePIntersection, CCTpfaModel>; };
struct OnePIntersectionBox { using InheritsFrom = std::tuple<OnePIntersection, BoxModel>; };

} // end namespace TTag

//////////////////////////////////////
// Properties for the bulk problems //
//////////////////////////////////////
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBulk>
{ using type = Dune::ALUGrid<DIMWORLD, DIMWORLD, CELLTYPE, Dune::nonconforming>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBulk>
{ using type = OnePBulkProblem<TypeTag>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using PermType = Dune::FieldMatrix<Scalar,
                                       GridView::dimensionworld,
                                       GridView::dimensionworld>;

public:
    using type = BulkSpatialParams< GridGeometry, Scalar, PermType >;
};

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePBulk>
{ static constexpr bool value = false; };

// use a custom grid geometry with larger maximum stencil for mpfa
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::OnePBulkMpfa>
{
private:
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using PrimaryIV = GetPropType<TypeTag, Properties::PrimaryInteractionVolume>;
    using SecondaryIV = GetPropType<TypeTag, Properties::SecondaryInteractionVolume>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;

    struct MyTraits : public CCMpfaFVGridGeometryTraits<GridView, NodalIndexSet, PrimaryIV, SecondaryIV>
    {
#if DIMWORLD== 2
        static constexpr int maxElementStencilSize = 40;
#else
        static constexpr int maxElementStencilSize = 200;
#endif
    };
public:
    using type = CCMpfaFVGridGeometry<GridView, MyTraits, getPropValue<TypeTag, Properties::EnableGridGeometryCache>()>;
};

///////////////////////////////////////
// Properties for the facet problems //
///////////////////////////////////////
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePFacet>
{ using type = Dune::FoamGrid<DIMWORLD-1, DIMWORLD>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::OnePFacet>
{ using type = OnePFacetProblem<TypeTag>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePFacet>
{
    using type = FacetSpatialParams< GetPropType<TypeTag, GridGeometry>,
                                     GetPropType<TypeTag, Scalar> >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePFacet>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePFacet>
{ static constexpr bool value = false; };


//////////////////////////////////////////////
// Properties for the intersection problems //
//////////////////////////////////////////////
#if NUMDOMAINS == 3

template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIntersection>
{ using type = Dune::FoamGrid<DIMWORLD-2, DIMWORLD>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::OnePIntersection>
{ using type = OnePIntersectionProblem<TypeTag>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIntersection>
{
    // these are facet spatial params, too, that is, we don't consider full tensors
    using type = FacetSpatialParams< GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar> >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePIntersection>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePIntersection>
{ static constexpr bool value = false; };

#endif

///////////////////////////////////////////////////
// Set coupling manager property in sub-problems //
///////////////////////////////////////////////////
#if NUMDOMAINS== 2
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag=void>
struct CouplingManagerTraits
{
    using BulkGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGridGeometry = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkGridGeometry, FacetGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};
#else
template<class BulkTypeTag, class FacetTypeTag, class IntersectionTypeTag>
struct CouplingManagerTraits
{
    using BulkGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::GridGeometry>;
    using FacetGridGeometry = Dumux::GetPropType<FacetTypeTag, Dumux::Properties::GridGeometry>;
    using IntersectionGridGeometry = Dumux::GetPropType<IntersectionTypeTag, Dumux::Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag, IntersectionTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingThreeDomainMapper<BulkGridGeometry, FacetGridGeometry, IntersectionGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingThreeDomainManager<MDTraits, CouplingMapper>;
};
#endif

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkTpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkTpfa, TTag::OnePFacetTpfa, TTag::OnePIntersectionTpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkMpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkMpfa, TTag::OnePFacetMpfa, TTag::OnePIntersectionMpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkBox>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkBox, TTag::OnePFacetBox, TTag::OnePIntersectionBox>::CouplingManager; };


template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePFacetTpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkTpfa, TTag::OnePFacetTpfa, TTag::OnePIntersectionTpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePFacetMpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkMpfa, TTag::OnePFacetMpfa, TTag::OnePIntersectionMpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePFacetBox>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkBox, TTag::OnePFacetBox, TTag::OnePIntersectionBox>::CouplingManager; };


template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIntersectionTpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkTpfa, TTag::OnePFacetTpfa, TTag::OnePIntersectionTpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIntersectionMpfa>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkMpfa, TTag::OnePFacetMpfa, TTag::OnePIntersectionMpfa>::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIntersectionBox>
{ using type = typename CouplingManagerTraits<TTag::OnePBulkBox, TTag::OnePFacetBox, TTag::OnePIntersectionBox>::CouplingManager; };

} // end namespace Dumux::Properties

#endif
