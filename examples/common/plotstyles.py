import matplotlib.pyplot as plt
import sys

# plot adjustments
plt.style.use('ggplot')

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Ubuntu'
plt.rcParams['font.serif'] = 'Kaku Gothic Interface'
plt.rcParams['font.weight'] = 'light'
plt.rcParams['font.size'] = 13
plt.rcParams['font.weight'] = 'light'
plt.rcParams['font.size'] = 13

plt.rcParams['axes.labelsize'] = 20
plt.rcParams['axes.labelweight'] = 'light'
plt.rcParams['axes.labelsize'] = 28
plt.rcParams['axes.labelweight'] = 'light'

plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['legend.fontsize'] = 11

plt.rcParams['text.usetex'] = True
plt.rcParams['figure.titlesize'] = 15

plt.rcParams.update( {'mathtext.default': 'regular' } )
plt.rcParams["text.latex.preamble"] = [r"\usepackage{stmaryrd}",
                                       r"\usepackage{siunitx}",
                                       r"\usepackage{amsmath}",
                                       r"\usepackage{cmbright}",
                                       r"\sisetup{exponent-product=\cdot}"]

numSchemes = 5
names = ['tpfa', 'mpfa', 'box', 'box-mortar', 'box-cont']
labels = [r'\textsc{tpfa}-\textsc{dfm}',
          r'\textsc{mpfa}-\textsc{dfm}',
          r'\textsc{ebox}-\textsc{dfm}',
          r'\textsc{ebox}-\textsc{mortar}-\textsc{dfm}',
          r'\textsc{box}-\textsc{dfm}']
linestyles = ['solid', 'dashed', 'solid', 'dashdot', 'dashdot']
markers = [None, None, None, None, None]

colorsMedium = [
# [103.0/255.0,191.0/255.0,92.0/255.0],
[255.0/255.0,158.0/255.0,74.0/255.0],
[114.0/255.0,158.0/255.0,206.0/255.0],
[237.0/255.0,102.0/255.0,93.0/255.0],
# [173.0/255.0,139.0/255.0,201.0/255.0],
# [168.0/255.0,120.0/255.0,110.0/255.0],
[237.0/255.0,151.0/255.0,202.0/255.0],
[162.0/255.0,162.0/255.0,162.0/255.0],
[205.0/255.0,204.0/255.0,93.0/255.0],
[109.0/255.0,204.0/255.0,218.0/255.0]]

# colorsLight = [
# [174.0/255.0,199.0/255.0,232.0/255.0],
# [255.0/255.0,187.0/255.0,120.0/255.0],
# [152.0/255.0,223.0/255.0,138.0/255.0],
# [255.0/255.0,152.0/255.0,150.0/255.0],
# [197.0/255.0,176.0/255.0,213.0/255.0],
# [196.0/255.0,156.0/255.0,148.0/255.0],
# [247.0/255.0,182.0/255.0,210.0/255.0],
# [199.0/255.0,199.0/255.0,199.0/255.0],
# [219.0/255.0,219.0/255.0,141.0/255.0],
# [158.0/255.0,218.0/255.0,229.0/255.0]]

colors = colorsMedium
referenceColor = 'k'
referenceLabel = 'reference'

# return the name corresponding to a scheme ID
def getSchemeName(ID):
    return names[ID]

# return the ID corresponding to a scheme with the given name
def getSchemeId(schemeName):

    # make sure other writings are supported
    schemeName = schemeName.replace('_', '-')
    for i in range(0, numSchemes):
        if getSchemeName(i) == schemeName:
            return i
    sys.stderr.write("Could not get id of scheme with name " + schemeName)
    sys.exit(1)
