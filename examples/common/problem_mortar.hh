// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem class for the mortar problem between
 *        domains of different dimensionality.
 */
#ifndef DUMUX_GLAESER2020B_MORTAR_PROBLEM_HH
#define DUMUX_GLAESER2020B_MORTAR_PROBLEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/feproblem.hh>

namespace Dumux {

// forward declarations
template<class TypeTag> class MortarProblem;
template<class Problem> struct ProblemTraits;

//! TODO: Replace once ProblemTraits exist in master
template<class TypeTag>
struct ProblemTraits< MortarProblem<TypeTag> >
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
};

/*!
 * \todo TODO: Doc this
 */
template<class TypeTag>
class MortarProblem : public FEProblem< MortarProblem<TypeTag> >
{
    using ThisType = MortarProblem<TypeTag>;
    using ParentType = FEProblem<ThisType>;

    using Scalar = typename ProblemTraits<ThisType>::Scalar;
    using PrimaryVariables = typename ProblemTraits<ThisType>::PrimaryVariables;
    using NumEqVector = typename ProblemTraits<ThisType>::NumEqVector;
    using BoundaryTypes = typename ProblemTraits<ThisType>::BoundaryTypes;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using GridGeometry = typename ProblemTraits<ThisType>::GridGeometry;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FEElementGeometry = typename GridGeometry::LocalView;

public:

    //! The constructor
    MortarProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                  std::shared_ptr<CouplingManager> couplingManager,
                  const std::string& paramGroup)
    : ParentType(gridGeometry, paramGroup)
    , couplingManager_(couplingManager)
    , scaleFactor_(getParamFromGroup<Scalar>(paramGroup, "Problem.ScaleFactor"))
    , name_(getParamFromGroup<std::string>(paramGroup, "Problem.Name"))
    {}

    //! Return the problem name.
    std::string name() const
    { return name_; }

    //! Specifies the kind of boundary condition on a boundary segment
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluate the source term at a given integration point.
     * \note We use the source function to evaluate the constraint at
     *       an integration point.
     *
     * \param element The grid element
     * \param feGeometry The finite element geometry
     * \param gridVarsLocalView Local view on the grid variables
     * \param ipData Shape function values/gradients evaluated at an integration point
     * \param ipVars The primary/secondary variables evaluated at an integration point
     */
    template<class GridVarsLocalView, class IpData, class IpVariables>
    NumEqVector source(const Element& element,
                       const FEElementGeometry& feGeometry,
                       const GridVarsLocalView& gridVarsLocalView,
                       const IpData& ipData,
                       const IpVariables& ipVars) const
    { return couplingManager_->evalMortarCondition(element, ipVars.priVars(), ipData.ipGlobal()); }

    //! Returns the scale factor for the mortar variable
    Scalar scaleFactor(const Element& element) const
    { return scaleFactor_; }

    //! Returns the scale factor for the mortar variable
    Scalar scaleFactor() const
    { return scaleFactor_; }

    // functions we need for compatibilty with general solver class for all examples
    template<class... Args> void writeCsvData(Args&&... x) const {}
    template<class... Args> void addOutputFields(Args&&... x) const {}
    template<class... Args> void updateOutputFields(Args&&... x) const {}

    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    std::shared_ptr<CouplingManager> couplingManager_;
    Scalar scaleFactor_;
    std::string name_;
};

} // end namespace Dumux

#endif
