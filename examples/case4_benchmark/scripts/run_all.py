import os
import sys
import subprocess

# use custom plot style
sys.path.append("../common")
from plotstyles import *

# mesh ratios
refineFactor = 0.3

useIntDiri = False if sys.argv[1].lower() != 'true' else True
if useIntDiri: mortarGridFactors = [6.0/5.0, 2.0]
else: mortarGridFactors = [2.0/3.0, 6.0/5.0]

# make meshes
subprocess.call(['cp', 'grids/benchmark.geo', 'tmp.geo'])
subprocess.call(['cp', 'tmp.geo', 'tmp_mortar.geo'])
subprocess.call(['gmsh', '-2',
                 '-format', 'msh2',
                 '-setnumber', 'refineFactor', str(refineFactor),
                 '-setnumber', 'doTransfinites', '0',
                 'tmp.geo'])

# mortar grid with ratio < 1
subprocess.call(['gmsh', '-2',
                 '-format', 'msh2',
                 '-setnumber', 'refineFactor', str(refineFactor*mortarGridFactors[0]),
                 '-setnumber', 'doTransfinites', '0',
                 'tmp_mortar.geo'])
subprocess.call(['cp', 'tmp_mortar.msh', 'tmp_mortar_fine.msh'])

# mortar grid with ratio > 1
subprocess.call(['gmsh', '-2',
                 '-format', 'msh2',
                 '-setnumber', 'refineFactor', str(refineFactor*mortarGridFactors[1]),
                 '-setnumber', 'doTransfinites', '0',
                 'tmp_mortar.geo'])
subprocess.call(['cp', 'tmp_mortar.msh', 'tmp_mortar_coarse.msh'])

for schemeName in names:
    schemeName = schemeName.replace('-', '_')

    if schemeName == 'box_cont': exeName = 'case4_box_cont_benchmark'
    else: exeName = 'case4_facet_benchmark_' + schemeName

    # run dumux and copy solver time files
    def executeDumux(exe, suffix):
        if os.path.exists('solvertimes.csv'): os.remove('solvertimes.csv')
        subprocess.run(['make', exe], check=True)
        subprocess.run(['./' + exe,
                        'params.input',
                        '-Bulk.Problem.UseInteriorDirichletBCs', ('true' if useIntDiri else 'false'),
                        '-Bulk.Problem.Name', 'benchmark_bulk_' + suffix,
                        '-Facet.Problem.Name', 'benchmark_facet_' + suffix,
                        '-IO.VtkName', 'benchmark_' + suffix,
                        '-Bulk.IO.VtkName', 'benchmark_bulk_' + suffix,
                        '-Facet.IO.VtkName', 'benchmark_facet_' + suffix,
                        '-IO.CsvFileNameBody', 'benchmark_' + suffix,
                        '-Bulk.IO.CsvFileNameBody', 'benchmark_bulk_' + suffix,
                        '-Facet.IO.CsvFileNameBodys', 'benchmark_facet_' + suffix,
                        '-Facet.SpatialParams.Permeability', sys.argv[2],
                        '-Grid.File', 'tmp.msh',
                        '-Mortar.Grid.File','tmp_mortar.msh'], check=True)
        subprocess.run(['cp', 'solvertimes.csv', suffix + '_solvertimes.csv'], check=True)

    if schemeName != 'box_mortar':
        executeDumux(exeName, schemeName)
    else:
        subprocess.call(['cp', 'tmp_mortar_fine.msh', 'tmp_mortar.msh'])
        executeDumux(exeName, schemeName + '_fine')
        subprocess.call(['cp', 'tmp_mortar_coarse.msh', 'tmp_mortar.msh'])
        executeDumux(exeName, schemeName + '_coarse')

# clean up
os.remove('tmp.msh')
os.remove('tmp.geo')
os.remove('tmp_mortar.msh')
os.remove('tmp_mortar_fine.msh')
os.remove('tmp_mortar_coarse.msh')
