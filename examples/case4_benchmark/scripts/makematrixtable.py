import scipy.io as io
import numpy as np
import sys
import os

# use custom plot style
sys.path.append("../common")
from plotstyles import *

def getMatrixSpecs(matrixFile):
    M = io.mmread(matrixFile).tocsc()

    shape = M.shape
    if len(shape) != 2:
        sys.stderr.write("Unexpected matrix shape")
        sys.exit(1)

    if shape[0] != shape[1]:
        sys.stderr.write("Expected a square matrix")
        sys.exit(1)

    return {'numDofs': int(shape[0]),
            'nnz': int(M.count_nonzero())}

numDofString = r'$N_\mathrm{dof}$'
nnzString = r'$N_\mathrm{nnz}$'
cpuTimeString = r'$t_\mathrm{ls} \, [\si{\percent}]$'

if len(sys.argv) < 3:
    sys.exit("Expected two mortar grid factors to be passed")

psi1 = sys.argv[1]
psi2 = sys.argv[2]

tableFile = open('table.tex', 'w')
tableFile.write(r'\begin{table}[h]' + '\n' \
                r'  \centering' + '\n' \
                r'  \caption{\textbf{Case 3 - matrix characteristics}. Number of degrees of freedom (' + numDofString + ') and number of nonzero entries (' + nnzString + ') in the system matrices for the fine grid. Moreover, the cpu time (' + cpuTimeString + r') for the solution of the linear system with the direct solver UMFPack~\citep{Davis2004UmfPack}, on a single cpu with \SI{1.8}{\giga\hertz}, is shown.}' + '\n' \
                r'  \begin{tabular}{ *{7}{l} }' + '\n' \
                r'  \toprule' + '\n' \
                r'  & \tpfaDfm & \mpfaDfm & \eboxDfm & \multicolumn{2}{l}{\eboxMortarDfm} & \boxDfm \\' + '\n' \
                r'  &          &          &          & $\mortarGridFactor = ' + psi1 + r'$ & $\mortarGridFactor = ' + psi2 + r'$ & \\ \midrule' + '\n' \
               )

specs = {}
refValue = 1.0
for baseSchemeName in names:
    schemeNames = [baseSchemeName] if baseSchemeName != 'box-mortar' else [baseSchemeName + '_fine', baseSchemeName + '_coarse']
    for schemeName in schemeNames:
        schemeName = schemeName.replace('-', '_')
        with open(schemeName + '_solvertimes.csv', 'r') as cpuTimeFile:
            lines = cpuTimeFile.readlines()
            if len(lines) != 1: sys.exit("Expected a single line")
            line = lines[0].split(',')
            solverTimes = [float(x) for x in line if x != '']

        if schemeName == 'box_cont':
            matrixFile = 'benchmark_' + schemeName + '_matrix.mtx'
        else:
            matrixFile = 'benchmark_bulk_' + schemeName + '_matrix.mtx'

        schemeName = schemeName.replace('_', '')
        specs[schemeName] = getMatrixSpecs(matrixFile)
        specs[schemeName]['cpuTime'] = '{:.2f}'.format(np.mean(solverTimes))

        # relate all CPU times to mpfa
        if schemeName == 'mpfa':
            refValue = float(specs[schemeName]['cpuTime'])

for rowCaption in [numDofString, nnzString, cpuTimeString]:
    tableFile.write(rowCaption + ' & ')
    if rowCaption == numDofString: measure = 'numDofs'
    elif rowCaption == nnzString: measure = 'nnz'
    else: measure = 'cpuTime'

    for key in ['tpfa', 'mpfa', 'box', 'boxmortarfine', 'boxmortarcoarse', 'boxcont']:
        if measure == 'cpuTime': specs[key][measure] = '{:.2f}'.format(float(specs[key][measure])/refValue*100.0)
        tableFile.write( r'\num{' + str(specs[key][measure]) + r'}')
        if key == 'boxcont': tableFile.write(r' \\' + '\n')
        else: tableFile.write(' & ')

tableFile.write(r'  \bottomrule' + '\n' \
                r'  \end{tabular}' + '\n' \
                r'  \label{tab:benchmarkMatrices}' + '\n' \
                r'\end{table}' + '\n')
