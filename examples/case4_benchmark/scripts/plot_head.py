import os
import sys
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# use custom plot style
sys.path.append("../common")
from plotstyles import *

if len(sys.argv) != 3:
    sys.exit("Expected two arguments:\n" \
             "\t - (true/false) if reference scheme is to be plotted\n" \
             "\t - path to the extractlinedata.py script in dumux/bin/postprocessing\n")

plotReference = True if sys.argv[1].lower() in ['true', 'yes', '1'] else False
extractScript = sys.argv[2]

# function to obtain the plot over line data for a vtu file
def getPlotData(vtuFile, p1, p2, numSamples):
    print("Reading " + vtuFile)
    subprocess.run(['pvpython', extractScript,
                    '-f', vtuFile,
                    '-of', 'temp',
                    '-r', str(numSamples),
                    '-p1', p1[0], p1[1], p1[2],
                    '-p2', p2[0], p2[1], p2[2]], check=True)
    if not os.path.exists('temp.csv'):
        sys.exit("Could not find .csv file created by " + extractScript)
    data = np.genfromtxt('temp.csv', names=True, delimiter=',')
    os.remove('temp.csv')
    return {'arc': data['arc_length'], 'h': data['p']}

# function to get data for first/second plot
def getFirstPlotData(vtuFile):
    return getPlotData(vtuFile, ['0', '500', '0'], ['700', '500', '0'], 2000)
def getSecondPlotData(vtuFile):
    return getPlotData(vtuFile, ['625', '0', '0'], ['625', '600', '0'], 2000)

if plotReference:

    print("Getting reference .csv data for mfe-mortar-dfm method")
    subprocess.run(['wget', 'https://git.iws.uni-stuttgart.de/benchmarks/fracture-flow/-/raw/master/real/results/mortardfm/mortardfm_real_y500.csv'], check=True)
    subprocess.run(['wget', 'https://git.iws.uni-stuttgart.de/benchmarks/fracture-flow/-/raw/master/real/results/mortardfm/mortardfm_real_x625.csv'], check=True)

    data1 = np.loadtxt('mortardfm_real_y500.csv', delimiter=',', skiprows=1)
    data2 = np.loadtxt('mortardfm_real_x625.csv', delimiter=',', skiprows=1)

    plt.figure(1)
    plt.plot(data1[:,2], data1[:,0], label='mfe-mortar-dfm', color=referenceColor)
    plt.figure(2)
    plt.plot(data2[:,2], data2[:,0], label='mfe-mortar-dfm', color=referenceColor)

    os.remove('mortardfm_real_y500.csv')
    os.remove('mortardfm_real_x625.csv')

for schemeName in names:
    schemeId = getSchemeId(schemeName)
    suffixes = [''] if schemeName != 'box-mortar' else ['_fine', '_coarse']

    schemeName = schemeName.replace('-', '_')
    for suffix in suffixes:
        if schemeName == 'box_cont': vtuName = 'benchmark_box_cont-00001.vtu'
        else: vtuName = 'benchmark_bulk_' + schemeName + suffix + '-00001.vtu'

        theLabel = labels[schemeId]
        lineStyle = linestyles[schemeId]
        if suffix == '_fine': theLabel = theLabel + r' ($\psi = 2/3$)'; lineStyle = 'solid'
        if suffix == '_coarse': theLabel = theLabel + r' ($\psi = 6/5$)'; lineStyle = 'dashed'

        # first plot (y = 500)
        data = getFirstPlotData(vtuName)
        plt.figure(1)
        plt.plot(data['arc'], data['h'], label=theLabel, color=colors[schemeId], linestyle=lineStyle)
        plt.xlabel("arc length [m]")
        plt.ylabel(r"${h_2}$ [m]")

        # second plot (x = 625)
        data = getSecondPlotData(vtuName)
        plt.figure(2)
        plt.plot(data['arc'], data['h'], label=theLabel, color=colors[schemeId], linestyle=lineStyle)
        plt.xlabel("arc length [m]")
        plt.ylabel(r"${h_2}$ [m]")

plt.figure(1)
plt.legend()
plt.ticklabel_format(axis='y', scilimits=(0, 0))
plt.savefig("y_500.pdf", bbox_inches='tight')
plt.figure(2)
plt.legend()
plt.ticklabel_format(axis='y', scilimits=(0, 0))
plt.savefig("x_625.pdf", bbox_inches='tight')
