#!/bin/bash

clearOldResults() {
    FOLDER=$1
    rm ${FOLDER}/*.vtu \
       ${FOLDER}/*.pvd \
       ${FOLDER}/*.vtp \
       ${FOLDER}/*.csv \
       ${FOLDER}/*.pdf \
       ${FOLDER}/*.tex \
       ${FOLDER}/*.mtx
}

runAndPlot() {
    PERM=$1
    USE_INT_DIRI=$2

    if [ "${USE_INT_DIRI}" == "true" ]; then
        RESULTS_FOLDER="results_k_${PERM}_conditions_11b"
        MORTAR_FACTOR_1="6/5"
        MORTAR_FACTOR_2="2"
    else
        RESULTS_FOLDER="results_k_${PERM}_conditions_11a"
        MORTAR_FACTOR_1="2/3"
        MORTAR_FACTOR_2="6/5"
    fi

    clearOldResults .
    python3 scripts/run_all.py ${USE_INT_DIRI} ${PERM}
    python3 scripts/plot_head.py true ../../../../dumux/bin/postprocessing/extractlinedata.py
    python3 scripts/makematrixtable.py ${MORTAR_FACTOR_1} ${MORTAR_FACTOR_2}

    mkdir -p ${RESULTS_FOLDER}
    clearOldResults ${RESULTS_FOLDER}
    mv *.vtu *.vtp *.pvd *.csv *.pdf *.mtx *.tex ${RESULTS_FOLDER}
}


# highly-permeable fractures with conditions (11a)
runAndPlot 1e-8 false

# highly-permeable fractures with conditions (11b)
runAndPlot 1e-8 true

# blocking fractures with conditions (11a)
runAndPlot 1e-18 false
