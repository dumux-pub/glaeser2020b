#!/bin/sh

# call this script from one level above the folder glaeser2020b, i.e.:
# ./glaeser2020b/installGlaeser2020b.sh

runAndCheck() {
    COMMAND=$1
    if ! ${COMMAND}; then
        echo "Command failed: ${COMMAND}"
        exit 1
    fi
}

# dune-common
runAndCheck "git clone https://gitlab.dune-project.org/core/dune-common.git"
cd dune-common
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard 5e4163f1be487bb6bd914baec500432260002273"
cd ..

# dune-geometry
runAndCheck "git clone https://gitlab.dune-project.org/core/dune-geometry.git"
cd dune-geometry
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard fb75493b000410cbd157927f80c68ec9e86c5553"
cd ..

# dune-grid
runAndCheck "git clone https://gitlab.dune-project.org/core/dune-grid.git"
cd dune-grid
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard 550c58dc9c75ebf9ff2d640bd5ccbfd100714ffe"
cd ..

# dune-localfunctions
runAndCheck "git clone https://gitlab.dune-project.org/core/dune-localfunctions.git"
cd dune-localfunctions
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard c0ec05fdd1ec9fac1f07b8761f4b43b86c3528b3"
cd ..

# dune-istl
runAndCheck "git clone https://gitlab.dune-project.org/core/dune-istl.git"
cd dune-istl
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard 58ad784133a53d4a91131007255f1536b0368dfd"
cd ..

# dune-foamgrid
runAndCheck "git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git"
cd dune-foamgrid
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard d49187be4940227c945ced02f8457ccc9d47536a"
cd ..

# dune-alugrid
runAndCheck "git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git"
cd dune-alugrid
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard 7b55e36305bded9dd0d14e5f7c9b40f28f228aed"
cd ..

# dune-functions
runAndCheck "git clone https://gitlab.dune-project.org/staging/dune-functions.git"
cd dune-functions
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard f73a78e28538ae9627b8e41d49e87833d7487e1f"
cd ..

# dune-typetree
runAndCheck "git clone https://gitlab.dune-project.org/staging/dune-typetree.git"
cd dune-typetree
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard a91bce0b55f59706f603829b4a6e1220342ecdfa"
cd ..

# dune-subgrid
runAndCheck "git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git"
cd dune-subgrid
runAndCheck "git checkout master"
runAndCheck "git reset --hard e47ce1787dd799f34ff7308a12bc518626426467"
cd ..

# dune-uggrid
runAndCheck "git clone https://gitlab.dune-project.org/staging/dune-uggrid.git"
cd dune-uggrid
runAndCheck "git checkout releases/2.7"
runAndCheck "git reset --hard 40f60b18ca7f9e25347b2f3310f23c17f628e490"
cd ..

# dumux
runAndCheck "git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git"
cd dumux
runAndCheck "git checkout master"
runAndCheck "git reset --hard e9624324fdc2baa205aed8cd1da6155caf6fdd5c"
runAndCheck "git apply ../glaeser2020b/dumux_master.patch"
runAndCheck "git apply ../glaeser2020b/localchanges.patch"
cd ..

# configure project
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
