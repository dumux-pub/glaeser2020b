// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Discretization
 * \brief Declares properties required for finite element methods.
 */
#ifndef DUMUX_FEM_PROPERTIES_HH
#define DUMUX_FEM_PROPERTIES_HH

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/properties/grid.hh>
#include <dumux/common/boundarytypes.hh>

#include <dumux/discretization/fem/fegridvariables.hh>
#include <dumux/discretization/fem/fegridgeometry.hh>

namespace Dumux {
namespace Properties {

//! Type tag for finite element methods.
// Create new type tags
namespace TTag {
struct FEMModel { using InheritsFrom = std::tuple<GridProperties>; };
} // end namespace TTag

template<class TypeTag>
struct GridVariables<TypeTag, TTag::FEMModel>
{
private:
    using P = GetPropType<TypeTag, Properties::Problem>;
    using X = GetPropType<TypeTag, Properties::SolutionVector>;
public:
    using type = FEGridVariables<P, X>;
};

template<class TypeTag>
struct BoundaryTypes<TypeTag, TTag::FEMModel>
{ using type = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>; };

template<class TypeTag>
struct SolutionVector<TypeTag, TTag::FEMModel>
{ using type = Dune::BlockVector<GetPropType<TypeTag, Properties::PrimaryVariables>>; };

template<class TypeTag>
struct JacobianMatrix<TypeTag, TTag::FEMModel>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    enum { numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq() };
    using MatrixBlock = typename Dune::FieldMatrix<Scalar, numEq, numEq>;
public:
    using type = typename Dune::BCRSMatrix<MatrixBlock>;
};

} // namespace Properties
} // namespace Dumux

 #endif
