// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \copydoc Dumux::BoxMortarFacetCouplingManager
 */
#ifndef DUMUX_BOX_MORTAR_FACETCOUPLING_MANAGER_HH
#define DUMUX_BOX_MORTAR_FACETCOUPLING_MANAGER_HH

#include <type_traits>
#include <algorithm>
#include <cassert>
#include <vector>
#include <utility>

#include <dune/common/indices.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/promotiontraits.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/fem/ipdata.hh>

#include <dumux/multidomain/facet/box/couplingmanager.hh>
#include <dumux/multidomain/glue.hh>

#include "mortarcouplingmapper.hh"

namespace Dumux {

/*!
 * \ingroup FacetCoupling
 * \brief TODO
 * \todo TODO: Doc me
 *
 * \tparam MDTraits The multidomain traits containing the types on all sub-domains
 * \tparam CouplingMapper The coupling maps between the bulk and the facet domain
 * \tparam bulkId The domain id of the bulk problem
 * \tparam facetId The domain id of the lower-dimensional facet problem
 * \tparam mortarId The domain id of the lower-dimensional mortar problem
 */
template< class MDTraits,
          class CouplingMapper,
          std::size_t bulkId = 0,
          std::size_t facetId = 1,
          std::size_t mortarId = 2>
class BoxMortarFacetCouplingManager
: public FacetCouplingManager<MDTraits, CouplingMapper, bulkId, facetId>
{
    using ParentType = FacetCouplingManager<MDTraits, CouplingMapper, bulkId, facetId>;

    using BulkIdType = typename MDTraits::template SubDomain<bulkId>::Index;
    using FacetIdType = typename MDTraits::template SubDomain<facetId>::Index;
    using MortarIdType = typename MDTraits::template SubDomain<mortarId>::Index;

    static constexpr auto bulkDomainId = BulkIdType();
    static constexpr auto facetDomainId = FacetIdType();
    static constexpr auto mortarDomainId = MortarIdType();

    // the sub-domain type tags
    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;

    // further types specific to the sub-problems
    template<std::size_t id> using Scalar = GetPropType<SubDomainTypeTag<id>, Properties::Scalar>;
    template<std::size_t id> using PrimaryVariables = GetPropType<SubDomainTypeTag<id>, Properties::PrimaryVariables>;
    template<std::size_t id> using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
    template<std::size_t id> using NumEqVector = GetPropType<SubDomainTypeTag<id>, Properties::NumEqVector>;
    template<std::size_t id> using LocalResidual = GetPropType<SubDomainTypeTag<id>, Properties::LocalResidual>;
    template<std::size_t id> using ElementBoundaryTypes = GetPropType<SubDomainTypeTag<id>, Properties::ElementBoundaryTypes>;

    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using ElementGeometry = typename Element<id>::Geometry;
    template<std::size_t id> using GlobalPosition = typename ElementGeometry<id>::GlobalCoordinate;
    template<std::size_t id> using GridIndex = typename IndexTraits<GridView<id>>::GridIndex;
    template<std::size_t id> using LocalIndex = typename IndexTraits<GridView<id>>::GridIndex;
    template<std::size_t id> using Stencil = std::vector<GridIndex<id>>;

    template<std::size_t id> using GridVariables = GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using GridVolumeVariables = typename GridVariables<id>::GridVolumeVariables;
    template<std::size_t id> using ElementVolumeVariables = typename GridVolumeVariables<id>::LocalView;
    template<std::size_t id> using GridFluxVarsCache = typename GridVolumeVariables<id>::GridFluxVariablesCache;
    template<std::size_t id> using ElementFluxVariablesCache = typename GridFluxVarsCache<id>::LocalView;
    template<std::size_t id> using VolumeVariables = typename GridVolumeVariables<id>::VolumeVariables;

    using CouplingMapperMortarBulk = MortarCouplingMapper< GridGeometry<mortarDomainId>,
                                                           GridGeometry<bulkDomainId> >;
    using CouplingMapperMortarFacet = MortarCouplingMapper< GridGeometry<mortarDomainId>,
                                                            GridGeometry<facetDomainId> >;

    /*!
     * \brief
     * \todo TODO: doc me
     * \note We need unique ptrs here because the local views have no default constructor.
     */
    struct MortarCouplingContext
    {
        GridIndex<mortarDomainId> mortarElementIndex;

        // required data in the bulk neighbors
        std::vector< GridIndex<bulkId> > bulkElementIndices;
        std::vector< std::unique_ptr<FVElementGeometry<bulkId>> > bulkFvGeometries;
        std::vector< std::unique_ptr<ElementVolumeVariables<bulkId>> > bulkElemVolVars;

        // required data in the facet neighbors
        std::vector< GridIndex<facetId> > facetElementIndices;
        std::vector< std::unique_ptr<FVElementGeometry<facetId>> > facetFvGeometries;
        std::vector< std::unique_ptr<ElementVolumeVariables<facetId>> > facetElemVolVars;

        void clear()
        {
            bulkElementIndices.clear();
            bulkFvGeometries.clear();
            bulkElemVolVars.clear();

            facetElementIndices.clear();
            facetFvGeometries.clear();
            facetElemVolVars.clear();
        }
    };

public:

    //! the type of the solution vector
    using SolutionVector = typename MDTraits::SolutionVector;

    /*!
     * \brief Initialize the coupling manager.
     * \param bulkProblem The problem to be solved on the bulk domain
     * \param facetProblem The problem to be solved on the lower-dimensional domain
     * \param mortarProblem The problem to be solved on the lower-dimensional mortar domain
     * \param curSol The current solution
     */
    template<class OverlapIndices>
    void init(std::shared_ptr< Problem<bulkDomainId> > bulkProblem,
              std::shared_ptr< Problem<facetDomainId> > facetProblem,
              std::shared_ptr< Problem<mortarDomainId> > mortarProblem,
              std::shared_ptr< CouplingMapper > couplingMapper,
              const OverlapIndices& overlapIndices,
              const SolutionVector& curSol)
    {
        const auto mortarBulkGlue = makeGlue(mortarProblem->gridGeometry(),
                                             bulkProblem->gridGeometry());
        const auto mortarFacetGlue = makeGlue(mortarProblem->gridGeometry(),
                                              facetProblem->gridGeometry());

        init(bulkProblem, facetProblem, mortarProblem, couplingMapper, overlapIndices,
             curSol, mortarBulkGlue, mortarFacetGlue);
    }

    /*!
     * \brief Initialize the coupling manager.
     * \param bulkProblem The problem to be solved on the bulk domain
     * \param facetProblem The problem to be solved on the lower-dimensional domain
     * \param mortarProblem The problem to be solved on the lower-dimensional mortar domain
     * \param curSol The current solution
     * \param mortarBulkGlue glue object between mortar and bulk grid
     * \param mortarFacetGlue glue object between mortar and facet grid
     */
    template<class OverlapIndices, class BulkGlue, class FacetGlue>
    void init(std::shared_ptr< Problem<bulkDomainId> > bulkProblem,
              std::shared_ptr< Problem<facetDomainId> > facetProblem,
              std::shared_ptr< Problem<mortarDomainId> > mortarProblem,
              std::shared_ptr< CouplingMapper > couplingMapper,
              const OverlapIndices& overlapIndices,
              const SolutionVector& curSol,
              const BulkGlue& mortarBulkGlue,
              const FacetGlue& mortarFacetGlue)
    {
        ParentType::init(bulkProblem, facetProblem, couplingMapper, curSol);

        this->setSubProblem(mortarProblem, mortarDomainId);
        mortarBulkMapper_.init(mortarProblem->gridGeometry(),
                               bulkProblem->gridGeometry(),
                               mortarBulkGlue, overlapIndices);
        mortarFacetMapper_.init(mortarProblem->gridGeometry(),
                                facetProblem->gridGeometry(),
                                mortarFacetGlue, overlapIndices);
    }

    using ParentType::couplingStencil;
    /*!
     * \brief The coupling stencil of a given bulk domain
     *        element with respect to the mortar domain.
     */
    const typename CouplingMapperMortarBulk::MortarStencil&
    couplingStencil(BulkIdType domainId,
                    const Element<bulkDomainId>& element,
                    MortarIdType domainJ) const
    { return mortarBulkMapper_.domainCouplingStencil(element); }

    /*!
     * \brief The coupling stencil of a given facet domain
     *        element with respect to the mortar domain.
     */
    const typename CouplingMapperMortarFacet::MortarStencil&
    couplingStencil(FacetIdType domainId,
                    const Element<facetDomainId>& element,
                    MortarIdType domainJ) const
    { return mortarFacetMapper_.domainCouplingStencil(element); }

    /*!
     * \brief The coupling stencil of an element of the
     *        mortar domain with respect to a bulk element.
     */
    const typename CouplingMapperMortarBulk::DomainStencil&
    couplingStencil(MortarIdType domainId,
                    const Element<mortarId>& element,
                    BulkIdType domainJ) const
    { return mortarBulkMapper_.mortarCouplingStencil(element); }

    /*!
     * \brief The coupling stencil of an element of the
     *        mortar domain with respect to a facet element.
     */
    const typename CouplingMapperMortarFacet::DomainStencil&
    couplingStencil(MortarIdType domainId,
                    const Element<mortarId>& element,
                    FacetIdType domainJ) const
    { return mortarFacetMapper_.mortarCouplingStencil(element); }

    using ParentType::evalCouplingResidual;
    /*!
     * \brief Evaluates the coupling element residual of a bulk
     *        domain element with respect to a dof of the mortar domain.
     * \note These consist of the fluxes across coupling scvfs in the bulk domain.
     */
    template< class BulkLocalAssembler >
    typename LocalResidual<bulkId>::ElementResidualVector
    evalCouplingResidual(BulkIdType domainId,
                         const BulkLocalAssembler& bulkLocalAssembler,
                         MortarIdType domainIdJ,
                         GridIndex<mortarDomainId> dofIdxGlobalJ)
    {
        using LocalResidual = LocalResidual<bulkDomainId>;
        using ResidualVector = typename LocalResidual::ElementResidualVector;

        const auto& fvGeometry = bulkLocalAssembler.fvGeometry();
        ResidualVector res(fvGeometry.numScv());
        res = 0.0;

        // compute the fluxes over all embedded interface segments
        const auto& element = bulkLocalAssembler.element();
        std::vector<std::size_t> finished;
        finished.reserve(fvGeometry.numScvf());

        for (const auto& interface : domainInterfaceSegments(mortarBulkMapper_, element))
        {
            const auto& scvf = fvGeometry.scvf(interface.domainSubEntityIndex);

            if (!scvf.interiorBoundary())
                continue;
            if (std::count(finished.begin(), finished.end(), scvf.index()))
                continue;

            const auto& scv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& localResidual = bulkLocalAssembler.localResidual();
            res[scv.localDofIndex()] += localResidual.evalFlux(this->problem(bulkDomainId),
                                                               bulkLocalAssembler.element(),
                                                               bulkLocalAssembler.fvGeometry(),
                                                               bulkLocalAssembler.curElemVolVars(),
                                                               bulkLocalAssembler.elemBcTypes(),
                                                               bulkLocalAssembler.elemFluxVarsCache(),
                                                               scvf);

            finished.push_back(scvf.index());
        }

        return res;
    }

    /*!
     * \brief Evaluates the coupling element residual of a facet
     *        domain element with respect to a dof of the mortar domain.
     * \todo TODO: This should be zero?
     */
    template< class FacetLocalAssembler>
    typename LocalResidual<facetDomainId>::ElementResidualVector
    evalCouplingResidual(FacetIdType domainId,
                         const FacetLocalAssembler& facetLocalAssembler,
                         MortarIdType domainIdJ,
                         GridIndex<mortarDomainId> dofIdxGlobalJ)
    {
        using LocalResidual = LocalResidual<facetDomainId>;
        using ResidualVector = typename LocalResidual::ElementResidualVector;

        const auto& fvGeometry = facetLocalAssembler.fvGeometry();
        ResidualVector res(fvGeometry.numScv());
        res = 0.0;

        const auto& element = facetLocalAssembler.element();
        const auto& stencil = mortarFacetMapper_.domainCouplingStencil(element);
        if (stencil.size() == 0)
            return res;

        for (const auto& scv : scvs(facetLocalAssembler.fvGeometry()))
            res[scv.localDofIndex()] -= this->evalSourcesFromBulk(facetLocalAssembler.element(),
                                                                  facetLocalAssembler.fvGeometry(),
                                                                  facetLocalAssembler.curElemVolVars(),
                                                                  scv);
        return res;
    }

    /*!
     * \brief Evaluates the coupling element residual of a mortar
     *        domain element with respect to a dof of another domain.
     */
    template< class MortarLocalAssembler, std::size_t j >
    auto evalCouplingResidual(MortarIdType domainId,
                              const MortarLocalAssembler& mortarLocalAssembler,
                              Dune::index_constant<j> domainIdJ,
                              GridIndex<j> dofIdxGlobalJ)
    { return mortarLocalAssembler.localOperator().evalFluxesAndSources(); }

    /*!
     * \brief TODO
     * \todo TODO: Doc me.
     */
    Scalar<mortarDomainId> getIntegratedMortarFlux(const Element<bulkDomainId>& element,
                                                   const SubControlVolumeFace<bulkDomainId>& scvf) const
    {
        Scalar<mortarDomainId> flux = 0.0;
        Scalar<mortarDomainId> magnitude = 0.0;
        for (const auto& interface : domainInterfaceSegments(mortarBulkMapper_, element))
        {
            if (interface.domainSubEntityIndex != scvf.index())
                continue;

            const auto mortarElemIdx = interface.mortarElementIndex;
            const auto& mortarGG = this->problem(mortarDomainId).gridGeometry();
            const auto mortarElement = mortarGG.element(mortarElemIdx);
            flux += integrateMortarVariable_(mortarElement,
                                             mortarElement.geometry(),
                                             interface.geometry);
            magnitude += interface.geometry.volume();
        }

        using std::abs;
        // TODO: REMOVE AFTER TESTING
        if ( abs(magnitude - scvf.area()) > 1e-6 )
            DUNE_THROW(Dune::InvalidStateException, "Volume error: " << abs(magnitude - scvf.area())
                                                    << " with integrated area: " << magnitude
                                                    << " and face area: " << scvf.area());
        return flux;
    }

    Scalar<mortarDomainId> averageCondition(const Element<mortarDomainId>& element,
                                            const PrimaryVariables<mortarDomainId>& mortarPriVars) const
    {
        Scalar<bulkDomainId> interfaceFlux = 0.0;
        Scalar<bulkDomainId> interfacePressure = 0.0;
        GlobalPosition<bulkDomainId> normal;

        bool isNeumann = false;
        double intVolume = 0.0;
        for (const auto& interface : mortarInterfaceSegments(mortarBulkMapper_, element))
        {
            // TODO: Integrate instead of evaluating at segment center
            auto it = std::find(context_.bulkElementIndices.begin(),
                                context_.bulkElementIndices.end(),
                                interface.domainElementIndex);

            assert(it != context_.bulkElementIndices.end());
            const auto idxInContext = std::distance(context_.bulkElementIndices.begin(), it);

            const auto& bulkGG = this->problem(bulkDomainId).gridGeometry();
            const auto& fvGeometry = *context_.bulkFvGeometries[idxInContext];
            const auto& scvf = fvGeometry.scvf(interface.domainSubEntityIndex);

            const auto bulkElement = bulkGG.element(interface.domainElementIndex);
            const auto& bcTypes = this->problem(bulkDomainId).interiorBoundaryTypes(bulkElement, scvf);

            const auto elemSol = elementSolution(bulkElement, this->curSol()[bulkDomainId], bulkGG);
            const auto elemGeometry = bulkElement.geometry();
            const auto& interfaceGeometry = interface.geometry;

            // flux continuity condition
            if (bcTypes.hasOnlyNeumann())
            {
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& insideVolVars = (*context_.bulkElemVolVars[idxInContext])[insideScv];
                const auto gradP = evalGradients(bulkElement, elemGeometry, bulkGG, elemSol, interfaceGeometry.center())[0];
                interfaceFlux -= interfaceGeometry.volume()
                                 *vtmv(scvf.unitOuterNormal(),
                                       insideVolVars.permeability(),
                                       gradP);
                isNeumann = true;
            }

            // TODO: Use integration (for geometries on which avg value is not equal to center value)
            interfacePressure += interfaceGeometry.volume()
                                 *evalSolution(bulkElement, elemGeometry, bulkGG, elemSol, interfaceGeometry.center())[0];
            normal = scvf.unitOuterNormal();
            intVolume += interfaceGeometry.volume();
        }

        if ( std::abs(intVolume - element.geometry().volume()) > 1e-6*element.geometry().volume() )
            DUNE_THROW(Dune::InvalidStateException, "Wrong volume");

        interfaceFlux /= intVolume;
        interfacePressure /= intVolume;

        Scalar<mortarDomainId> facetPressure = 0.0;
        Scalar<mortarDomainId> facetFlux = 0.0;
        intVolume = 0.0;
        for (const auto& interface : mortarInterfaceSegments(mortarFacetMapper_, element))
        {
            auto it = std::find(context_.facetElementIndices.begin(),
                                context_.facetElementIndices.end(),
                                interface.domainElementIndex);
            assert(it != context_.facetElementIndices.end());
            const auto idxInContext = std::distance(context_.facetElementIndices.begin(), it);

            const auto& facetGG = this->problem(facetDomainId).gridGeometry();
            const auto facetElement = facetGG.element(interface.domainElementIndex);
            const auto& fvGeometry = (*context_.facetFvGeometries[idxInContext]);
            const auto& elemVolVars = (*context_.facetElemVolVars[idxInContext]);
            const auto& interfaceGeometry = interface.geometry;

            const auto elemSol = elementSolution(facetElement, this->curSol()[facetDomainId], facetGG);
            const auto curFacetPressure = evalSolution(facetElement, facetElement.geometry(), facetGG, elemSol, interfaceGeometry.center())[0];
            facetPressure += interfaceGeometry.volume()*curFacetPressure;

            if (isNeumann)
            {
                const auto& scv = fvGeometry.scv(interface.domainSubEntityIndex);
                const auto& volVars = elemVolVars[scv];
                const auto nKn = vtmv(normal, volVars.permeability(), normal);

                using std::sqrt;
                static constexpr int bulkDim = GridView<bulkDomainId>::dimension;
                static constexpr int bulkDimWorld = GridView<bulkDomainId>::dimensionworld;
                static constexpr bool isSurfaceGrid = bulkDimWorld > bulkDim;
                const auto l = isSurfaceGrid ? sqrt(volVars.extrusionFactor()/2.0)
                                             : volVars.extrusionFactor()/2.0;

                facetFlux += interfaceGeometry.volume()*nKn*(curFacetPressure - interfacePressure)/l;
            }

            intVolume += interfaceGeometry.volume();
        }

        static constexpr int mortarDim = Element<mortarDomainId>::Geometry::mydimension;
        static constexpr double eps = mortarDim == 1 ? 1e-6 : 1e-9;
        if ( std::abs(intVolume - element.geometry().volume()) > eps*element.geometry().volume() )
            DUNE_THROW(Dune::InvalidStateException, "Wrong volume for mortar element with center: "
                                                    << element.geometry().center()
                                                    << " and volume " << element.geometry().volume()
                                                    << " -> int volume: " << intVolume);

        facetFlux /= intVolume;
        facetPressure /= intVolume;

        if (isNeumann)
        {
            static const bool useKGRADP = getParam<bool>("Mortar.UseKGradP");
            if (useKGRADP)
                return interfaceFlux + facetFlux;
            else
                return mortarPriVars[0] + facetFlux*this->problem(mortarDomainId).scaleFactor();
        }
        else
            return interfacePressure - facetPressure;
    }

    /*!
     * \todo TODO: Doc this!
     * \todo TODO: what about gravity and two-phase flow conditions? Should this be elsewhere?
     */
    NumEqVector<mortarDomainId>
    evalMortarCondition(const Element<mortarDomainId>& element,
                        const PrimaryVariables<mortarDomainId>& mortarPriVars,
                        const GlobalPosition<mortarDomainId>& globalPos) const
    {
        NumEqVector<mortarDomainId> result(0.0);

        Scalar<bulkDomainId> interfaceFlux = 0.0;
        Scalar<bulkDomainId> interfacePressure = 0.0;
        GlobalPosition<bulkDomainId> normal;

        bool isNeumann = false;
        bool evaluated = false;
        for (const auto& interface : mortarInterfaceSegments(mortarBulkMapper_, element))
        {
            if (intersectsPointGeometry(globalPos, interface.geometry))
            {
                auto it = std::find(context_.bulkElementIndices.begin(),
                                    context_.bulkElementIndices.end(),
                                    interface.domainElementIndex);

                assert(it != context_.bulkElementIndices.end());
                const auto idxInContext = std::distance(context_.bulkElementIndices.begin(), it);

                const auto& bulkGG = this->problem(bulkDomainId).gridGeometry();
                const auto& fvGeometry = *context_.bulkFvGeometries[idxInContext];
                const auto& scvf = fvGeometry.scvf(interface.domainSubEntityIndex);

                const auto bulkElement = bulkGG.element(interface.domainElementIndex);
                const auto& bcTypes = this->problem(bulkDomainId).interiorBoundaryTypes(bulkElement, scvf);

                const auto elemSol = elementSolution(bulkElement, this->curSol()[bulkDomainId], bulkGG);
                const auto elemGeometry = bulkElement.geometry();

                // flux continuity condition
                if (bcTypes.hasOnlyNeumann())
                {
                    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                    const auto& insideVolVars = (*context_.bulkElemVolVars[idxInContext])[insideScv];
                    const auto gradP = evalGradients(bulkElement, elemGeometry, bulkGG, elemSol, globalPos)[0];
                    interfaceFlux -= vtmv(scvf.unitOuterNormal(),
                                          insideVolVars.permeability(),
                                          gradP);
                    isNeumann = true;
                }

                interfacePressure = evalSolution(bulkElement, elemGeometry, bulkGG, elemSol, globalPos)[0];
                evaluated = true;
                normal = scvf.unitOuterNormal();
                break;
            }
        }

        // TODO: Turn into assertion
        if (!evaluated)
            DUNE_THROW(Dune::InvalidStateException, "Not evaluated");

        // find the mortar-facet interface for this position and add values to residual
        evaluated = false;
        for (const auto& interface : mortarInterfaceSegments(mortarFacetMapper_, element))
        {
            if (intersectsPointGeometry(globalPos, interface.geometry))
            {
                auto it = std::find(context_.facetElementIndices.begin(),
                                    context_.facetElementIndices.end(),
                                    interface.domainElementIndex);
                assert(it != context_.facetElementIndices.end());
                const auto idxInContext = std::distance(context_.facetElementIndices.begin(), it);

                const auto& facetGG = this->problem(facetDomainId).gridGeometry();
                const auto facetElement = facetGG.element(interface.domainElementIndex);
                const auto& fvGeometry = (*context_.facetFvGeometries[idxInContext]);
                const auto& elemVolVars = (*context_.facetElemVolVars[idxInContext]);

                const auto elemSol = elementSolution(facetElement, this->curSol()[facetDomainId], facetGG);
                const auto p = evalSolution(facetElement, facetElement.geometry(), facetGG, elemSol, globalPos)[0];

                if (isNeumann)
                {
                    const auto& scv = fvGeometry.scv(interface.domainSubEntityIndex);
                    const auto& volVars = elemVolVars[scv];
                    const auto nKn = vtmv(normal, volVars.permeability(), normal);

                    using std::sqrt;
                    static constexpr int bulkDim = GridView<bulkDomainId>::dimension;
                    static constexpr int bulkDimWorld = GridView<bulkDomainId>::dimensionworld;
                    static constexpr bool isSurfaceGrid = bulkDimWorld > bulkDim;
                    const auto l = isSurfaceGrid ? sqrt(volVars.extrusionFactor()/2.0)
                                                 : volVars.extrusionFactor()/2.0;

                    static const bool useKGRADP = getParam<bool>("Mortar.UseKGradP");
                    if (useKGRADP)
                    {
                        result[0] += interfaceFlux;
                        result[0] += nKn*(p - interfacePressure)/l;
                    }
                    else
                    {
                        result[0] = mortarPriVars[0];
                        result[0] += this->problem(mortarDomainId).scaleFactor(element)*nKn*(p - interfacePressure)/l;
                    }
                }
                else
                    result = interfacePressure - p;

                evaluated = true;
                break;
            }
        }

        if (!evaluated)
            DUNE_THROW(Dune::InvalidStateException, "Mortar not evaluated");
        // result /= this->problem(mortarDomainId).scaleFactor(element);
        return result;
    }

    using ParentType::bindCouplingContext;
    /*!
     * \brief TODO
     * \todo TODO: Doc me.
     */
    template< class Assembler >
    void bindCouplingContext(MortarIdType domainId,
                             const Element<mortarDomainId>& element,
                             const Assembler& assembler)
    {
        context_.clear();

        // bulk data preparation
        for (const auto& interface : mortarInterfaceSegments(mortarBulkMapper_, element))
        {
            if (std::count(context_.bulkElementIndices.begin(),
                           context_.bulkElementIndices.end(),
                           interface.domainElementIndex))
                continue;

            const auto& bulkGG = this->problem(bulkDomainId).gridGeometry();
            const auto bulkElement = bulkGG.element(interface.domainElementIndex);

            auto fvGeometry = localView(bulkGG);
            auto elemVolVars = localView(assembler.gridVariables(bulkDomainId).curGridVolVars());

            fvGeometry.bind(bulkElement);
            elemVolVars.bind(bulkElement, fvGeometry, this->curSol()[bulkDomainId]);

            context_.bulkElementIndices.push_back(interface.domainElementIndex);
            context_.bulkFvGeometries.emplace_back(std::make_unique<FVElementGeometry<bulkId>>(std::move(fvGeometry)) );
            context_.bulkElemVolVars.emplace_back(std::make_unique<ElementVolumeVariables<bulkId>>(std::move(elemVolVars)) );
        }

        // facet data preparation
        for (const auto& interface : mortarInterfaceSegments(mortarFacetMapper_, element))
        {
            if (std::count(context_.facetElementIndices.begin(),
                           context_.facetElementIndices.end(),
                           interface.domainElementIndex))
                continue;

            const auto& facetGG = this->problem(facetDomainId).gridGeometry();
            const auto facetElement = facetGG.element(interface.domainElementIndex);

            auto fvGeometry = localView(facetGG);
            auto elemVolVars = localView(assembler.gridVariables(facetDomainId).curGridVolVars());

            fvGeometry.bind(facetElement);
            elemVolVars.bind(facetElement, fvGeometry, this->curSol()[facetDomainId]);

            context_.facetElementIndices.push_back(interface.domainElementIndex);
            context_.facetFvGeometries.emplace_back(std::make_unique<FVElementGeometry<facetId>>(std::move(fvGeometry)) );
            context_.facetElemVolVars.emplace_back(std::make_unique<ElementVolumeVariables<facetId>>(std::move(elemVolVars)) );
        }
    }

    using ParentType::updateCouplingContext;
    /*!
     * \brief TODO DOC THIS OVERLOAD
     * \todo TODO: instead of a complete re-bind, be more selective/efficient!
     */
    template< std::size_t i, class LocalAssembler>
    void updateCouplingContext(Dune::index_constant<i> domainId,
                               const LocalAssembler& localAssembler,
                               MortarIdType domainIdJ,
                               GridIndex<mortarDomainId> dofIdxGlobalJ,
                               const PrimaryVariables<mortarDomainId>& priVarsJ,
                               unsigned int pvIdxJ)
    {
        // communicate deflected solution
        this->curSol()[domainIdJ][dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];
    }

    /*!
     * \brief After deflecting the solution of a sub-domain during
     *        assembly of the mortar domain, update the context.
     * \todo TODO: instead of a complete re-bind, be more selective/efficient!
     */
    template< class MortarLocalAssembler, std::size_t j, std::enable_if_t<(j == bulkId || j == facetId), int> = 0 >
    void updateCouplingContext(MortarIdType domainId,
                               const MortarLocalAssembler& mortarLocalAssembler,
                               Dune::index_constant<j> domainIdJ,
                               GridIndex<j> dofIdxGlobalJ,
                               const PrimaryVariables<j>& priVarsJ,
                               unsigned int pvIdxJ)
    {
        // communicate deflected solution
        this->curSol()[domainIdJ][dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];

        // update the context
        const auto& bulkGG = this->problem(bulkDomainId).gridGeometry();
        for (unsigned int i = 0; i < context_.bulkElementIndices.size(); ++i)
        {
            const auto bulkElement = bulkGG.element(context_.bulkElementIndices[i]);
            const auto& bulkFvGeometry = *context_.bulkFvGeometries[i];
            (*context_.bulkElemVolVars[i]).bind(bulkElement, bulkFvGeometry, this->curSol()[bulkDomainId]);
        }

        const auto& facetGG = this->problem(facetDomainId).gridGeometry();
        for (unsigned int i = 0; i < context_.facetElementIndices.size(); ++i)
        {
            const auto facetElement = facetGG.element(context_.facetElementIndices[i]);
            const auto& facetFvGeometry = *context_.facetFvGeometries[i];
            (*context_.facetElemVolVars[i]).bind(facetElement, facetFvGeometry, this->curSol()[facetDomainId]);
        }
    }

private:

    //! TODO: Doc me
    template<class SegmentGeometry>
    Scalar<mortarDomainId> integrateMortarVariable_(const Element<mortarDomainId>& element,
                                                    const ElementGeometry<mortarDomainId>& geometry,
                                                    const SegmentGeometry& segmentGeometry) const
    {
        const auto& mortarProblem = this->problem(mortarDomainId);
        const auto& mortarGG = mortarProblem.gridGeometry();
        auto ggLocalView = localView(mortarGG);
        ggLocalView.bind(element);

        const auto& basisView = ggLocalView.feBasisLocalView();
        const auto& localBasis = basisView.tree().finiteElement().localBasis();

        using QuadRules = Dune::QuadratureRules<Scalar<mortarDomainId>, GridView<facetDomainId>::dimension>;
        static const auto intOrder = getParamFromGroup<Scalar<mortarDomainId>>(mortarProblem.paramGroup(), "FluxIntegrationOrder");

        Scalar<mortarDomainId> flux = 0.0;
        const auto elemSol = elementSolution(element, this->curSol()[mortarDomainId], mortarGG);
        for (const auto& qp : QuadRules::rule(segmentGeometry.type(), intOrder))
        {
            const auto ipLocal = qp.position();

            // TODO: implement evalSolution()
            auto ipData = FEIntegrationPointData(geometry, ipLocal, localBasis);
            Scalar<mortarDomainId> ipSol = 0.0;

            for (unsigned int i = 0; i < ipData.size(); ++i)
            { auto tmp = elemSol[i][0]; tmp *= ipData.shapeValue(i); ipSol += tmp; }

            ipSol *= qp.weight()*segmentGeometry.integrationElement(ipLocal);
            flux += ipSol;
        }

        flux /= mortarProblem.scaleFactor(element);
        return flux;
    }

    CouplingMapperMortarBulk mortarBulkMapper_;
    CouplingMapperMortarFacet mortarFacetMapper_;
    MortarCouplingContext context_;
};

/*!
 * \ingroup FacetCoupling
 * \brief TODO
 * \todo TODO: Doc me
 *
 * \tparam MDTraits The multidomain traits containing the types on all sub-domains
 * \tparam CouplingMapper The coupling maps between the bulk and the facet domain
 * \tparam bulkId The domain id of the bulk problem
 * \tparam facetId The domain id of the lower-dimensional facet problem
 * \tparam mortarId The domain id of the lower-dimensional mortar problem
 */
template< class MDTraits,
          class CouplingMapper,
          std::size_t bulkId = 0,
          std::size_t facetId = 1,
          std::size_t intersectionId = 2,
          std::size_t mortarFacetId = 3,
          std::size_t mortarIntersectionId = 4>
class BoxMortarFacetCouplingThreeDomainManager
: public BoxMortarFacetCouplingManager<MDTraits, CouplingMapper, bulkId, facetId, mortarFacetId>
, public BoxMortarFacetCouplingManager<MDTraits, CouplingMapper, facetId, intersectionId, mortarIntersectionId>
{
    using BulkFacetManager = BoxMortarFacetCouplingManager<MDTraits, CouplingMapper, bulkId, facetId, mortarFacetId>;
    using FacetEdgeManager = BoxMortarFacetCouplingManager<MDTraits, CouplingMapper, facetId, intersectionId, mortarIntersectionId>;

    // convenience aliases and instances of the domain ids
    using BulkIdType = typename MDTraits::template SubDomain<bulkId>::Index;
    using FacetIdType = typename MDTraits::template SubDomain<facetId>::Index;
    using IntersectionIdType = typename MDTraits::template SubDomain<intersectionId>::Index;
    using MortarFacetIdType = typename MDTraits::template SubDomain<mortarFacetId>::Index;
    using MortarIntersectionIdType = typename MDTraits::template SubDomain<mortarIntersectionId>::Index;
    static constexpr auto bulkDomainId = BulkIdType();
    static constexpr auto facetDomainId = FacetIdType();
    static constexpr auto intersectionDomainId = IntersectionIdType();
    static constexpr auto mortarFacetDomainId = MortarFacetIdType();
    static constexpr auto mortarIntersectionDomainId = MortarIntersectionIdType();

    // the sub-domain type tags
    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;

    // further types specific to the sub-problems
    template<std::size_t id> using LocalResidual = GetPropType<SubDomainTypeTag<id>, Properties::LocalResidual>;
    template<std::size_t id> using PrimaryVariables = GetPropType<SubDomainTypeTag<id>, Properties::PrimaryVariables>;
    template<std::size_t id> using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;

    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using GridIndexType = typename IndexTraits<GridView<id>>::GridIndex;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;

    template<std::size_t id> using GridVariables = GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using ElementVolumeVariables = typename GridVariables<id>::GridVolumeVariables::LocalView;
    template<std::size_t id> using ElementFluxVariablesCache = typename GridVariables<id>::GridFluxVariablesCache::LocalView;

    using EmptyStencilType = typename BulkFacetManager::template CouplingStencilType<0, 1>;

public:
    //! types used for coupling stencils
    template<std::size_t i, std::size_t j>
    using CouplingStencilType = typename BulkFacetManager::template CouplingStencilType<0, 1>;

    //! the type of the solution vector
    using SolutionVector = typename MDTraits::SolutionVector;

    /*!
     * \todo TODO: Doc me
     */
    template<class OverlapIndices>
    void init(std::shared_ptr< Problem<bulkId> > bulkProblem,
              std::shared_ptr< Problem<facetId> > facetProblem,
              std::shared_ptr< Problem<intersectionId> > intersectionProblem,
              std::shared_ptr< Problem<mortarFacetId> > mortarFacetProblem,
              std::shared_ptr< Problem<mortarIntersectionId> > mortarIntersectionProblem,
              std::shared_ptr< CouplingMapper > couplingMapper,
              const OverlapIndices& mortarFacetOverlapIndices,
              const OverlapIndices& mortarIntersectionOverlapIndices,
              const SolutionVector& curSol)
    {
        BulkFacetManager::init(bulkProblem, facetProblem, mortarFacetProblem, couplingMapper, mortarFacetOverlapIndices, curSol);
        FacetEdgeManager::init(facetProblem, intersectionProblem, mortarIntersectionProblem, couplingMapper, mortarIntersectionOverlapIndices, curSol);
    }

    //! Pull up functionalities from the parent classes
    using BulkFacetManager::couplingStencil;
    using FacetEdgeManager::couplingStencil;

    using BulkFacetManager::isCoupled;
    using FacetEdgeManager::isCoupled;

    using BulkFacetManager::isOnInteriorBoundary;
    using FacetEdgeManager::isOnInteriorBoundary;

    using BulkFacetManager::getLowDimVolVars;
    using FacetEdgeManager::getLowDimVolVars;

    using BulkFacetManager::getLowDimElement;
    using FacetEdgeManager::getLowDimElement;

    using BulkFacetManager::getLowDimElementIndex;
    using FacetEdgeManager::getLowDimElementIndex;

    using BulkFacetManager::evalSourcesFromBulk;
    using FacetEdgeManager::evalSourcesFromBulk;

    using BulkFacetManager::evalCouplingResidual;
    using FacetEdgeManager::evalCouplingResidual;

    using BulkFacetManager::bindCouplingContext;
    using FacetEdgeManager::bindCouplingContext;

    using BulkFacetManager::updateCouplingContext;
    using FacetEdgeManager::updateCouplingContext;

    using BulkFacetManager::updateCoupledVariables;
    using FacetEdgeManager::updateCoupledVariables;

    using BulkFacetManager::extendJacobianPattern;
    using FacetEdgeManager::extendJacobianPattern;

    using BulkFacetManager::evalAdditionalDomainDerivatives;
    using FacetEdgeManager::evalAdditionalDomainDerivatives;

    using BulkFacetManager::getIntegratedMortarFlux;
    using FacetEdgeManager::getIntegratedMortarFlux;

    using BulkFacetManager::averageCondition;
    using FacetEdgeManager::averageCondition;

    using BulkFacetManager::evalMortarCondition;
    using FacetEdgeManager::evalMortarCondition;

    // extension of the jacobian pattern for the facet domain only occurs
    // within the bulk-facet coupling & for mpfa being used in the bulk domain.
    template<class JacobianPattern>
    void extendJacobianPattern(FacetIdType, JacobianPattern& pattern) const
    { BulkFacetManager::extendJacobianPattern(facetDomainId, pattern); }

    template<class FacetLocalAssembler, class JacobianMatrixDiagBlock, class GridVariables>
    void evalAdditionalDomainDerivatives(FacetIdType,
                                         const FacetLocalAssembler& facetLocalAssembler,
                                         const typename FacetLocalAssembler::LocalResidual::ElementResidualVector& origResiduals,
                                         JacobianMatrixDiagBlock& A,
                                         GridVariables& gridVariables)
    { BulkFacetManager::evalAdditionalDomainDerivatives(facetDomainId, facetLocalAssembler, origResiduals, A, gridVariables); }

    //! All additional combinations of domains, not handled in the two parents, have empty stencils
    //! TODO: type safety of empty stencils for the different domains
    const EmptyStencilType& couplingStencil(BulkIdType, const Element<bulkId>& element, IntersectionIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(BulkIdType, const Element<bulkId>& element, MortarIntersectionIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(IntersectionIdType, const Element<intersectionId>& element, BulkIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(IntersectionIdType, const Element<intersectionId>& element, MortarFacetIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(MortarFacetIdType, const Element<mortarFacetId>& element, MortarIntersectionIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(MortarFacetIdType, const Element<mortarFacetId>& element, IntersectionIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(MortarIntersectionIdType, const Element<mortarIntersectionId>& element, BulkIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }
    const EmptyStencilType& couplingStencil(MortarIntersectionIdType, const Element<mortarIntersectionId>& element, MortarFacetIdType) const
    { return BulkFacetManager::getEmptyStencil(bulkDomainId); }

    /*!
     * \brief updates the current solution. We have to overload this here
     *        to avoid ambiguity and update the solution in both managers
     */
    void updateSolution(const SolutionVector& sol)
    {
        BulkFacetManager::updateSolution(sol);
        FacetEdgeManager::updateSolution(sol);
    }

    /*!
     * \brief Interface for evaluating the coupling residual between the pairs of domains that are
     *        not yet handled within the two parent classes. These are always zero as coupling only
     *        occurs between domain pairs handled in the parent classes. This should never be called
     *        but we need it for overload resolution.
     * \todo TODO: We need the separate overload for the mortar domains here due to the return
     *       type specification -> mortar model has no property "LocalResidual"
     */
    template<std::size_t i,
             std::size_t j,
             class LocalAssembler,
             std::enable_if_t<( (i==mortarFacetId && j==mortarIntersectionId)
                                 || (i==mortarFacetId && j==intersectionId)
                                 || (i==mortarIntersectionId && j==bulkId)
                                 || (i==mortarIntersectionId && j==mortarFacetId)
                              ), int> = 0>
    typename LocalResidual<0>::ElementResidualVector
    evalCouplingResidual(Dune::index_constant<i> domainI,
                         const LocalAssembler& localAssembler,
                         Dune::index_constant<j> domainJ,
                         GridIndexType<j> dofIdxGlobalJ)
    {
        // This is a hack as LocalResidual property does not exist for mortar type tag
        // The return type etc should be adapted for more complex models
        typename LocalResidual<0>::ElementResidualVector res(1);
        res = 0.0;
        return res;
    }

    /*!
     * \brief Interface for evaluating the coupling residual between the pairs of domains that are
     *        not yet handled within the two parent classes. These are always zero as coupling only
     *        occurs between domain pairs handled in the parent classes. This should never be called
     *        but we need it for overload resolution.
     * \todo TODO: We need the separate overload for the mortar domains here due to the return
     *       type specification -> mortar model has no property "LocalResidual"
     */
    template<std::size_t i,
             std::size_t j,
             class LocalAssembler,
             std::enable_if_t<( (i==bulkId && j==intersectionId)
                                 || (i==bulkId && j==mortarIntersectionId)
                                 || (i==intersectionId && j==bulkId)
                                 || (i==intersectionId && j==mortarFacetId)
                              ), int> = 0>
    typename LocalResidual<i>::ElementResidualVector
    evalCouplingResidual(Dune::index_constant<i> domainI,
                         const LocalAssembler& localAssembler,
                         Dune::index_constant<j> domainJ,
                         GridIndexType<j> dofIdxGlobalJ)
    {
        typename LocalResidual<i>::ElementResidualVector res(1);
        res = 0.0;
        return res;
    }

    /*!
     * \brief Interface for binding the coupling context for the facet domain. In this case
     *        we have to bind both the facet -> bulk and the facet -> edge coupling context.
     */
    template< class Assembler >
    void bindCouplingContext(FacetIdType, const Element<facetId>& element, const Assembler& assembler)
    {
        BulkFacetManager::bindCouplingContext(facetDomainId, element, assembler);
        FacetEdgeManager::bindCouplingContext(facetDomainId, element, assembler);
    }

    /*!
     * \brief Interface for updating the coupling context of the facet domain. In this case
     *        we have to update both the facet -> bulk and the facet -> edge coupling context.
     */
    template< class FacetLocalAssembler >
    void updateCouplingContext(FacetIdType domainI,
                               const FacetLocalAssembler& facetLocalAssembler,
                               FacetIdType domainJ,
                               GridIndexType<facetId> dofIdxGlobalJ,
                               const PrimaryVariables<facetId>& priVarsJ,
                               unsigned int pvIdxJ)
    {
        BulkFacetManager::updateCouplingContext(domainI, facetLocalAssembler, domainJ, dofIdxGlobalJ, priVarsJ, pvIdxJ);
        FacetEdgeManager::updateCouplingContext(domainI, facetLocalAssembler, domainJ, dofIdxGlobalJ, priVarsJ, pvIdxJ);
    }

    /*!
     * \brief Interface for updating the coupling context between for pairs of domain ids
     *        that are not yet handled in the parent classes. Needed for overload resolution.
     */
    template<std::size_t i,
             std::size_t j,
             class LocalAssembler,
             std::enable_if_t<( (i==bulkId && j==intersectionId)
                                 || (i==bulkId && j==mortarIntersectionId)
                                 || (i==intersectionId && j==bulkId)
                                 || (i==intersectionId && j==mortarFacetId)
                                 || (i==mortarFacetId && j==mortarIntersectionId)
                                 || (i==mortarFacetId && j==intersectionId)
                                 || (i==mortarIntersectionId && j==bulkId)
                                 || (i==mortarIntersectionId && j==mortarFacetId)
                              ), int> = 0>
    void updateCouplingContext(Dune::index_constant<i> domainI,
                               const LocalAssembler& localAssembler,
                               Dune::index_constant<j> domainJ,
                               GridIndexType<j> dofIdxGlobalJ,
                               const PrimaryVariables<j>& priVarsJ,
                               unsigned int pvIdxJ)
    { /*do nothing here*/ }

    /*!
     * \brief Interface for updating the local views of the facet domain after updateCouplingContext
     *        the coupling context. In this case we have to forward the both managers as the facet
     *        domain is a part in both.
     */
    template< class FacetLocalAssembler, class UpdatableElementVolVars, class UpdatableFluxVarCache>
    void updateCoupledVariables(FacetIdType domainI,
                                const FacetLocalAssembler& facetLocalAssembler,
                                UpdatableElementVolVars& elemVolVars,
                                UpdatableFluxVarCache& elemFluxVarsCache)
    {
        BulkFacetManager::updateCoupledVariables(domainI, facetLocalAssembler, elemVolVars, elemFluxVarsCache);
        FacetEdgeManager::updateCoupledVariables(domainI, facetLocalAssembler, elemVolVars, elemFluxVarsCache);
    }

    //! Return a const reference to bulk or facet problem
    template<std::size_t id, std::enable_if_t<(id == bulkId || id == facetId || id == mortarFacetId), int> = 0>
    const Problem<id>& problem() const { return BulkFacetManager::template problem<id>(); }

    //! Return a reference to bulk or facet problem
    template<std::size_t id, std::enable_if_t<(id == bulkId || id == facetId || id == mortarFacetId), int> = 0>
    Problem<id>& problem() { return BulkFacetManager::template problem<id>(); }

    //! Return a const reference to edge problem
    template<std::size_t id, std::enable_if_t<(id == intersectionId || id == mortarIntersectionId), int> = 0>
    const Problem<id>& problem() const { return FacetEdgeManager::template problem<id>(); }

    //! Return a reference to edge problem
    template<std::size_t id, std::enable_if_t<(id == intersectionId || id == mortarIntersectionId), int> = 0>
    Problem<id>& problem() { return FacetEdgeManager::template problem<id>(); }
};

} // end namespace Dumux

#endif // DUMUX_BOX_MORTAR_FACETCOUPLING_MANAGER_HH
