/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \copydoc Dumux::MortarGridCreator
 */
#ifndef DUMUX_BOX_FACETCOUPLING_MORTAR_GRID_CREATOR_HH
#define DUMUX_BOX_FACETCOUPLING_MORTAR_GRID_CREATOR_HH

#include <memory>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/grid/common/gridfactory.hh>

#include <dumux/multidomain/glue.hh>

namespace Dumux {

/*!
 * \ingroup FacetCoupling
 * \brief Grid creator for a mortar grid used to describe the interface
 *        between two grids of different dimensionality.
 * \todo TODO: Doc more
 */
template<class Grid>
class FacetCouplingMortarGridCreator
{
    using IndexType = typename Grid::LeafGridView::IndexSet::IndexType;
    using Element = typename Grid::template Codim<0>::Entity;
    using Geometry = typename Element::Geometry;
    using GlobalPosition = typename Geometry::GlobalCoordinate;

public:

    template<class BulkGG, class MortarGG>
    void create(const BulkGG& bulkGG,
                const MortarGG& mortarGG)
    {
        static_assert(int(BulkGG::GridView::dimensionworld) == int(MortarGG::GridView::dimensionworld),
                      "World dimension mismatch");
        static_assert(int(BulkGG::GridView::dimension) > int(MortarGG::GridView::dimension),
                      "Mortar grid dimension is expected to be smaller than bulk grid dimension");
        const auto glue = makeGlue(mortarGG, bulkGG);

        std::size_t vertexIdx = 0;
        std::size_t elementIdx = 0;
        std::vector<IndexType> cornerIndices;
        std::vector<bool> handled(mortarGG.gridView().size(0), false);
        for (const auto& is : intersections(glue))
        {
            if (is.numDomainNeighbors() != 1)
                DUNE_THROW(Dune::InvalidStateException,
                           "This expects always 1 domain neighbor");

            const auto& mortarElement = is.domainEntity();
            const auto mortarElemIdx = mortarGG.elementMapper().index(mortarElement);
            if (!handled[mortarElemIdx])
            {
                const auto& eg = mortarElement.geometry();

                // make an individual element for each
                // interface of the element with the bulk domain
                std::vector<std::size_t> elemInsertionIndices(is.numTargetNeighbors());
                for (std::size_t i = 0; i < is.numTargetNeighbors(); ++i)
                {
                    cornerIndices.clear();
                    cornerIndices.reserve(eg.corners());

                    for (unsigned int i = 0; i < eg.corners(); ++i)
                    {
                        gridFactory_.insertVertex(eg.corner(i));
                        cornerIndices.push_back(vertexIdx);
                        vertexIdx++;
                    }

                    gridFactory_.insertElement(eg.type(), cornerIndices);
                    elemInsertionIndices[i] = elementIdx++;
                }

                for (int i = 0; i < elemInsertionIndices.size(); ++i)
                    for (int j = i+1; j < elemInsertionIndices.size(); ++j)
                    {
                        rawOverlapIndices_[elemInsertionIndices[i]].push_back(elemInsertionIndices[j]);
                        rawOverlapIndices_[elemInsertionIndices[j]].push_back(elemInsertionIndices[i]);
                    }
            }

            handled[mortarElemIdx] = true;
        }

        gridPtr_ = std::shared_ptr<Grid>(gridFactory_.createGrid());
    }

    //! Return reference to the grid
    const Grid& grid() const
    { return *gridPtr_; }

    //! Return reference to the grid factory
    const Dune::GridFactory<Grid>& gridFactory() const
    { return gridFactory_; }

    template<class MortarGG>
    std::vector< std::vector<std::size_t> > getMortarOverlapIndices(const MortarGG& mortarGG) const
    {
        std::vector<std::size_t> idxMap(mortarGG.gridView().size(0));
        for (const auto& e : elements(mortarGG.gridView()))
            idxMap[gridFactory().insertionIndex(e)] = mortarGG.elementMapper().index(e);

        std::vector< std::vector<std::size_t> > result(grid().leafGridView().size(0));
        for (const auto& pair : rawOverlapIndices_)
        {
            result[idxMap[pair.first]].reserve(pair.second.size());
            for (auto rawIdx : pair.second)
                result[idxMap[pair.first]].push_back(idxMap[rawIdx]);
        }
        return result;
    }

private:
    Dune::GridFactory<Grid> gridFactory_;
    std::shared_ptr<Grid> gridPtr_;
    std::unordered_map<std::size_t, std::vector<std::size_t> > rawOverlapIndices_;
};

} // end namespace Dumux

#endif
