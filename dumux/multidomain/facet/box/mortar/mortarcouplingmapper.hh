// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \copydoc Dumux::BoxMortarFacetCouplingManager
 */
#ifndef DUMUX_BOX_MORTAR_FACETCOUPLING_MAPPER_HH
#define DUMUX_BOX_MORTAR_FACETCOUPLING_MAPPER_HH

#include <array>
#include <algorithm>
#include <cassert>
#include <vector>

#include <dune/common/promotiontraits.hh>
#include <dune/common/iteratorrange.hh>
#include <dune/common/iteratorfacades.hh>
#include <dune/common/exceptions.hh>
#include <dune/geometry/affinegeometry.hh>

#include <dumux/common/math.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/multidomain/glue.hh>

namespace Dumux {

/*!
 * \ingroup FacetCoupling
 * \brief TODO
 * \todo TODO: Doc me
 *
 * \tparam TODO
 */
template< class MortarGridGeometry,
          class DomainGridGeometry >
class MortarCouplingMapper
{
    using DomainGridView = typename DomainGridGeometry::GridView;
    using MortarGridView = typename MortarGridGeometry::GridView;

    using DomainElement = typename DomainGridView::template Codim<0>::Entity;
    using DomainElementGeometry = typename DomainElement::Geometry;
    using DomainLocalView = typename DomainGridGeometry::LocalView;

    using MortarElement = typename MortarGridView::template Codim<0>::Entity;
    using MortarElementGeometry = typename MortarElement::Geometry;

    using IndexList = std::vector<std::size_t>;
    using DomainIndexType = typename IndexTraits<DomainGridView>::GridIndex;
    using MortarIndexType = typename IndexTraits<MortarGridView>::GridIndex;
    using ctype = typename Dune::PromotionTraits<typename DomainGridView::ctype,
                                                 typename MortarGridView::ctype>::PromotedType;

    // extract grid dimensions
    static constexpr int domainDim = DomainGridView::dimension;
    static constexpr int mortarDim = MortarGridView::dimension;
    static_assert(domainDim >= mortarDim, "Grid dimension mismatch");
    static_assert(mortarDim < 3, "Mortar grid dimension must be smaller than 3");

    static constexpr int dimWorld = DomainGridView::dimensionworld;
    static_assert(dimWorld == MortarGridView::dimensionworld, "World dimension mismatch");

    // types to describe the geometry of an interface segment
    using SegmentGeometry = Dune::AffineGeometry<ctype, mortarDim, dimWorld>;

    struct InterfaceSegment
    {
        SegmentGeometry geometry;
        MortarIndexType mortarElementIndex;
        DomainIndexType domainElementIndex;
        DomainIndexType domainSubEntityIndex;

        InterfaceSegment(const SegmentGeometry& g)
        : geometry(g)
        {}
    };

    template<class IndexVector>
    class InterfaceSegmentIterator
    : public Dune::ForwardIteratorFacade< InterfaceSegmentIterator<IndexVector>,
                                          const InterfaceSegment >
    {
        using ThisType = InterfaceSegmentIterator<IndexVector>;
        using IndexIterator = typename IndexVector::const_iterator;
        using MapperType = MortarCouplingMapper<MortarGridGeometry, DomainGridGeometry>;

    public:
        //! constructor
        InterfaceSegmentIterator(const IndexIterator& it,
                                 const MapperType& mapper)
        : it_(it)
        , mapperPtr_(&mapper)
        {}

        //! dereferencing yields an interface segment
        const InterfaceSegment& dereference() const
        { return mapperPtr_->interfaceSegment(*it_); }

        //! equality check with other iterator
        bool equals(const ThisType& other) const
        { return it_ == other.it_; }

        void increment()
        { ++it_; }

    private:
        IndexIterator it_;
        const MapperType* mapperPtr_;
    };

public:
    //! export stencil types
    using MortarStencil = std::vector<MortarIndexType>;
    using DomainStencil = std::vector<DomainIndexType>;

    /*!
     * \brief TODO
     * \todo TODO: Doc me
     */
    void init(const MortarGridGeometry& mortarGridGeometry,
              const DomainGridGeometry& domainGridGeometry,
              const std::vector<std::vector<std::size_t>>& overlapIndices)
    {
        init(mortarGridGeometry,
             domainGridGeometry,
             makeGlue(mortarGridGeometry, domainGridGeometry),
             overlapIndices);
    }
    /*!
     * \brief TODO
     * \todo TODO: Doc me
     * \todo TODO: The term "domain" is used here different than in intersections.
     *             This should be adapted to be less confusing, as in intersections,
     *             target() returns the entity we want for "domain"
     */
    template<class Glue>
    void init(const MortarGridGeometry& mortarGridGeometry,
              const DomainGridGeometry& domainGridGeometry,
              const Glue& glue,
              const std::vector<std::vector<std::size_t>>& overlapIndices)
    {
        clear_();

        mortarGridGeometry_ = &mortarGridGeometry;
        domainGridGeometry_ = &domainGridGeometry;

        // determine estimate for number of interfaces
        std::size_t numInterfaceEstimate = 0;
        for (const auto& is : intersections(glue))
            numInterfaceEstimate += is.numTargetNeighbors();

        domainMortarStencils_.resize(domainGridGeometry.gridView().size(0));
        mortarDomainStencils_.resize(mortarGridGeometry.gridView().size(0));
        interfaceSegments_.reserve(numInterfaceEstimate);
        std::cout << "MORTARDIM: " << int(MortarGridGeometry::GridView::dimension) << std::endl;
        std::cout << "DOMAINDIM: " << int(DomainGridGeometry::GridView::dimension) << std::endl;
        ctype minVolume = std::numeric_limits<ctype>::max();
        for (const auto& is : intersections(glue))
        {
            // neglect small (possibly erroneous intersections)
            auto domainNeighborVolume = is.domainEntity().geometry().volume();
            if (is.geometry().volume() < 1e-8*domainNeighborVolume)
                continue;

            using std::min;
            minVolume = min(minVolume, is.geometry().volume());
            // TODO: Turn into assertions after testing
            if (is.numDomainNeighbors() != 1)
                DUNE_THROW(Dune::InvalidStateException, "Found more than one mortar neighbor! Found " << is.numDomainNeighbors());
            if (domainDim == mortarDim && is.numTargetNeighbors() != 1)
                DUNE_THROW(Dune::InvalidStateException, "Found more than one domain neighbor! Found " << is.numTargetNeighbors());
            if (domainDim > mortarDim && is.numTargetNeighbors() < 2)
            {
                for (int cId = 0; cId < is.targetEntity().geometry().corners(); ++cId)
                    std::cout << std::setprecision(15) << "Domain element corner " << cId << ": " << is.targetEntity().geometry().corner(cId) << std::endl;
                for (int cId = 0; cId < is.domainEntity().geometry().corners(); ++cId)
                    std::cout << std::setprecision(15) << "Mortar element corner " << cId << ": " << is.domainEntity().geometry().corner(cId) << std::endl;
                for (int cId = 0; cId < is.geometry().corners(); ++cId)
                    std::cout << std::setprecision(15) << "IS corner " << cId << ": " << is.geometry().corner(cId) << std::endl;
                std::cout << "IS Volume: " << is.geometry().volume() << std::endl;
                DUNE_THROW(Dune::InvalidStateException, "Did not find  more than one domain neighbor! Found " << is.numTargetNeighbors()
                                                        << "\n\t - for mortar element with center: " << std::setprecision(15) << is.domainEntity().geometry().center()
                                                        << "\n\t - and is center: " << std::setprecision(15) << is.geometry().center());
            }

            const auto mortarElement = is.domainEntity();
            const auto mortarElementIdx = mortarGridGeometry.elementMapper().index(mortarElement);
            const auto mortarElemGeometry = mortarElement.geometry();

            // find out which neighbor to use for this mortar element
            unsigned int nIdx = getDomainNeighborIndex_(mortarElementIdx,
                                                        overlapIndices[mortarElementIdx],
                                                        mortarElemGeometry,
                                                        is);
            const auto domainElement = is.targetEntity(nIdx);
            const auto domainElementIdx = domainGridGeometry.elementMapper().index(domainElement);

            // skip the rest if for this mortar element there are already interfaces
            // to this bulk element registered from a previous intersection
            bool found = false;
            auto it = mortarInterfacesMap_.find(mortarElementIdx);
            if (it != mortarInterfacesMap_.end())
                for (auto segIdx : it->second)
                    if (interfaceSegments_[segIdx].domainElementIndex == domainElementIdx)
                    { found = true; break; }
            if (found)
                continue;

            auto domainFvGeometry = localView(domainGridGeometry);
            domainFvGeometry.bindElement(domainElement);

            auto mortarLocalView = localView(mortarGridGeometry);
            mortarLocalView.bind(mortarElement);
            const auto& mortarBasisLocalView = mortarLocalView.feBasisLocalView();

            // fill stencils
            for (const auto& scv : scvs(domainFvGeometry))
                mortarDomainStencils_[mortarElementIdx].push_back(scv.dofIndex());
            for (unsigned int i = 0; i < mortarBasisLocalView.size(); ++i)
                domainMortarStencils_[domainElementIdx].push_back(mortarBasisLocalView.index(i));

            // create (sub-)interfaces with each overlapping sub-entity
            addSubInterfaces_(domainElement, mortarElement, domainFvGeometry,
                              mortarElemGeometry, domainElementIdx, mortarElementIdx);
        }

        std::cout << "Minimum detected intersection volume between "
                  << domainDim << "- and " << mortarDim << "-dimensional domains: "
                  << minVolume << std::endl;

        auto makeUnique = [] (auto& s)
        {
            std::sort(s.begin(), s.end());
            s.erase(std::unique(s.begin(), s.end()), s.end());
        };
        for (auto& s : domainMortarStencils_) makeUnique(s);
        for (auto& s : mortarDomainStencils_) makeUnique(s);
    }

    //! returns the coupling stencil of a domain element
    const MortarStencil& domainCouplingStencil(const DomainElement& element) const
    { return domainMortarStencils_[domainGridGeometry_->elementMapper().index(element)]; }

    //! returns the coupling stencil of a mortar element
    const DomainStencil& mortarCouplingStencil(const MortarElement& element) const
    { return mortarDomainStencils_[mortarGridGeometry_->elementMapper().index(element)]; }

    //! returns the i-th interface segment
    const InterfaceSegment& interfaceSegment(std::size_t i) const
    { assert(i < interfaceSegments_.size()); return interfaceSegments_[i]; }

    //! iterator range for interface segments embedded in a domain element.
    friend inline Dune::IteratorRange< InterfaceSegmentIterator<IndexList> >
    domainInterfaceSegments(const MortarCouplingMapper& mapper, const DomainElement& domainElement)
    {
        const auto& eIdx = mapper.domainGridGeometry_->elementMapper().index(domainElement);
        auto it = mapper.domainInterfacesMap_.find(eIdx);

        // return an empty iterator range
        using Iterator = InterfaceSegmentIterator<IndexList>;
        if (it == mapper.domainInterfacesMap_.end())
            return Dune::IteratorRange<Iterator>(Iterator(mapper.domainInterfacesMap_.begin()->second.begin(), mapper),
                                                 Iterator(mapper.domainInterfacesMap_.begin()->second.begin(), mapper));

        return Dune::IteratorRange<Iterator>(Iterator(it->second.begin(), mapper),
                                             Iterator(it->second.end(), mapper));
    }

    //! iterator range for interface segments embedded in a mortar element.
    friend inline Dune::IteratorRange< InterfaceSegmentIterator<IndexList> >
    mortarInterfaceSegments(const MortarCouplingMapper& mapper, const MortarElement& mortarElement)
    {
        const auto& eIdx = mapper.mortarGridGeometry_->elementMapper().index(mortarElement);
        auto it = mapper.mortarInterfacesMap_.find(eIdx);

        // return an empty iterator range
        using Iterator = InterfaceSegmentIterator<IndexList>;
        if (it == mapper.mortarInterfacesMap_.end())
            return Dune::IteratorRange<Iterator>(Iterator(mapper.mortarInterfacesMap_.begin()->second.begin(), mapper),
                                                 Iterator(mapper.mortarInterfacesMap_.begin()->second.begin(), mapper));

        return Dune::IteratorRange<Iterator>(Iterator(it->second.begin(), mapper),
                                             Iterator(it->second.end(), mapper));
    }

private:
    //! clear all data
    void clear_()
    {
        interfaceSegments_.clear();
        domainMortarStencils_.clear();
        mortarDomainStencils_.clear();

        domainInterfacesMap_.clear();
        mortarInterfacesMap_.clear();
    }

    // returns the neighbor of an intersection to use for the interface
    // between a mortar element and the domain
    template<class Intersection>
    unsigned int getDomainNeighborIndex_(MortarIndexType mortarElementIdx,
                                         const std::vector<std::size_t>& overlapIndices,
                                         const MortarElementGeometry& mortarElemGeometry,
                                         const Intersection& is)
    {
        if constexpr (domainDim == mortarDim)
            return 0;
        else
        {
            unsigned int nIdx = 0;
            const bool elementWasRegistered = mortarInterfacesMap_.find(mortarElementIdx) != mortarInterfacesMap_.end();
            const bool geometryWasRegistered = std::any_of(overlapIndices.begin(),
                                                           overlapIndices.end(),
                                                           [&] (auto idx) { return mortarInterfacesMap_.find(idx) != mortarInterfacesMap_.end(); });

            if (elementWasRegistered || geometryWasRegistered)
            {
                bool found = false;
                for (unsigned int i = 0; i < is.numTargetNeighbors(); ++i)
                {
                    const auto& domainElement = is.targetEntity(i);
                    const auto& domainElementGeometry = domainElement.geometry();
                    // const auto domainElementIdx = domainGridGeometry_->elementMapper().index(domainElement);

                    // lambda to check if the domain element adjacent to an interface is on the same side
                    // as the current domain element
                    auto segmentOnSameSide = [&] (auto segmentIndex)
                    {
                        const auto& otherElemIdx = interfaceSegments_[segmentIndex].domainElementIndex;
                        return this->isOnSameSide_(domainElementGeometry,
                                                   mortarElemGeometry,
                                                   domainGridGeometry_->element(otherElemIdx).geometry());
                    };

                    // if interfaces have been registered for this element yet, use this neighbor if it is
                    // on the same side as those neighbors of the previously registered interfaces
                    if (elementWasRegistered)
                    {
                        const auto& segIndices = mortarInterfacesMap_.at(mortarElementIdx);
                        for (auto segIdx : segIndices)
                        {
                            if (segmentOnSameSide(segIdx))
                            { nIdx = i; found = true; break; }
                        }
                    }
                    // otherwise, use this neighbor if it is NOT on the side of any previously registered interface
                    else
                    {
                        bool noneOnSameSide = true;
                        for (auto overlapIdx : overlapIndices)
                        {
                            auto overlapIt = mortarInterfacesMap_.find(overlapIdx);
                            if (overlapIt != mortarInterfacesMap_.end())
                            {
                                for (auto segIdx : overlapIt->second)
                                    if (segmentOnSameSide(segIdx))
                                    { noneOnSameSide = false; break; }
                            }

                            if (!noneOnSameSide) break;
                        }

                        if (noneOnSameSide)
                        { nIdx = i; found = true; break; }
                    }

                    if (found) break;
                }

                if (!found)
                {
                    std::cout << "IS INFO: " << std::endl;
                    std::cout << " -> vol: " << is.geometry().volume() << std::endl;
                    for (int i = 0; i < is.geometry().corners(); ++i)
                        std::cout << " -> c" << i << ": " << is.geometry().corner(i) << std::endl;
                    DUNE_THROW(Dune::InvalidStateException, "Could not determine domain neighbor of interface");
                }
            }

            return nIdx;
        }
    }

    // returns true if the both domain elements are on the same side of the mortar geometry
    bool isOnSameSide_(const DomainElementGeometry& geo1,
                       const MortarElementGeometry& mortarGeom,
                       const DomainElementGeometry& geo2)
    {
        static_assert(domainDim > mortarDim, "Query assumes higher-dimensional domain geometries");

        const auto v1 = geo1.center() - mortarGeom.center();
        const auto v2 = geo2.center() - mortarGeom.center();

        // intersection domain mortars in 3d
        if constexpr (dimWorld == 3 && mortarDim == 1)
        {
            auto v = mortarGeom.corner(1) - mortarGeom.corner(0);
            auto n = crossProduct(v, v1);
            n /= n.two_norm();

            // must be on the same plane
            if (std::abs(n*v2) > 1e-6*v2.two_norm())
                return false;

            // projection of geo1.center() & geom2.center() on the line
            v /= v.two_norm();
            auto projV1 = v; projV1 *= v*v1; projV1 += mortarGeom.center();
            auto projV2 = v; projV2 *= v*v2; projV2 += mortarGeom.center();

            const auto d1 = geo1.center() - projV1;
            const auto d2 = geo2.center() - projV2;
            return !std::signbit(d1*d2);
        }

        using GlobalPosition = typename MortarElementGeometry::GlobalCoordinate;
        GlobalPosition n;
        if constexpr (dimWorld == 2)
        {
            const auto v = mortarGeom.corner(1) - mortarGeom.corner(0);
            n = GlobalPosition({v[1], -v[0]});
        }
        else
        {
            const auto a = mortarGeom.corner(1) - mortarGeom.corner(0);
            const auto b = mortarGeom.corner(2) - mortarGeom.corner(0);
            n = crossProduct(a, b);
        }

        return std::signbit(v1*n) == std::signbit(v2*n);
    }

    //! add (sub-) interfaces for overlapping sub-entities between the domains
    void addSubInterfaces_(const DomainElement& domainElement,
                           const MortarElement& mortarElement,
                           const DomainLocalView& domainLocalView,
                           const MortarElementGeometry& geometry,
                           const DomainIndexType domainElementIdx,
                           const MortarIndexType mortarElementIdx)
    {
        // lambda to add sub-interfaces
        auto addInterfaces = [&] (const auto& geometries, auto subEntityIndex) -> void
        {
            for (const auto& geometry : geometries)
            {
                const auto iFaceIdx = interfaceSegments_.size();
                interfaceSegments_.emplace_back(geometry);

                auto& interface = interfaceSegments_.back();
                interface.domainElementIndex = domainElementIdx;
                interface.domainSubEntityIndex = subEntityIndex;
                interface.mortarElementIndex = mortarElementIdx;

                domainInterfacesMap_[domainElementIdx].push_back(iFaceIdx);
                mortarInterfacesMap_[mortarElementIdx].push_back(iFaceIdx);
            }
        };

        if constexpr(domainDim > mortarDim)
        {
            for (const auto& scvf : scvfs(domainLocalView))
            {
                using Scvf = typename DomainGridGeometry::SubControlVolumeFace;
                using ScvfGeometry = typename Scvf::Traits::Geometry;
                using IntersectionAlgo = GeometryIntersection<ScvfGeometry, MortarElementGeometry>;

                typename IntersectionAlgo::Intersection result;
                if (IntersectionAlgo::intersection(scvf.geometry(), geometry, result))
                    addInterfaces( makeInterfaceSegmentGeometries_(result), scvf.index() );
            }
        }
        else
        {
            for (const auto& scv : scvs(domainLocalView))
            {
                using Scv = typename DomainGridGeometry::SubControlVolume;
                using ScvGeometry = typename Scv::Traits::Geometry;
                using IntersectionAlgo = GeometryIntersection<ScvGeometry, MortarElementGeometry>;

                typename IntersectionAlgo::Intersection result;
                if (IntersectionAlgo::intersection(scv.geometry(), geometry, result))
                    addInterfaces( makeInterfaceSegmentGeometries_(result), scv.localDofIndex() );
            }
        }
    }

    //! turn a 1d raw entity intersection into a segment geometry
    template<class RawIntersection, int d = mortarDim, std::enable_if_t<d == 1, int> = 0>
    std::array<SegmentGeometry, 1> makeInterfaceSegmentGeometries_(const RawIntersection& is) const
    { return {SegmentGeometry(Dune::GeometryTypes::line, is)}; }

    //! turn a 2d raw entity intersection into a segment geometry triangulation
    template<class RawIntersection, int d = mortarDim, std::enable_if_t<d == 2, int> = 0>
    std::vector<SegmentGeometry> makeInterfaceSegmentGeometries_(const RawIntersection& is) const
    {
        std::vector<SegmentGeometry> result;
        for (const auto& triangle : triangulate<mortarDim, dimWorld>(is))
            result.emplace_back(Dune::GeometryTypes::triangle, triangle);
        return result;
    }

    const MortarGridGeometry* mortarGridGeometry_;
    const DomainGridGeometry* domainGridGeometry_;

    std::vector< InterfaceSegment > interfaceSegments_;
    std::vector< MortarStencil > domainMortarStencils_;
    std::vector< DomainStencil > mortarDomainStencils_;

    //! Maps to the elements the embedded interfaces
    std::unordered_map< DomainIndexType, std::vector<std::size_t> > domainInterfacesMap_;
    std::unordered_map< MortarIndexType, std::vector<std::size_t> > mortarInterfacesMap_;
};

} // end namespace Dumux

#endif // DUMUX_BOX_MORTAR_FACETCOUPLING_MAPPER_HH
