// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Assembly
 * \ingroup FEMDiscretization
 * \ingroup MultiDomain
 * \brief An assembler for Jacobian and residual contribution per element
 *        (finite element methods) for multidomain problems
 */
#ifndef DUMUX_MULTIDOMAIN_FE_LOCAL_ASSEMBLER_HH
#define DUMUX_MULTIDOMAIN_FE_LOCAL_ASSEMBLER_HH

#include <dune/common/indices.hh>
#include <dune/common/hybridutilities.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numericdifferentiation.hh>

#include <dumux/assembly/fem/operatorsbase.hh>
#include <dumux/assembly/fem/localoperator.hh>
#include <dumux/assembly/numericepsilon.hh>
#include <dumux/assembly/diffmethod.hh>

namespace Dumux {

/*!
 * \ingroup Assembly
 * \ingroup FEMDiscretization
 * \ingroup MultiDomain
 * \brief The finite element schemes' multidomain local assembler
 * \tparam id the id of the sub domain
 * \tparam TypeTag the TypeTag
 * \tparam Assembler the assembler type
 * \tparam DM the numeric differentiation method
 * \tparam implicit whether the assembler is explicit or implicit in time
 */
template<std::size_t id, class TypeTag, class Assembler, DiffMethod DM = DiffMethod::numeric, bool implicit = true>
class SubDomainFELocalAssembler;

/*!
 * \ingroup Assembly
 * \ingroup FEMDiscretization
 * \ingroup MultiDomain
 * \brief Finite element schemes' multidomain local assembler using numeric differentiation and implicit time discretization
 */
template<std::size_t id, class TypeTag, class Assembler>
class SubDomainFELocalAssembler<id, TypeTag, Assembler, DiffMethod::numeric, /*implicit=*/true>
{
    using SolutionVector = typename Assembler::ResidualType;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVarsLocalView = typename GridVariables::LocalView;
    using SubSolutionVector = typename GridVariables::SolutionVector;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FEElementGeometry = typename GridGeometry::LocalView;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    // HACK: Here we hard-code the MortarOperator for now
    class Operators : public FEOperatorsBase<GridVarsLocalView>
    {
        using Parent = FEOperatorsBase<GridVarsLocalView>;
    public:
        /*!
         * \brief The constructor
         */
        Operators(const Element& element,
                  const FEElementGeometry& feGeometry,
                  const GridVarsLocalView& gridVarsLocalView)
        : Parent(element, feGeometry, gridVarsLocalView)
        {}

        template<class... Args>
        typename FEOperatorsBase<GridVarsLocalView>::StorageTerm
        storage(Args&&... args) const { return {0.0}; }

        template<class... Args>
        typename FEOperatorsBase<GridVarsLocalView>::FluxTerm
        flux(Args&&... args) const { return {0.0}; }

        // We hack the constraint into the source term for now!
        template<class IpData, class IpVariables>
        typename FEOperatorsBase<GridVarsLocalView>::SourceTerm
        source(const IpData& ipData,
               const IpVariables& ipVars) const
        {
            const auto& problem = this->gridVariablesLocalView().gridVariables().problem();
            static const bool useAverage = getParam<bool>("Mortar.UseAverageCondition");
            if (useAverage)
                return problem.couplingManager().averageCondition(this->element(), ipVars.priVars());
            else
                return problem.source(this->element(), this->feGeometry(), this->gridVariablesLocalView(), ipData, ipVars);
        }
    };

    using LocalOperator = FELocalOperator<GridVarsLocalView, Operators>;

    static constexpr int numEq = GetPropType<TypeTag, Properties::PrimaryVariables>::size();
    static constexpr int dim = GridView::dimension;
    static constexpr auto domainI = Dune::index_constant<id>();

public:
    //! pull up public export of base class
    using ElementResidualVector = typename LocalOperator::ElementResidualVector;

    //! The constructor
    explicit SubDomainFELocalAssembler(const Assembler& assembler,
                                       const Element& element,
                                       const SolutionVector& curSol,
                                       CouplingManager& couplingManager)
    : assembler_(assembler)
    , element_(element)
    , curSol_(curSol)
    , couplingManager_(couplingManager)
    , feGeometry_(localView(assembler.gridGeometry(domainI)))
    , gridVarsLocalView_(localView(assembler.gridVariables(domainI)))
    , localOperator_(nullptr)
    {}

    /*!
     * \brief Prepares all local views necessary for local assembly.
     */
    void bindLocalViews()
    {
        couplingManager_.bindCouplingContext(domainI, element_, assembler_);
        feGeometry_.bind(element_);
        gridVarsLocalView_.bind(element_, feGeometry_);
        localOperator_ = std::make_unique<LocalOperator>(element_, feGeometry_, gridVarsLocalView_);
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix. The element residual is written into the right hand side.
     */
    template<class JacobianMatrixRow, class GridVariablesTuple>
    void assembleJacobianAndResidual(JacobianMatrixRow& jacRow, SubSolutionVector& res, GridVariablesTuple& gridVariables)
    {
        bindLocalViews();

        // for the diagonal jacobian block
        // forward to the internal implementation
        const auto residual = assembleJacobianAndResidualImpl(jacRow[domainI], *std::get<domainI>(gridVariables));

        // update the residual vector
        const auto& localView = feGeometry_.feBasisLocalView();
        for (unsigned int i = 0; i < localView.size(); ++i)
            res[localView.index(i)] += residual[i];

        // assemble the coupling blocks
        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::Hybrid::size(jacRow)), [&](auto domainJ)
        {
            if (domainJ != id)
                this->assembleJacobianCoupling(domainJ, jacRow[domainJ], residual, *std::get<domainJ>(gridVariables));
        });
    }

    /*!
     * \brief Assemble the residual vector entries only
     */
    void assembleResidual(SubSolutionVector& res)
    {
        bindLocalViews();

        const auto residual = localOperator_->evalFluxesAndSources();
        const auto& localView = feGeometry_.feBasisLocalView();
        for (unsigned int i = 0; i < localView.size(); ++i)
            res[localView.index(i)] += residual[i];
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<class JacobianMatrixDiagBlock, class GridVariables>
    ElementResidualVector assembleJacobianAndResidualImpl(JacobianMatrixDiagBlock& A, GridVariables& gridVariables)
    {
        // get some aliases for convenience
        const auto& localView = feGeometry_.feBasisLocalView();
        auto& elemSol = gridVarsLocalView_.elemSol();

        // get the vector of the actual element residuals
        const auto origResiduals = localOperator_->evalFluxesAndSources();

        for (unsigned int i = 0; i < localView.size(); ++i)
        {
            // undeflected privars and privars to be deflected
            const auto globalJ = localView.index(i);
            const auto origPriVarsJ = elemSol[i];
            auto& priVarsJ = elemSol[i];

            for (int pvIdx = 0; pvIdx < JacobianMatrixDiagBlock::block_type::cols; ++pvIdx)
            {
                auto evalResidual = [&](Scalar priVar)
                {
                    priVarsJ[pvIdx] = priVar;
                    couplingManager_.updateCouplingContext(domainI, *this, domainI, globalJ, priVarsJ, pvIdx);
                    return localOperator_->evalFluxesAndSources();
                };

                // derive the residuals numerically
                ElementResidualVector partialDerivs(localView.size());

                const auto& paramGroup = assembler_.problem(domainI).paramGroup();
                static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                static const auto epsCoupl = couplingManager_.numericEpsilon(domainI, paramGroup);

                NumericDifferentiation::partialDerivative(evalResidual, origPriVarsJ[pvIdx], partialDerivs, origResiduals,
                                                          epsCoupl(origPriVarsJ[pvIdx], pvIdx), numDiffMethod);

                // update the global stiffness matrix with the current partial derivatives
                for (unsigned int localI = 0; localI < localView.size(); ++localI)
                {
                    const auto globalI = localView.index(localI);

                    // don't add derivatives for green entities
                    for (int eqIdx = 0; eqIdx < numEq; eqIdx++)
                        A[globalI][globalJ][eqIdx][pvIdx] += partialDerivs[localI][eqIdx];
                }

                // restore the current element solution
                priVarsJ[pvIdx] = origPriVarsJ[pvIdx];

                // restore the undeflected state of the coupling context
                couplingManager_.updateCouplingContext(domainI, *this, domainI, globalJ, priVarsJ, pvIdx);
            }
        }

        return origResiduals;
    }

    /*!
     * \brief Assemble the entries in a coupling block of the jacobian.
     *        There is no coupling block between a domain and itself.
     */
    template<std::size_t otherId, class JacRow, class GridVariables,
             typename std::enable_if_t<(otherId == id), int> = 0>
    void assembleJacobianCoupling(Dune::index_constant<otherId> domainJ, JacRow& jacRow,
                                  const ElementResidualVector& res, GridVariables& gridVariables)
    {}

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<std::size_t otherId, class JacobianBlock, class GridVariables,
             typename std::enable_if_t<(otherId != id), int> = 0>
    void assembleJacobianCoupling(Dune::index_constant<otherId> domainJ, JacobianBlock& A,
                                  const ElementResidualVector& res, GridVariables& gridVariables)
    {
        // get some references for convenience
        const auto& stencil = couplingManager_.couplingStencil(domainI, element_, domainJ);
        const auto& curSolJ = curSol_[domainJ];

        for (const auto globalJ : stencil)
        {
            // undeflected privars and privars to be deflected
            const auto origPriVarsJ = curSolJ[globalJ];
            auto priVarsJ = origPriVarsJ;

            // the undeflected coupling residual
            const auto origResidual = couplingManager_.evalCouplingResidual(domainI, *this, domainJ, globalJ);

            for (int pvIdx = 0; pvIdx < JacobianBlock::block_type::cols; ++pvIdx)
            {
                auto evalCouplingResidual = [&](Scalar priVar)
                {
                    priVarsJ[pvIdx] = priVar;
                    couplingManager_.updateCouplingContext(domainI, *this, domainJ, globalJ, priVarsJ, pvIdx);
                    return couplingManager_.evalCouplingResidual(domainI, *this, domainJ, globalJ);
                };

                // derive the residuals numerically
                const auto localView = feGeometry_.feBasisLocalView();
                ElementResidualVector partialDerivs(localView.size());

                const auto& paramGroup = assembler_.problem(domainJ).paramGroup();
                static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                static const auto epsCoupl = couplingManager_.numericEpsilon(domainJ, paramGroup);

                NumericDifferentiation::partialDerivative(evalCouplingResidual, origPriVarsJ[pvIdx], partialDerivs, origResidual,
                                                          epsCoupl(origPriVarsJ[pvIdx], pvIdx), numDiffMethod);

                // update the global stiffness matrix with the current partial derivatives
                for (unsigned int localI = 0; localI < localView.size(); ++localI)
                {
                    const auto globalI = localView.index(localI);

                    // don't add derivatives for green entities
                    for (int eqIdx = 0; eqIdx < numEq; eqIdx++)
                        A[globalI][globalJ][eqIdx][pvIdx] += partialDerivs[localI][eqIdx];
                }

                // restore the current element solution
                priVarsJ[pvIdx] = origPriVarsJ[pvIdx];

                // restore the undeflected state of the coupling context
                couplingManager_.updateCouplingContext(domainI, *this, domainJ, globalJ, priVarsJ, pvIdx);
            }
        }
    }

    const LocalOperator& localOperator() const
    { return *localOperator_; }

private:
    const Assembler& assembler_;       //!< access pointer to assembler instance
    const Element& element_;           //!< the element whose residual is assembled
    const SolutionVector& curSol_;     //!< the current solution
    CouplingManager& couplingManager_; //!< the coupling manager

    FEElementGeometry feGeometry_;
    GridVarsLocalView gridVarsLocalView_;
    std::unique_ptr<LocalOperator> localOperator_;
};

} // end namespace Dumux

#endif
